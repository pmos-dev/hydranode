import * as http from 'http';

import * as express from 'express';

import { TPropertyObject } from 'tscommons-core';
import { CommonsAsync } from 'tscommons-async';
import { CommonsConfig } from 'tscommons-config';

import { CommonsConfigFile } from 'nodecommons-config';
import { CommonsArgs } from 'nodecommons-cli';
import { CommonsOutput } from 'nodecommons-cli';
import { isICommonsCredentials } from 'nodecommons-database';
import { isICommonsExpressConfig } from 'nodecommons-express';

import { Lists } from '../classes/lists';
import { Expiry } from '../classes/expiry';
import { Tracker } from '../classes/tracker';

import { ExpressServer } from '../servers/express.server';
import { CrawlServer } from '../servers/crawl.server';
import { RestServer } from '../servers/rest.server';
import { LiveServer } from '../servers/live.server';
import { UiServer } from '../servers/ui.server';
import { MaintenanceServer } from '../servers/maintenance.server';

import { DatabaseService } from '../services/database.service';

import { isICrawlConfig } from '../interfaces/icrawl-config';
import { isIParsingConfig } from '../interfaces/iparsing-config';
import { isIRobotsConfig } from '../interfaces/irobots-config';
import { isIUiConfig } from '../interfaces/iui-config';

import { ServerParser } from '../parsers/server.parser';
import { HtmlParser } from '../parsers/html.parser';
import { JpegParser } from '../parsers/jpeg.parser';
import { SpellingParser } from '../parsers/spelling.parser';
import { BadWordsParser } from '../parsers/bad-words.parser';
import { PhpErrorParser } from '../parsers/php-error.parser';
import { AspErrorParser } from '../parsers/asp-error.parser';
import { UomBlockedParser } from '../parsers/bespoke/uom-blocked.parser';

const config: CommonsConfig = new CommonsConfigFile(`${__dirname}/../../config/hydra.json`);
const args: CommonsArgs = new CommonsArgs();

const credentials: TPropertyObject = config.getObject('database');
if (!isICommonsCredentials(credentials)) throw new Error('Credentials are not valid');
const database: DatabaseService = new DatabaseService(credentials);

const crawlConfig: TPropertyObject = config.getObject('crawl');
if (!isICrawlConfig(crawlConfig)) throw new Error('Crawl config is not valid');

const parsingConfig: TPropertyObject = config.getObject('parsing');
if (!isIParsingConfig(parsingConfig)) throw new Error('Parsing config is not valid');

const robotsConfig: TPropertyObject = config.getObject('robots');
if (!isIRobotsConfig(robotsConfig)) throw new Error('Robots config is not valid');

const expressConfig: TPropertyObject = config.getObject('express');
if (!isICommonsExpressConfig(expressConfig)) throw new Error('Express config is not valid');
if (args.hasProperty('port', 'number')) expressConfig.port = args.getNumber('port');

const uiConfig: TPropertyObject = config.getObject('ui');
if (!isIUiConfig(uiConfig)) throw new Error('UI config is not valid');

const list: List = new List(`${__dirname}/../../config/lists.json`);
const expiry: Expiry = new Expiry(`${__dirname}/../../config/expiry.json`);

const ex: express.Express = express();
const server: http.Server = http.createServer(ex);

const expressServer: ExpressServer = new ExpressServer(
		ex,
		server,
		expressConfig
);
expressServer.init();

// tslint:disable-next-line:no-unused-expression
new RestServer(
		expressServer,
		database,
		config
);

const live: LiveServer = new LiveServer(
		expressServer,
		config
);

const tracker: Tracker = new Tracker(
		database,
		live
);

const crawl: CrawlServer = new CrawlServer(
		database,
		crawlConfig,
		list,
		[ ServerParser, HtmlParser, JpegParser, SpellingParser, BadWordsParser, PhpErrorParser, AspErrorParser, UomBlockedParser ],
		parsingConfig,
		robotsConfig,
		tracker
);

// tslint:disable-next-line:no-unused-expression
new UiServer(
		expressServer,
		uiConfig,
		expressConfig
);

const maintenance: MaintenanceServer = new MaintenanceServer(
		`${__dirname}/../../config/schedule.json`,
		expiry,
		list,
		database,
		crawl
);

class Hydra {
	// tslint:disable:no-shadowed-variable
	constructor(
			private database: DatabaseService,
			private expressServer: ExpressServer,
			private tracker: Tracker,
			private crawl: CrawlServer,
			private maintenance: MaintenanceServer
	) {}
	// tslint:enable:no-shadowed-variable
	
	public async start(): Promise<void> {
		await this.database.init();
		
		await this.expressServer.listen();
		
		this.maintenance.start();
		
		this.tracker.start();
		
		await this.crawl.start();
	}
}

(async () => {
	const hydra: Hydra = new Hydra(
			database,
			expressServer,
			tracker,
			crawl,
			maintenance
	);
	await hydra.start();

	CommonsOutput.doing('About to exit. Counting down');
	for (let i = 5; i-- > 0;) {
		CommonsOutput.progress(i);
		
		try {
			await CommonsAsync.timeout(1000);
		} catch (ex) {
			// do nothing
		}
	}
	CommonsOutput.success();
	
	CommonsOutput.alert('Shutting down now');
	
	process.exit(0);
	
})();
