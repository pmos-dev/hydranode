import * as http from 'http';

import { TPropertyObject } from 'tscommons-core';
import { CommonsConfig } from 'tscommons-config';

import { CommonsConfigFile } from 'nodecommons-config';
import { CommonsOutput } from 'nodecommons-cli';
import { isICommonsCredentials } from 'nodecommons-database';

import { List } from '../../classes/list';
import { Crawler } from '../../classes/crawler';

import { DatabaseService } from '../../services/database.service';

import { EStatus, fromEStatus } from '../../enums/estatus';

const config: CommonsConfig = new CommonsConfigFile(`${__dirname}/../../../config/hydra.json`);

const credentials: TPropertyObject = config.getObject('database');
if (!isICommonsCredentials(credentials)) throw new Error('Credentials are not valid');
const database: DatabaseService = new DatabaseService(credentials);

const list: List = new List(`${__dirname}/../../../config/lists.json`);

(async () => {
	await database.init();
	const dbo: any = database.getRawDatabase();

	let tally: number = 0;
	let found: number = 0;
	
	CommonsOutput.doing('Search for whitelist dead domains');
	
	const results: any = await dbo.collection('urls')
			.aggregate([
			 		{ $match: { status: fromEStatus(EStatus.DEAD) } },
			 		{ $group: { _id: '$domain', tally: { $sum: 1 } } }
			]);

	const domains: string[] = [];
	while (await results.hasNext()) {
		const row = await results.next();

		if (!list.isWhite(`http://${row._id}/`)) continue;
		
		if ((tally % 10) === 0) CommonsOutput.progress(tally);

		domains.push(row._id);
		tally++;
	}
	
	CommonsOutput.result(tally);

	CommonsOutput.doing('Challenging DNS');
	tally = 0; found = 0;
	const undead: string[] = [];
	for (const domain of domains) {
		if ((tally % 10) === 0) CommonsOutput.progress(`${tally}, ${found}`);
		tally++;

		const url: string = `http://${domain}/`;

		try {
			//await Crawler.resolve(domain);
			await Crawler.request(http, url, 3000, 1);
		} catch (ex) { continue; }

		undead.push(domain);
		found++;
	}

	console.log(undead);

	process.exit(0);
})();
