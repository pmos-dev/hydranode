import { TPropertyObject } from 'tscommons-core';
import { CommonsConfig } from 'tscommons-config';

import { CommonsConfigFile } from 'nodecommons-config';
import { CommonsOutput } from 'nodecommons-cli';
import { isICommonsCredentials } from 'nodecommons-database';

import { List } from '../../classes/list';

import { DatabaseService } from '../../services/database.service';

import { EStatus, fromEStatus } from '../../enums/estatus';

const list: List = new List(`${__dirname}/../../../config/lists.json`);

const config: CommonsConfig = new CommonsConfigFile(`${__dirname}/../../../config/hydra.json`);

const credentials: TPropertyObject = config.getObject('database');
if (!isICommonsCredentials(credentials)) throw new Error('Credentials are not valid');
const database: DatabaseService = new DatabaseService(credentials);

(async () => {
	await database.init();
	const dbo: any = database.getRawDatabase();
	let tally: number = 0;
	let found: number = 0;
	
	CommonsOutput.doing('Searching for all whitelisted DONE URLs');
	
	const results: any = await dbo.collection('urls')
			.find(
					{
							status: fromEStatus(EStatus.DONE)
					},
					{ url: 1 }
			);

	const urls: string[] = [];
	tally = 0;
	
	while (await results.hasNext()) {
		if ((tally % 1000) === 0) CommonsOutput.progress(`${tally}, ${found}`);
		tally++;

		const row = await results.next();
		if (!list.isWhite(row.url)) continue;

		urls.push(row.url);
		found++;
	}
	CommonsOutput.result(found);
	
	const regex: RegExp = new RegExp('^[a-z]{1,16}://([-a-z0-9.]+)/');
	
	CommonsOutput.doing('Aggregating domains');
	tally = 0; found = 0;
	const domains: string[] = [];
	for (const url of urls) {
		if ((tally % 1000) === 0) CommonsOutput.progress(`${tally}, ${found}`);
		tally++;
		
		const matches: RegExpExecArray|null = regex.exec(url);
		if (matches === null) continue;
		
		const domain: string = matches[1];
		
		if (!domains.includes(domain)) {
			domains.push(domain);
			found++;
		}
	}
	CommonsOutput.result(found);
	
	CommonsOutput.doing('Searching for all whitelisted domains without root URLs');

	tally = 0; found = 0;
	const missing: string[] = [];
	for (const domain of domains) {
		if ((tally % 100) === 0) CommonsOutput.progress(`${tally}, ${found}`);
		tally++;

		const count: number = await dbo.collection('urls')
				.find(
						{ $and: [
							{ status: fromEStatus(EStatus.DONE) },
							{ domain: domain },
							{ $or: [
									{ url: `http://${domain}/` },
									{ url: `https://${domain}/` }
							] }
						] },
						{ url: 1 }
				).count();

		if (count === 0) {
			missing.push(domain);
			found++;
		}
	}
	CommonsOutput.result(found);

	CommonsOutput.doing('Seeding into queue');
	tally = 0;
	for (const domain of missing) {
		{
			const url = `http://${domain}/`;
			const outcome: boolean = await database.queue(url);
			if (outcome) {
				tally++;
				CommonsOutput.progress(tally);
			}
		}
		{
			const url = `https://${domain}/`;
			const outcome: boolean = await database.queue(url);
			if (outcome) {
				tally++;
				CommonsOutput.progress(tally);
			}
		}
	}
	CommonsOutput.result(tally);

	process.exit(0);

})();
