import { TPropertyObject } from 'tscommons-core';
import { CommonsConfig } from 'tscommons-config';

import { CommonsConfigFile } from 'nodecommons-config';
import { CommonsOutput } from 'nodecommons-cli';
import { isICommonsCredentials } from 'nodecommons-database';

import { DatabaseService } from '../../services/database.service';

const config: CommonsConfig = new CommonsConfigFile(`${__dirname}/../../../config/hydra.json`);
const credentials: TPropertyObject = config.getObject('database');
if (!isICommonsCredentials(credentials)) throw new Error('Credentials are not valid');
const database: DatabaseService = new DatabaseService(credentials);

(async () => {
	await database.init();
	const dbo: any = database.getRawDatabase();

	CommonsOutput.doing(`Enumerating personalpages.manchester.ac.uk URLs`);
	const results = await (dbo.collection('urls')
			.find({
					domain: 'personalpages.manchester.ac.uk'
			}, { url: 1, _id: 0 }));

	let tally: number = 0;
	const urls: string[] = [];
	while (await results.hasNext()) {
		if ((++tally % 100) === 0) CommonsOutput.progress(tally);
		
		const row = await results.next();

		urls.push(row.url.toLowerCase());
	}
	CommonsOutput.result(urls.length);

	CommonsOutput.doing(`Extracting paths`);
	const regex: RegExp = /^http(?:s)?:\/\/personalpages\.manchester\.ac\.uk\/((?:postgrad|staff|student)\/[^/]+)(?:\/|$)/;
	const paths: string[] = urls
			.filter((url: string): boolean => regex.test(url))
			.map((url: string): string => {
				const match: RegExpExecArray|null = regex.exec(url);
				
				if (match === null || match === undefined) throw new Error(`Unable to match for: ${url}`);
				
				return match[1];
			})
			.filter((value: string, index: number, array: string[]): boolean => array.indexOf(value) === index);

	CommonsOutput.success();
	
	paths.sort();

	for (const path of paths) console.log(path);
	
	process.exit(0);
})();
