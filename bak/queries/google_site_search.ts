import { TPropertyObject } from 'tscommons-core';
import { CommonsConfig } from 'tscommons-config';

import { CommonsConfigFile } from 'nodecommons-config';
import { CommonsOutput } from 'nodecommons-cli';
import { isICommonsCredentials } from 'nodecommons-database';

import { DatabaseService } from '../../services/database.service';

import { EStatus, fromEStatus } from '../../enums/estatus';

const config: CommonsConfig = new CommonsConfigFile(`${__dirname}/../../../config/hydra.json`);

const credentials: TPropertyObject = config.getObject('database');
if (!isICommonsCredentials(credentials)) throw new Error('Credentials are not valid');
const database: DatabaseService = new DatabaseService(credentials);

const regex: RegExp = new RegExp('^http[s]?://([^/]+)/');

(async () => {
	await database.init();
	const dbo: any = database.getRawDatabase();

	CommonsOutput.doing(`Enumerating `);
	const results = await (dbo.collection('urls')
			.find({
					status: { $ne: fromEStatus(EStatus.ARCHIVED) },
					'links.form': /\/google_site_search\.php$/
			}, { url: 1, _id: 0 }));

	let tally: number = 0;
	const domains: string[] = [];
	while (await results.hasNext()) {
		if ((++tally % 100) === 0) CommonsOutput.progress(`${tally}, ${domains.length}`);

		const row = await results.next();

		const result: RegExpExecArray|null = regex.exec(row.url);
		if (result === null) {
			console.log(row.url);
			process.exit(1);
			throw new Error('');	// typescript
		}
		
		const domain: string|null = result[1];
		if (domain === null) {
			console.log(row.url);
			process.exit(1);
		}

		if (!domains.includes(domain)) domains.push(domain);
	}
	CommonsOutput.result(domains.length);

	domains.sort();

	for (const domain of domains) {
		console.log(domain);
	}

	process.exit(0);
})();
