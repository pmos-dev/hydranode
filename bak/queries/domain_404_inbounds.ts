import { CommonsType } from 'tscommons-core';
import { TPropertyObject } from 'tscommons-core';
import { CommonsString } from 'tscommons-core';
import { CommonsConfig } from 'tscommons-config';

import { CommonsConfigFile } from 'nodecommons-config';
import { CommonsOutput } from 'nodecommons-cli';
import { CommonsArgs } from 'nodecommons-cli';
import { isICommonsCredentials } from 'nodecommons-database';

import { DatabaseService } from '../../services/database.service';

import { EStatus, fromEStatus, toEStatus } from '../../enums/estatus';

const config: CommonsConfig = new CommonsConfigFile(`${__dirname}/../../../config/hydra.json`);
const args: CommonsArgs = new CommonsArgs();

if (!args.hasProperty('domain', 'string')) throw new Error('No domain supplied');
const domain: string = args.getString('domain');

const excludeInternal: boolean = args.hasAttribute('exclude-internal');

const credentials: TPropertyObject = config.getObject('database');
if (!isICommonsCredentials(credentials)) throw new Error('Credentials are not valid');
const database: DatabaseService = new DatabaseService(credentials);

let tally: number = 0;

(async () => {
	await database.init();
	const dbo: any = database.getRawDatabase();

	CommonsOutput.doing(`Enumerating 404 URLs for domain ${domain}`);
	const results = await (dbo.collection('urls')
			.find({
					status: { $ne: fromEStatus(EStatus.ARCHIVED) },
					domain: domain,
					statusCode: 404
			}, { url: 1 }));

	tally = 0;
	const urls: string[] = [];
	while (await results.hasNext()) {
		if ((++tally % 100) === 0) CommonsOutput.progress(tally);
		
		const row = await results.next();

		urls.push(row.url);
	}
	CommonsOutput.result(urls.length);
	
	const internal: RegExp = new RegExp(`^(http|https)://${CommonsString.regexEscapeString(domain)}[:/]`);

	CommonsOutput.doing('Enumerating inbound referrers');
	tally = 0;
	const inbound: { url: string; outgoing: string }[] = [];
	for (const url of urls) {
		if ((++tally % 100) === 0) CommonsOutput.progress(`${tally}, found ${inbound.length}`);
		
		const results2 = await (dbo.collection('links')
				.find({
						outgoing: url
				}, { url: 1 }));
		
		while (await results2.hasNext()) {
			const row = await results2.next();
			if (excludeInternal && internal.test(row['url'])) continue;
			
			inbound.push({ url: row['url'], outgoing: url });
		}
	}

	CommonsOutput.result(inbound.length);

	CommonsOutput.doing('Pruning FAILED and ARCHIVED urls');
	tally = 0;
	const pruned: { url: string; outgoing: string }[] = [];
	for (const url of inbound) {
		if ((++tally % 100) === 0) CommonsOutput.progress(`${tally}, pruned to ${pruned.length}`);
		const results2 = await (dbo.collection('urls')
				.find({
						url: url.url
				}, { status: 1 }));

		let skip: boolean = false;
		while (await results2.hasNext()) {
			const row = await results2.next();
			const status: EStatus|undefined = toEStatus(row.status);
			if (status === undefined) {
				console.log('Unable to derived status', row.status);
				process.exit(1);
			}
			if ([ EStatus.FAILED, EStatus.ARCHIVED ].includes(status!)) skip = true;
		}
		
		if (skip) continue;
		
		pruned.push(url);
	}
	CommonsOutput.result(pruned.length);
	
	const grouped: { [url: string]: string[] } = {};

	for (const url of inbound) {
		if (!CommonsType.hasProperty(grouped, url.outgoing)) CommonsType.assertObject(grouped)[url.outgoing] = [];
		grouped[url.outgoing].push(url.url);
	}

	for (const url of Object.keys(grouped)
			.sort((a: string, b: string): number => {
				if (a < b) return -1;
				if (a > b) return 1;
				return 0;
			})) {
		console.log(url);
		for (const inbound2 of grouped[url]) console.log(` - ${inbound2}`);
	}
	
	process.exit(0);
})();
