import { TPropertyObject } from 'tscommons-core';
import { CommonsTree, TSegmentTree } from 'tscommons-core';
import { CommonsConfig } from 'tscommons-config';

import { CommonsConfigFile } from 'nodecommons-config';
import { CommonsOutput } from 'nodecommons-cli';
import { CommonsArgs } from 'nodecommons-cli';
import { isICommonsCredentials } from 'nodecommons-database';

import { DatabaseService } from '../../services/database.service';

import { EStatus, fromEStatus } from '../../enums/estatus';

const config: CommonsConfig = new CommonsConfigFile(`${__dirname}/../../../config/hydra.json`);
const args: CommonsArgs = new CommonsArgs();

if (!args.hasProperty('domain', 'string')) throw new Error('No domain supplied');
const domain: string = args.getString('domain');

const credentials: TPropertyObject = config.getObject('database');
if (!isICommonsCredentials(credentials)) throw new Error('Credentials are not valid');
const database: DatabaseService = new DatabaseService(credentials);

(async () => {
	await database.init();
	const dbo: any = database.getRawDatabase();
	let tally: number = 0;
	
	CommonsOutput.doing(`Enumerating DONE 2xx URLs for ${domain}`);
	const results: any = await dbo.collection('urls').find(
			{ $and: [
					{ domain: domain },
					{ status: fromEStatus(EStatus.DONE) },
					{ statusCode: { $gte: 200 } },
					{ statusCode: { $lt: 300 } }
			]}, {
					url: 1
			}
	);

	const urls: string[] = [];
	tally = 0;
	while (await results.hasNext()) {
		tally++;
		if ((tally % 100) === 0) CommonsOutput.progress(tally);

		const row = await results.next();
		urls.push(row.url);
	}
	CommonsOutput.success(tally);

	CommonsOutput.doing('Mapping paths for URLs');
	const splits: string[][] = [];
	for (const url of urls) {
		const regex: RegExp = new RegExp(`^http(?:s?)://${domain}(?::[0-9]+)?(/.*)$`);
		const match: RegExpExecArray|null = regex.exec(url);
		
		if (match === null) throw new Error(`Unable to match for ${url}`);

		let path: string = match[1];
		if (path.indexOf('?') !== -1) path = path.split('?')[0];
		
		const segments: string[] = path.split('/');
		segments.pop();	// remove the tail
		
		splits.push(segments);
	}
	CommonsOutput.success();

	CommonsOutput.doing('Building tree');
	const tree: TSegmentTree = CommonsTree.stringSegmentArrayToTree(splits);
	CommonsOutput.success();

	console.log(tree);
	
	process.exit(0);
})();
