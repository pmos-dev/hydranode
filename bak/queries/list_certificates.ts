import * as https from 'https';

import { TPropertyObject } from 'tscommons-core';
import { CommonsConfig } from 'tscommons-config';

import { CommonsConfigFile } from 'nodecommons-config';
import { CommonsOutput } from 'nodecommons-cli';
import { isICommonsCredentials } from 'nodecommons-database';

import { DatabaseService } from '../../services/database.service';

import { EStatus, fromEStatus } from '../../enums/estatus';

const config: CommonsConfig = new CommonsConfigFile(`${__dirname}/../../../config/hydra.json`);

const credentials: TPropertyObject = config.getObject('database');
if (!isICommonsCredentials(credentials)) throw new Error('Credentials are not valid');
const database: DatabaseService = new DatabaseService(credentials);

const getCertificateDate = (url: string): Promise<Date|undefined> => {
	return new Promise<Date|undefined>((resolve, _reject): void => {
		try {
			const req: any = https.request(url, (res) => {
				res.on('data', (_) => {
					// do nothing
				});
				res.on('end', () => {
					const certificate: any = req.connection.getPeerCertificate();
					if (!certificate) return resolve(undefined);
					
					return resolve(new Date(certificate.valid_to));
				});
			});
			req.setHeader('User-Agent', 'Mozilla/5.0 (compatible; UoM Hydra)'),
			req.setHeader('Cache-Control', 'no-cache');
			req.setHeader('Pragma', 'no-cache');
			req.on('error', (_: any) => {
				return resolve(undefined);
			});
			req.end();
		} catch (ex) { return resolve(undefined); }
	});
};

(async () => {
	await database.init();
	const dbo: any = database.getRawDatabase();

	CommonsOutput.doing(`Enumerating UoM owned domains`);
	const results = await dbo.collection('domains')
			.find(
					{ $or: [
						{ domain: /(manchester|man)\.ac\.uk$/ },
						{ ip: /^((130\.88\.)|(10\.99\.))/ }
					] },
					{ domain: 1 }
			);

	const domains: string[] = [];
	while (await results.hasNext()) {
		const row = await results.next();
		domains.push(row.domain);
	}
	CommonsOutput.success(domains.length);
	
	CommonsOutput.doing(`Fetching all HTTPS URLs for domains`);
	const results2 = await dbo.collection('urls')
			.find(
				{ $and: [
					{ status: { $ne: fromEStatus(EStatus.ARCHIVED) } },
					{ domain: { $in: domains } },
					{ url: /^https:\/\// }
				] },
				{ url: 1 }
			);

	const urls: string[] = [];
	while (await results2.hasNext()) {
		const row = await results2.next();
		urls.push(row.url);
	}
	CommonsOutput.success(urls.length);

	const pick: string[] = [];
	CommonsOutput.doing(`Picking up to 5 random URLs for each domain`);
	for (const domain of domains) {
		if (0 === (pick.length % 10)) CommonsOutput.progress(pick.length);
		
		const regex2: RegExp = new RegExp(`^https://${domain.replace('.', '\.')}/`);
		const pool: string[] = urls
				.filter((url: string): boolean => regex2.test(url));
		
		for (let i = 5; i-- > 0;) {
			if (pool.length === 0) break;

			const pivot: number = Math.floor(Math.random() * pool.length);
			pick.push(...pool.splice(pivot, 1));
		}
	}
	CommonsOutput.success(pick.length);

	CommonsOutput.doing(`Fetching certificates for each URL`);
	const expiry: Map<string, Date> = new Map<string, Date>();
	const regex: RegExp = new RegExp('^https://([^/]+)/');
	
	let tally: number = 0;
	for (const url of pick) {
		if (0 === (tally++ % 10)) CommonsOutput.progress(tally);
		
		const array: RegExpExecArray|null = regex.exec(url);
		if (!array || array.length === 0) {
			CommonsOutput.error(`Unable to match domain for ${url}`);
			process.exit(1);
		}
		
		const domain: string = array![1];
		if (expiry.has(domain)) continue;
		
		try {
			const expires: Date|undefined = await getCertificateDate(url);
			if (expires) expiry.set(domain, expires);
		} catch (ex) {
			// ignore
		}
	}
	CommonsOutput.success(expiry.size);
	
	for (const domain of expiry.keys()) {
		console.log(`${domain},${expiry.get(domain)}`);
	}
	
	process.exit(0);
	
})();
