import { TPropertyObject } from 'tscommons-core';
import { CommonsConfig } from 'tscommons-config';

import { CommonsConfigFile } from 'nodecommons-config';
import { CommonsOutput } from 'nodecommons-cli';
import { CommonsArgs } from 'nodecommons-cli';
import { isICommonsCredentials } from 'nodecommons-database';

import { List } from '../../classes/list';

import { DatabaseService } from '../../services/database.service';

import { EStatus, fromEStatus } from '../../enums/estatus';

const list: List = new List(`${__dirname}/../../../config/lists.json`);

const config: CommonsConfig = new CommonsConfigFile(`${__dirname}/../../../config/hydra.json`);
const args: CommonsArgs = new CommonsArgs();

const credentials: TPropertyObject = config.getObject('database');
if (!isICommonsCredentials(credentials)) throw new Error('Credentials are not valid');
const database: DatabaseService = new DatabaseService(credentials);

const forIp: boolean = args.hasAttribute('ip');
const forDomain: boolean = args.hasAttribute('domain');
const forWhitelist: boolean = args.hasAttribute('whitelist');

(async () => {
	await database.init();
	const dbo: any = database.getRawDatabase();

	const domains: string[] = [];
	
	if (forIp) {
		CommonsOutput.doing('Enumerating internal domains');
		const results = await (dbo.collection('domains')
				.find({
						ip: /^(10|(172\.((1[6-9])|(2[0-9])|(3[01])))|(192\.168)|(130\.88))\./
				}, { domain: 1 }));
		
		while (await results.hasNext()) {
			const row = await results.next();
	
			if (/[^.-a-z0-9]/i.test(row.domain)) continue;
			if (/^\./.test(row.domain)) continue;
			if (/\.$/.test(row.domain)) continue;

			if (!domains.includes(row.domain)) domains.push(row.domain);
		}
		CommonsOutput.result(domains.length);
	}

	if (forWhitelist) {
		CommonsOutput.doing('Enumerating non-archived whitelisted URL domains');
		const results = await (dbo.collection('urls')
				.find({
						status: { $ne: fromEStatus(EStatus.ARCHIVED) }
				}, { domain: 1 }));
		
		let tally: number = 0;
		while (await results.hasNext()) {
			if ((++tally % 100) === 0) CommonsOutput.progress(`${tally}, ${domains.length}`);

			const row = await results.next();
			if (!list.isWhite(row.url)) continue;
	
			if (/[^.-a-z0-9]/i.test(row.domain)) continue;
			if (/^\./.test(row.domain)) continue;
			if (/\.$/.test(row.domain)) continue;

			if (!domains.includes(row.domain)) domains.push(row.domain);
		}
		CommonsOutput.result(domains.length);
	}

	if (forDomain) {
		CommonsOutput.doing('Enumerating known owned domains');
		const results = await (dbo.collection('domains')
				.find({
						domain: /(^|[.])(manchester|man|umist|mbs)\.ac\.uk$/
				}, { domain: 1 }));
		
		while (await results.hasNext()) {
			const row = await results.next();
	
			if (/[^.-a-z0-9]/i.test(row.domain)) continue;
			if (/^\./.test(row.domain)) continue;
			if (/\.$/.test(row.domain)) continue;

			if (!domains.includes(row.domain)) domains.push(row.domain);
		}
		CommonsOutput.result(domains.length);
	}

	CommonsOutput.doing('Compacting domains');

	const regex: RegExp = new RegExp('^www\.(.+)$');
	const compact: string[] = [];
	for (const domain of domains
			.map((d: string): string => {
				const result: RegExpExecArray|null = regex.exec(d);
				if (result === null) return d;
				return result[1];
			})
	) {
		if (!compact.includes(domain)) compact.push(domain);
	}

	compact.sort();

	for (const domain of compact) {
		console.log(domain);
	}

	process.exit(0);
})();
