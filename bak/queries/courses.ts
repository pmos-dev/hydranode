import { TPropertyObject } from 'tscommons-core';
import { CommonsConfig } from 'tscommons-config';

import { CommonsConfigFile } from 'nodecommons-config';
import { CommonsOutput } from 'nodecommons-cli';
import { isICommonsCredentials } from 'nodecommons-database';

import { DatabaseService } from '../../services/database.service';

const config: CommonsConfig = new CommonsConfigFile(`${__dirname}/../../../config/hydra.json`);
const credentials: TPropertyObject = config.getObject('database');
if (!isICommonsCredentials(credentials)) throw new Error('Credentials are not valid');
const database: DatabaseService = new DatabaseService(credentials);

let tally: number = 0;

const courses: Map<number, string[]> = new Map<number, string[]>();

(async () => {
	await database.init();
	const dbo: any = database.getRawDatabase();

	CommonsOutput.doing(`Enumerating UG URLs`);
	const results = await (dbo.collection('urls')
			.find({
					domain: 'www.manchester.ac.uk',
					url: /\/study\/undergraduate\/courses\/20[0-9]{2}\/[0-9]+\//
			}, { url: 1, title: 1, _id: 0 }));

	tally = 0;
	const urls: {
			url: string;
			title: string;
	}[] = [];
	while (await results.hasNext()) {
		if ((++tally % 100) === 0) CommonsOutput.progress(tally);
		
		const row = await results.next();

		urls.push({ url: row.url, title: row.title });
	}
	CommonsOutput.result(urls.length);

	const regex: RegExp = /courses\/20[0-9]{2}\/([0-9]+)/;
	const regex2: RegExp = /^(.+) \(20[0-9]{2} entry\) \| The University of Manchester$/;
	
	CommonsOutput.doing(`Compacting`);
	for (const row of urls) {
		if (row.title === undefined) continue;
		if (row.title.match(/(301|302|303|304|305|306|307|400|401|403|404|407|500) /)) continue;
		if (row.title.match(/Course profile not found/)) continue;
		
		const outcome: RegExpExecArray|null = regex.exec(row.url);
		if (!outcome) {
			console.log(`Unable to match ${row.url}`);
			process.exit(1);
		}
		
		const outcome2: RegExpExecArray|null = regex2.exec(row.title);
		if (outcome2) row.title = outcome2![1];
		
		const plan: number = parseInt(outcome![1]!, 10);
		if (!courses.has(plan)) courses.set(plan, []);
		
		if (!courses.get(plan)!.includes(row.title)) courses.get(plan)!.push(row.title);
	}
	CommonsOutput.result(0);
	
	console.log(courses);
	
	process.exit(0);
})();
