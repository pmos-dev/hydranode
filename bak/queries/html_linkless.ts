import { TPropertyObject } from 'tscommons-core';
import { CommonsConfig } from 'tscommons-config';

import { CommonsConfigFile } from 'nodecommons-config';
import { CommonsOutput } from 'nodecommons-cli';
import { isICommonsCredentials } from 'nodecommons-database';

import { List } from '../../classes/list';

import { DatabaseService } from '../../services/database.service';

import { EStatus, fromEStatus } from '../../enums/estatus';

const config: CommonsConfig = new CommonsConfigFile(`${__dirname}/../../../config/hydra.json`);

const credentials: TPropertyObject = config.getObject('database');
if (!isICommonsCredentials(credentials)) throw new Error('Credentials are not valid');
const database: DatabaseService = new DatabaseService(credentials);

const list: List = new List(`${__dirname}/../../../config/lists.json`);

(async () => {
	await database.init();
	const dbo: any = database.getRawDatabase();

	let tally: number = 0;
	let found: number = 0;
	
	CommonsOutput.doing('Searching for whitelisted text/html DONE with status code 200');
	
	const results: any = await dbo.collection('urls')
			.find(
					{
							status: fromEStatus(EStatus.DONE),
							statusCode: 200,
							'headers.content-type': /^text\/html/
					},
					{ url: 1 }
			);

	const urls: string[] = [];
	tally = 0;
	
	while (await results.hasNext()) {
		if ((tally % 100) === 0) CommonsOutput.progress(tally);

		const row = await results.next();
		if (!list.isWhite(row.url)) continue;

		urls.push(row.url);

		tally++;
	}
	CommonsOutput.result(tally);
	
	CommonsOutput.doing(`Searching for linkless matches`);
	const matches: string[] = [];
	tally = 0; found = 0;
	for (const url of urls) {
		tally++;
		if ((tally % 10) === 0) CommonsOutput.progress(`${tally}, ${found}`);

		const results2: any = await (dbo.collection('links')
				.find(
						{ url: url },
						{ _id: 1 }
				))
				.limit(1);
		
		let links: number = 0;
		while (await results2.hasNext()) {
			await results2.next();
			links++;
		}

		if (links > 0) continue;

		matches.push(url);
		found++;
	}
	CommonsOutput.result(found);

	for (const url of matches) {
		console.log(url);
	}
	
	process.exit(0);
})();
