import { TPropertyObject } from 'tscommons-core';
import { CommonsConfig } from 'tscommons-config';

import { CommonsConfigFile } from 'nodecommons-config';
import { CommonsOutput } from 'nodecommons-cli';
import { CommonsArgs } from 'nodecommons-cli';
import { isICommonsCredentials } from 'nodecommons-database';

import { DatabaseService } from '../../services/database.service';

import { EStatus, fromEStatus } from '../../enums/estatus';

const config: CommonsConfig = new CommonsConfigFile(`${__dirname}/../../../config/hydra.json`);
const args: CommonsArgs = new CommonsArgs();

const credentials: TPropertyObject = config.getObject('database');
if (!isICommonsCredentials(credentials)) throw new Error('Credentials are not valid');
const database: DatabaseService = new DatabaseService(credentials);

const threshold: number = args.getNumberOrUndefined('threshold') || 1000;
const length: number = args.getNumberOrUndefined('length') || 35;
const fixedDomain: string|undefined = args.getStringOrUndefined('domain');

const includeDone: boolean = args.hasAttribute('include-done');
const includeFailed: boolean = args.hasAttribute('include-failed');
const includeDisallowed: boolean = args.hasAttribute('include-disallowed');

(async () => {
	await database.init();
	const dbo: any = database.getRawDatabase();

	const status: EStatus[] = [ EStatus.QUEUED ];
	if (includeDone) status.push(EStatus.DONE);
	if (includeFailed) status.push(EStatus.FAILED);
	if (includeDisallowed) status.push(EStatus.DISALLOWED);

	let tally: number = 0;
	let found: number = 0;
	
	CommonsOutput.doing('Enumerating domains');
	
	const matches: string[] = [];

	if (fixedDomain) matches.push(fixedDomain);
	else {
		const results: any = await dbo.collection('domains').find();
		const domains: string[] = [];
		
		try {
			tally = 0;
			
			while (await results.hasNext()) {
				if ((tally % 100) === 0) CommonsOutput.progress(tally);
	
				const row = await results.next();
	
				domains.push(row.domain);
	
				tally++;
			}
		} catch (err) {
			if (err.code !== 43) throw err;
		}
		
		CommonsOutput.result(tally);
	
		CommonsOutput.doing(`Finding domains with more than ${threshold} URLs`);
		
		tally = 0; found = 0;
		for (const domain of domains) {
			tally++;
			if ((tally % 10) === 0) CommonsOutput.progress(`${tally}, ${found}`);
	
			const results2: any = await (dbo.collection('urls')
					.find({
							status: { $ne: fromEStatus(EStatus.ARCHIVED) },
							domain: domain
					})
					.count()
			);
			if (results2 < threshold) continue;
	
			matches.push(domain);
			found++;
		}
		CommonsOutput.result(found);
	}
	
	CommonsOutput.doing(`Enumerating URLs with identical first ${length} path characters`);
	
	tally = 0; found = 0;
	let urls: { url: string; tally: number }[] = [];
	for (const domain of matches) {
		tally++;
		if ((tally % 10) === 0) CommonsOutput.progress(`${tally}, ${found}`);

		const relativeLength: number = length + 'http://'.length + '/'.length + domain.length;
		
		const results2: any = await (dbo.collection('urls')
				.aggregate([
						{ $match: {
							$and: [
								{ status: { $in: status } },
								{ domain: domain }
							]
						} },
						{ $group: {
								_id: { $substr: [ '$url', 0, relativeLength ] },
								tally: { $sum: 1 }
						} },
						{ $sort: { tally: -1 } }
				]));

		while (await results2.hasNext()) {
			const row = await results2.next();
			if (row.tally < threshold) continue;
			urls.push({ url: row._id, tally: row.tally});
			found++;
		}
	}
	CommonsOutput.result(found);

	urls = urls
			.sort((a: { url: string; tally: number }, b: { url: string; tally: number }): number => {
				if (a.tally < b.tally) return -1;
				if (a.tally > b.tally) return 1;
				return 0;
			});
	
	for (const url of urls) {
		console.log(`${url.tally}: ${url.url}`);
	}
	
	process.exit(0);
})();
