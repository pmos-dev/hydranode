import { TPropertyObject } from 'tscommons-core';
import { CommonsString } from 'tscommons-core';
import { CommonsConfig } from 'tscommons-config';

import { CommonsConfigFile } from 'nodecommons-config';
import { CommonsOutput } from 'nodecommons-cli';
import { CommonsArgs } from 'nodecommons-cli';
import { isICommonsCredentials } from 'nodecommons-database';

import { DatabaseService } from '../../services/database.service';

import { EStatus, fromEStatus } from '../../enums/estatus';

const config: CommonsConfig = new CommonsConfigFile(`${__dirname}/../../../config/hydra.json`);
const args: CommonsArgs = new CommonsArgs();

if (!args.hasProperty('prefix', 'string')) throw new Error('No prefix supplied');
const prefix: string = args.getString('prefix');

const regexp: RegExp = new RegExp(`^${CommonsString.regexEscapeString(prefix)}`);

const credentials: TPropertyObject = config.getObject('database');
if (!isICommonsCredentials(credentials)) throw new Error('Credentials are not valid');
const database: DatabaseService = new DatabaseService(credentials);

let tally: number = 0;

(async () => {
	await database.init();
	const dbo: any = database.getRawDatabase();

	CommonsOutput.doing(`Enumerating prefix URLs for ${prefix}`);
	const results = await (dbo.collection('urls')
			.find({
					status: { $ne: fromEStatus(EStatus.ARCHIVED) },
					url: regexp
			}, { url: 1 }));

	tally = 0;
	const urls: string[] = [];
	while (await results.hasNext()) {
		if ((++tally % 100) === 0) CommonsOutput.progress(tally);
		
		const row = await results.next();

		urls.push(row.url);
	}
	CommonsOutput.result(urls.length);

	CommonsOutput.doing('Enumerating outgoing unique referrers');
	tally = 0;
	const sources: string[] = [];
	for (const url of urls) {
		if ((++tally % 100) === 0) CommonsOutput.progress(`${tally}, found ${sources.length}`);
		
		const results2 = await (dbo.collection('links')
				.find({
						outgoing: url
				}, { url: 1 }));
		
		while (await results2.hasNext()) {
			const row = await results2.next();
			if (!sources.includes(row['url'])) sources.push(row['url']);
		}
	}

	CommonsOutput.result(sources.length);

	console.log(sources);
	
	process.exit(0);
})();
