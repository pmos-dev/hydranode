import { TPropertyObject } from 'tscommons-core';
import { CommonsString } from 'tscommons-core';
import { CommonsConfig } from 'tscommons-config';

import { CommonsConfigFile } from 'nodecommons-config';
import { CommonsOutput } from 'nodecommons-cli';
import { CommonsArgs } from 'nodecommons-cli';
import { isICommonsCredentials } from 'nodecommons-database';

import { DatabaseService } from '../../services/database.service';

import { EStatus, fromEStatus } from '../../enums/estatus';

const config: CommonsConfig = new CommonsConfigFile(`${__dirname}/../../../config/hydra.json`);
const args: CommonsArgs = new CommonsArgs();

if (!args.hasProperty('domain', 'string')) throw new Error('No domain supplied');
const domain: string = args.getString('domain');

const excludeInternal: boolean = args.hasAttribute('exclude-internal');

const credentials: TPropertyObject = config.getObject('database');
if (!isICommonsCredentials(credentials)) throw new Error('Credentials are not valid');
const database: DatabaseService = new DatabaseService(credentials);

let tally: number = 0;

(async () => {
	await database.init();
	const dbo: any = database.getRawDatabase();

	CommonsOutput.doing(`Enumerating URLs for domain ${domain}`);
	const results = await (dbo.collection('urls')
			.find({
					status: { $ne: fromEStatus(EStatus.ARCHIVED) },
					domain: domain
			}, { url: 1 }));

	tally = 0;
	const urls: string[] = [];
	while (await results.hasNext()) {
		if ((++tally % 100) === 0) CommonsOutput.progress(tally);
		
		const row = await results.next();

		urls.push(row.url);
	}
	CommonsOutput.result(urls.length);
	
	const internal: RegExp = new RegExp(`^(http|https)://${CommonsString.regexEscapeString(domain)}[:/]`);

	CommonsOutput.doing('Enumerating inbound referrers');
	tally = 0;
	const inbound: { url: string; outgoing: string }[] = [];
	for (const url of urls) {
		if ((++tally % 100) === 0) CommonsOutput.progress(`${tally}, found ${inbound.length}`);
		
		const results2 = await (dbo.collection('links')
				.find({
						outgoing: url
				}, { url: 1 }));
		
		while (await results2.hasNext()) {
			const row = await results2.next();
			if (excludeInternal && internal.test(row['url'])) continue;
			
			inbound.push({ url: row['url'], outgoing: url });
		}
	}

	CommonsOutput.result(inbound.length);

	for (const url of inbound
			.sort((a: { url: string; outgoing: string }, b: { url: string; outgoing: string }): number => {
				if (a.url < b.url) return -1;
				if (a.url > b.url) return 1;
				return 0;
			})) {
		console.log(`"${url.url}","${url.outgoing}"`);
	}
	//console.log(inbound);
	
	process.exit(0);
})();
