import { TPropertyObject } from 'tscommons-core';
import { CommonsConfig } from 'tscommons-config';

import { CommonsConfigFile } from 'nodecommons-config';
import { CommonsOutput } from 'nodecommons-cli';
import { CommonsInput } from 'nodecommons-cli';
import { isICommonsCredentials } from 'nodecommons-database';

import { DatabaseService } from '../../services/database.service';

import { EStatus, fromEStatus } from '../../enums/estatus';

const config: CommonsConfig = new CommonsConfigFile(`${__dirname}/../../../config/hydra.json`);

const credentials: TPropertyObject = config.getObject('database');
if (!isICommonsCredentials(credentials)) throw new Error('Credentials are not valid');
const database: DatabaseService = new DatabaseService(credentials);

(async () => {
	await database.init();
	const dbo: any = database.getRawDatabase();

	const regex: RegExp = new RegExp('^(.+?)/$');
	
	const urls: string[] = (await CommonsInput.readAllStdIn())
			.map((s: string): string => s.trim())
			.filter((s: string): boolean => s !== '')
			.map((url: string): string => {
				const result: RegExpExecArray|null = regex.exec(url);
				if (result === null) return url;
				
				return result[1];
			});

	CommonsOutput.doing('Enumerating bad URLs');
	const results = await (dbo.collection('urls')
			.find(
					{ $and: [
							{ status: { $ne: fromEStatus(EStatus.ARCHIVED) } },
							{ $or: urls
									.map((url: string): {} => {
										const encoded: string = url
												.replace(/\./g, '\\.');
										
										return { url: new RegExp(encoded) };
									})
							}
					] },
					{ url: 1 }
			));
	
	const matches: string[] = [];
	while (await results.hasNext()) {
		const row = await results.next();
		matches.push(row.url);
	}
	CommonsOutput.success(matches.length);

	const map: Map<string, string[]> = new Map<string, string[]>();
	
	CommonsOutput.doing('Collecting source URLs');
	let tally: number = 0;
	for (const dest of matches) {
		CommonsOutput.progress(++tally);
		const results2 = await (dbo.collection('links')
				.find(
						{
							outgoing: dest
						},
						{ url: 1 }
				));
		
		while (await results2.hasNext()) {
			const row = await results2.next();
			
			if (!map.has(row.url)) map.set(row.url, []);
			map.get(row.url)!.push(dest);
		}
	}
	
	CommonsOutput.success(map.size);
	
	for (const src of map.keys()) {
		console.log(src);
		for (const dest of map.get(src)!) {
			console.log(`\t${dest}`);
		}
	}
	
	process.exit(0);
})();
