import { TPropertyObject } from 'tscommons-core';
import { CommonsConfig } from 'tscommons-config';

import { CommonsConfigFile } from 'nodecommons-config';
import { CommonsOutput } from 'nodecommons-cli';
import { CommonsArgs } from 'nodecommons-cli';
import { isICommonsCredentials } from 'nodecommons-database';

import { DatabaseService } from '../../services/database.service';

import { EStatus, fromEStatus } from '../../enums/estatus';

const config: CommonsConfig = new CommonsConfigFile(`${__dirname}/../../../config/hydra.json`);
const args: CommonsArgs = new CommonsArgs();

const credentials: TPropertyObject = config.getObject('database');
if (!isICommonsCredentials(credentials)) throw new Error('Credentials are not valid');
const database: DatabaseService = new DatabaseService(credentials);

const regex: string = args.getString('regex');

(async () => {
	await database.init();
	const dbo: any = database.getRawDatabase();

	let tally: number = 0;
	let found: number = 0;
	
	CommonsOutput.doing('Enumerating matching DONE URLs');
	
	const results: any = await dbo.collection('urls')
			.find({
					status: fromEStatus(EStatus.DONE),
					url: new RegExp(regex, 'i')
			});

	const matches: string[] = [];
	tally = 0; found = 0;
	while (await results.hasNext()) {
		tally++;
		if ((tally % 100) === 0) CommonsOutput.progress(`${tally}, ${found}`);

		const row = await results.next();

		matches.push(row.url);

		found++;
	}
	CommonsOutput.result(found);

	CommonsOutput.doing('Enumerating outgoing links');
	
	const results2: any = await dbo.collection('links')
			.find({ outgoing: { $in: matches } });

	const outgoings: string[] = [];
	tally = 0; found = 0;
	while (await results2.hasNext()) {
		tally++;
		if ((tally % 100) === 0) CommonsOutput.progress(tally);

		const row = await results2.next();

		outgoings.push(row.url);
	}
	CommonsOutput.result(tally);
	
	CommonsOutput.doing('Pruning back to DONE source URLs');
	const results3: any = await dbo.collection('urls')
			.find({
					status: fromEStatus(EStatus.DONE),
					url: { $in: outgoings }
			});

	const urls: string[] = [];
	tally = 0; found = 0;
	while (await results3.hasNext()) {
		tally++;
		if ((tally % 100) === 0) CommonsOutput.progress(tally);

		const row = await results3.next();

		urls.push(row.url);
	}
	CommonsOutput.result(tally);
	
	CommonsOutput.doing('Aggregating domains');
	const results4: any = await dbo.collection('urls')
			.find({
					status: fromEStatus(EStatus.DONE),
					url: { $in: urls }
			});

	const domains: string[] = [];
	tally = 0; found = 0;
	while (await results4.hasNext()) {
		tally++;
		if ((tally % 100) === 0) CommonsOutput.progress(`${tally}, ${domains.length}`);

		const row = await results4.next();

		if (!domains.includes(row.domain)) domains.push(row.domain);
	}
	CommonsOutput.result(domains.length);

	for (const domain of domains.sort()) {
		console.log(domain);
	}

	process.exit(0);
})();
