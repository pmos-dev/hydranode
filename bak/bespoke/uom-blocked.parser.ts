import { DatabaseService } from '../../services/database.service';

import { IParsingConfig } from '../../interfaces/iparsing-config';
import { IRequestOutcome } from '../../interfaces/irequest-outcome';

import { StringParser } from '../string.parser';

const BLOCKED_PARSER: string = '<h1>Web page blocked</h1>\\s+'
		+ '<p>Access to this web page is restricted by the University\'s acceptable usage policy\.</p>\\s+'
		+ '<p>If you feel you have a valid reason to visit this web page, or would like further advice, please contact the <a href="http://www\.itservices\.manchester\.ac\.uk/help">IT Support Centre</a>\.</p>\\s+'
		+ '<p>It would be helpful if you provide the following information when reporting this to the support centre:\\s+'
		+ '<ul>\\s+'
		+ '<li><b>Category: </b>(.+?)</li>\\s+'
		+ '<li><b>User or IP:';

export class UomBlockedParser extends StringParser {
	constructor(
			private url: string,
			outcome: IRequestOutcome,
			_config: IParsingConfig
	) {
		super(outcome.data);
	}

	public async parse(database: DatabaseService): Promise<void> {
		if (this.data === undefined) return;

		const singlified: string = this.data.substring(0, 4096).replace(/[\t\n\r]/g, ' ');
		
		const pattern: RegExp = new RegExp(BLOCKED_PARSER);

		const result: RegExpExecArray|null = pattern.exec(singlified);
		if (result === null) {
			await database.unsetData(this.url, 'uomBlocked');
		} else {
			await database.setData(this.url, 'uomBlocked', result[1]);
		}
	}

	public supports(_contentType: string, _isWhite: boolean): boolean {
		return true;	// everything
	}
	
	public links(): string[] {
		return [];
	}
}
