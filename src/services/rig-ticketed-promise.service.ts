
import { TEncoded, TEncodedObject } from 'tscommons-es-core';
import { CommonsRestClientService } from 'tscommons-es-rest';
import { ECommonsHttpMethod, TCommonsHttpRequestOptions } from 'tscommons-es-http';

import { commonsTicketedNodeGenericSubmitAndWaitAsPromise } from 'nodecommons-es-socket-io-ticketed';

export type TPromptAndKeepLoaded = {
		prompt: string;
		keepLoaded?: string;
};

export abstract class RigTicketedPromiseService {
	constructor(
			private restService: CommonsRestClientService,
			private socketIoUrl: string,
			private connectToSubmitDelay: number = 100
	) {}

	public submitAndWaitAsPromise<
			ResultT extends TEncoded = TEncoded,
			BodyT extends (TEncodedObject & TPromptAndKeepLoaded) = (TEncodedObject & TPromptAndKeepLoaded),
			ParamsT extends TEncodedObject = TEncodedObject,
			HeadersT extends TEncodedObject = TEncodedObject
	>(
			script: string,
			body: BodyT,
			checker: (data: unknown) => data is ResultT,
			params?: ParamsT|undefined,	// the params can be explicitly undefined (none) as well as optional
			headers?: HeadersT|undefined,	// the headers can be explicitly undefined (none) as well as optional
			options: TCommonsHttpRequestOptions = {}
	): Promise<ResultT> {
		return commonsTicketedNodeGenericSubmitAndWaitAsPromise<
			ResultT,
			BodyT,
			ParamsT,
			HeadersT
		>(
				this.socketIoUrl,
				'rig-marshall',
				this.restService,
				ECommonsHttpMethod.POST,
				script,
				body,
				checker,
				params,
				headers,
				options,
				this.connectToSubmitDelay
		);
	}
}
