import { CommonsInternalHttpClientImplementation } from 'nodecommons-es-http';

import { TQueueLength } from '../types/tqueue-length';

import { RigTicketRestService } from './rig-ticket-rest.service';

export class RigQueueLengthService {
	private restService: RigTicketRestService;

	constructor(
			url: string,
			key: string
	) {
		this.restService = new RigTicketRestService(
				new CommonsInternalHttpClientImplementation(),
				`${url}/tickets`,
				key
		);
	}

	public async getLength(): Promise<TQueueLength> {
		return await this.restService.length();
	}
}
