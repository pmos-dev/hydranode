import { CommonsFixedDuration, TPropertyObject, commonsTypeAttemptObject, commonsTypeHasPropertyNumber, commonsTypeHasPropertyStringOrUndefined, commonsTypeIsString } from 'tscommons-es-core';

import { CommonsInternalHttpClientImplementation } from 'nodecommons-es-http';

import { OllamaRestService } from './ollama-rest.service';
import { RigTicketedPromiseService, TPromptAndKeepLoaded } from './rig-ticketed-promise.service';

export type TSubjectivityResult = {
		score: number;
		reason?: string;
};
function isTSubjectivityResult(test: unknown): test is TSubjectivityResult {
	if (!commonsTypeHasPropertyNumber(test, 'score')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'reason')) return false;

	return true;
}

const GENERIC_PROMPT: string = `
The following text was extracted from a webpage as plain text:

\`\`\`\`\`\`\`\`\`\`\`\`\`\`\`\`\`\`\`
<<<TEXT>>>
\`\`\`\`\`\`\`\`\`\`\`\`\`\`\`\`\`\`\`

Generate a single numeric digit score <<<REASON>>>, using the following scale:
<<<SCALE>>>

Output the score and a single sentence justifying the reason for that score, using the JSON format:
{ "score": <score>, "reason": "<reason>"}

Do not include any other prefix or suffix explanatory text.
`.trim();

const OFFENCE_PROMPT: string = GENERIC_PROMPT
		.replace(
				'<<<REASON>>>',
				'for how offensive or controversial the text is'
		)
		.replace(
				'<<<SCALE>>>',
				`
0 = not offensive or controversial at all
1 = mildly offensive or controversial to a few people
2 = significantly offensive or controversial to most people
3 = extremely offensive or highly controversial, potentially racist, sexist, homophobic or promoting hatred.
				`.trim()
		);

const INTEREST_PROMPT: string = GENERIC_PROMPT
		.replace(
				'<<<REASON>>>',
				'for how interesting or enlightening the text is'
		)
		.replace(
				'SCALE',
				`
0 = purely function information with no further interest, such as opening times or directions
1 = information that is partially interesting or intriguing, such as informative descriptions
2 = opinion and commentary which is likely to be interesting to many people
3 = highly informative and interesting or enlightening commentary, which most people would rate highly.
				`.trim()
		);

export class RigSubjectivityScaleService extends RigTicketedPromiseService {
	constructor(
			url: string,
			key: string,
			private model: string
	) {
		super(
				new OllamaRestService(
						new CommonsInternalHttpClientImplementation(),
						`${url}/ollama`,
						key
				),
				url,
				100
		);
	}

	private async generateSubjectivityScore(
			text: string,
			prompt: string,
			keepAlive?: CommonsFixedDuration
	): Promise<TSubjectivityResult|undefined> {
		prompt = prompt
				.replace('<<<TEXT>>>', text);

		const body: TPromptAndKeepLoaded = { prompt: prompt };
		if (keepAlive) {
			const mins: number = keepAlive.minutes;
			if (mins < 1) throw new Error('keepAlive cannot be less than 1 minute');

			body.keepLoaded = `${Math.round(mins)}m`;
		}

		try {
			const response: string = await this.submitAndWaitAsPromise<
					string,
					TPromptAndKeepLoaded
			>(
					`/generate/${this.model}`,
					body,
					(data: unknown): data is string => commonsTypeIsString(data),
					undefined,
					undefined,
					{
							timeout: 30000,
							maxReattempts: 0
					}
			);
			const attempt: TPropertyObject<unknown>|undefined = commonsTypeAttemptObject(response);
			if (!attempt) return undefined;

			if (!isTSubjectivityResult(attempt)) return undefined;
			
			return attempt;
		} catch (e) {
			return undefined;
		}
	}

	public async generateOffenceScore(
			text: string,
			keepAlive?: CommonsFixedDuration
	): Promise<TSubjectivityResult|undefined> {
		return await this.generateSubjectivityScore(
				text,
				OFFENCE_PROMPT,
				keepAlive
		);
	}

	public async generateInterestScore(
			text: string,
			keepAlive?: CommonsFixedDuration
	): Promise<TSubjectivityResult|undefined> {
		return await this.generateSubjectivityScore(
				text,
				INTEREST_PROMPT,
				keepAlive
		);
	}
}
