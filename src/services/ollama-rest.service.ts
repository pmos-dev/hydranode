import { TEncodedObject, commonsTypeIsString, commonsTypeIsStringArray } from 'tscommons-es-core';
import { ECommonsHttpContentType } from 'tscommons-es-http';
import { CommonsKeyRestClientService } from 'tscommons-es-rest';

import { CommonsInternalHttpClientImplementation } from 'nodecommons-es-http';

export class OllamaRestService extends CommonsKeyRestClientService {
	constructor(
			implementation: CommonsInternalHttpClientImplementation,
			url: string,
			key: string
	) {
		super(
				implementation,
				url,
				key,
				ECommonsHttpContentType.JSON
		);
	}

	public async listModels(): Promise<string[]> {
		const result: unknown = await super.getRest<string[]>('/models');
		if (!commonsTypeIsStringArray(result)) throw new Error('Result was not a string array');

		return result;
	}

	public async generate(
			model: string,
			prompt: string
	): Promise<string> {
		const body: TEncodedObject = {
				prompt: prompt
		};

		const result: unknown = await super.postRest(
				`/generate/${model}`,
				body,
				undefined,
				undefined,
				{
						maxReattempts: 0,
						timeout: 30000
				}
		);
		if (!commonsTypeIsString(result)) throw new Error('Result was not a string');

		return result;
	}
}
