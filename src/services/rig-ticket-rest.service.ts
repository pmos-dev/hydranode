import { commonsTypeIsNumberKeyObject } from 'tscommons-es-core';
import { ECommonsHttpContentType } from 'tscommons-es-http';
import { CommonsKeyRestClientService } from 'tscommons-es-rest';

import { CommonsInternalHttpClientImplementation } from 'nodecommons-es-http';

import { TQueueLength } from '../types/tqueue-length';

export class RigTicketRestService extends CommonsKeyRestClientService {
	constructor(
			implementation: CommonsInternalHttpClientImplementation,
			url: string,
			key: string
	) {
		super(
				implementation,
				url,
				key,
				ECommonsHttpContentType.JSON
		);
	}

	public async length(): Promise<TQueueLength> {
		const result: unknown = await super.getRest<TQueueLength>('/length');
		if (!commonsTypeIsNumberKeyObject(result)) throw new Error('Result was not a number key object');

		return result as TQueueLength;
	}
}
