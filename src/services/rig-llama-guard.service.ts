import { commonsArrayRemoveUndefineds, commonsArrayUnique, CommonsFixedDuration, commonsStringSplitString, commonsTypeHasPropertyEnum, commonsTypeHasPropertyEnumArray, commonsTypeIsString } from 'tscommons-es-core';

import { CommonsInternalHttpClientImplementation } from 'nodecommons-es-http';

import { OllamaRestService } from './ollama-rest.service';
import { RigTicketedPromiseService, TPromptAndKeepLoaded } from './rig-ticketed-promise.service';

export enum ELlamaGuardClassification {
		SAFE = 'safe',
		UNSAFE = 'unsafe'
}

export function toELlamaGuardClassification(type: string): ELlamaGuardClassification|undefined {
	switch (type) {
		case ELlamaGuardClassification.SAFE.toString():
			return ELlamaGuardClassification.SAFE;
		case ELlamaGuardClassification.UNSAFE.toString():
			return ELlamaGuardClassification.UNSAFE;
	}
	return undefined;
}

export function fromELlamaGuardClassification(type: ELlamaGuardClassification): string {
	switch (type) {
		case ELlamaGuardClassification.SAFE:
			return ELlamaGuardClassification.SAFE.toString();
		case ELlamaGuardClassification.UNSAFE:
			return ELlamaGuardClassification.UNSAFE.toString();
	}
	
	throw new Error('Unknown ELlamaGuardClassification');
}

export function isELlamaGuardClassification(test: unknown): test is ELlamaGuardClassification {
	if (!commonsTypeIsString(test)) return false;
	
	return toELlamaGuardClassification(test) !== undefined;
}

export function keyToELlamaGuardClassification(key: string): ELlamaGuardClassification {
	switch (key) {
		case 'SAFE':
			return ELlamaGuardClassification.SAFE;
		case 'UNSAFE':
			return ELlamaGuardClassification.UNSAFE;
	}
	
	throw new Error(`Unable to obtain ELlamaGuardClassification for key: ${key}`);
}

export const ELLAMA_GUARD_CLASSIFICATIONS: ELlamaGuardClassification[] = Object.keys(ELlamaGuardClassification)
		.map((e: string): ELlamaGuardClassification => keyToELlamaGuardClassification(e));

export enum ELlamaGuardCategory {
		S1 = 'S1',
		S2 = 'S2',
		S3 = 'S3',
		S4 = 'S4',
		S5 = 'S5',
		S6 = 'S6',
		S7 = 'S7',
		S8 = 'S8',
		S9 = 'S9',
		S10 = 'S10',
		S11 = 'S11',
		S12 = 'S12',
		S13 = 'S13',
		S14 = 'S14'
}

export function toELlamaGuardCategory(type: string): ELlamaGuardCategory|undefined {
	switch (type) {
		case ELlamaGuardCategory.S1.toString():
			return ELlamaGuardCategory.S1;
		case ELlamaGuardCategory.S2.toString():
			return ELlamaGuardCategory.S2;
		case ELlamaGuardCategory.S3.toString():
			return ELlamaGuardCategory.S3;
		case ELlamaGuardCategory.S4.toString():
			return ELlamaGuardCategory.S4;
		case ELlamaGuardCategory.S5.toString():
			return ELlamaGuardCategory.S5;
		case ELlamaGuardCategory.S6.toString():
			return ELlamaGuardCategory.S6;
		case ELlamaGuardCategory.S7.toString():
			return ELlamaGuardCategory.S7;
		case ELlamaGuardCategory.S8.toString():
			return ELlamaGuardCategory.S8;
		case ELlamaGuardCategory.S9.toString():
			return ELlamaGuardCategory.S9;
		case ELlamaGuardCategory.S10.toString():
			return ELlamaGuardCategory.S10;
		case ELlamaGuardCategory.S11.toString():
			return ELlamaGuardCategory.S11;
		case ELlamaGuardCategory.S12.toString():
			return ELlamaGuardCategory.S12;
		case ELlamaGuardCategory.S13.toString():
			return ELlamaGuardCategory.S13;
		case ELlamaGuardCategory.S14.toString():
			return ELlamaGuardCategory.S14;
	}
	return undefined;
}

export function fromELlamaGuardCategory(type: ELlamaGuardCategory): string {
	switch (type) {
		case ELlamaGuardCategory.S1:
			return ELlamaGuardCategory.S1.toString();
		case ELlamaGuardCategory.S2:
			return ELlamaGuardCategory.S2.toString();
		case ELlamaGuardCategory.S3:
			return ELlamaGuardCategory.S3.toString();
		case ELlamaGuardCategory.S4:
			return ELlamaGuardCategory.S4.toString();
		case ELlamaGuardCategory.S5:
			return ELlamaGuardCategory.S5.toString();
		case ELlamaGuardCategory.S6:
			return ELlamaGuardCategory.S6.toString();
		case ELlamaGuardCategory.S7:
			return ELlamaGuardCategory.S7.toString();
		case ELlamaGuardCategory.S8:
			return ELlamaGuardCategory.S8.toString();
		case ELlamaGuardCategory.S9:
			return ELlamaGuardCategory.S9.toString();
		case ELlamaGuardCategory.S10:
			return ELlamaGuardCategory.S10.toString();
		case ELlamaGuardCategory.S11:
			return ELlamaGuardCategory.S11.toString();
		case ELlamaGuardCategory.S12:
			return ELlamaGuardCategory.S12.toString();
		case ELlamaGuardCategory.S13:
			return ELlamaGuardCategory.S13.toString();
		case ELlamaGuardCategory.S14:
			return ELlamaGuardCategory.S14.toString();
	}
	
	throw new Error('Unknown ELlamaGuardCategory');
}

export function isELlamaGuardCategory(test: unknown): test is ELlamaGuardCategory {
	if (!commonsTypeIsString(test)) return false;
	
	return toELlamaGuardCategory(test) !== undefined;
}

export function keyToELlamaGuardCategory(key: string): ELlamaGuardCategory {
	switch (key) {
		case 'S1':
			return ELlamaGuardCategory.S1;
		case 'S2':
			return ELlamaGuardCategory.S2;
		case 'S3':
			return ELlamaGuardCategory.S3;
		case 'S4':
			return ELlamaGuardCategory.S4;
		case 'S5':
			return ELlamaGuardCategory.S5;
		case 'S6':
			return ELlamaGuardCategory.S6;
		case 'S7':
			return ELlamaGuardCategory.S7;
		case 'S8':
			return ELlamaGuardCategory.S8;
		case 'S9':
			return ELlamaGuardCategory.S9;
		case 'S10':
			return ELlamaGuardCategory.S10;
		case 'S11':
			return ELlamaGuardCategory.S11;
		case 'S12':
			return ELlamaGuardCategory.S12;
		case 'S13':
			return ELlamaGuardCategory.S13;
		case 'S14':
			return ELlamaGuardCategory.S14;
	}
	
	throw new Error(`Unable to obtain ELlamaGuardCategory for key: ${key}`);
}

export function prettyLlamaGuardCategory(category: ELlamaGuardCategory): string {
	switch (category) {
		case ELlamaGuardCategory.S1: return 'Violent Crimes';
		case ELlamaGuardCategory.S2: return 'Non-Violent Crimes';
		case ELlamaGuardCategory.S3: return 'Sex Crimes';
		case ELlamaGuardCategory.S4: return 'Child Exploitation';
		case ELlamaGuardCategory.S5: return 'Defamation';
		case ELlamaGuardCategory.S6: return 'Specialized Advice';
		case ELlamaGuardCategory.S7: return 'Privacy';
		case ELlamaGuardCategory.S8: return 'Intellectual Property';
		case ELlamaGuardCategory.S9: return 'Indiscriminate Weapons';
		case ELlamaGuardCategory.S10: return 'Hate';
		case ELlamaGuardCategory.S11: return 'Self-Harm';
		case ELlamaGuardCategory.S12: return 'Sexual Content';
		case ELlamaGuardCategory.S13: return 'Elections';
		case ELlamaGuardCategory.S14: return 'Code Interpreter Abuse';
	}
}

export const ELLAMA_GUARD_CATEGORYS: ELlamaGuardCategory[] = Object.keys(ELlamaGuardCategory)
		.map((e: string): ELlamaGuardCategory => keyToELlamaGuardCategory(e));

export type TLlamaGuardResult = {
		classification: ELlamaGuardClassification;
} & ({
		classification: ELlamaGuardClassification.SAFE;
}
	| {
		classification: ELlamaGuardClassification.UNSAFE;
		categories: ELlamaGuardCategory[];
	}
);

function isTLlamaGuardResult(test: unknown): test is TLlamaGuardResult {
	if (!commonsTypeHasPropertyEnum<ELlamaGuardClassification>(test, 'classification', isELlamaGuardClassification)) return false;

	if (test.classification === ELlamaGuardClassification.UNSAFE) {
		if (!commonsTypeHasPropertyEnumArray<ELlamaGuardCategory>(test, 'categories', isELlamaGuardCategory)) return false;
	}

	return true;
}

function parseToTLlamaGuardResult(s: string): TLlamaGuardResult|false {
	s = s.trim();
	if (s === 'safe') {
		return {
				classification: ELlamaGuardClassification.SAFE
		};
	}

	const regex: RegExpExecArray|null = /^unsafe(?:\n|\\n|\\\\n)\s*((?:S[0-9]{1,2})(\s*,\s*(?:S[0-9]{1,2}))*)\s*$/.exec(s);
	if (!regex) return false;

	const categories: ELlamaGuardCategory[] = commonsArrayUnique(
			commonsArrayRemoveUndefineds<ELlamaGuardCategory>(
					commonsStringSplitString(regex[1], ',', true, true)
							.map((s2: string): ELlamaGuardCategory|undefined => toELlamaGuardCategory(s2))
			),
			undefined,
			(a: ELlamaGuardCategory, b: ELlamaGuardCategory): boolean => a === b
	);
	
	if (categories.length === 0) return false;

	return {
			classification: ELlamaGuardClassification.UNSAFE,
			categories: categories
	};
}

export class RigLlamaGuardService extends RigTicketedPromiseService {
	constructor(
			url: string,
			key: string,
			private model: string
	) {
		super(
				new OllamaRestService(
						new CommonsInternalHttpClientImplementation(),
						`${url}/ollama`,
						key
				),
				url,
				100
		);
	}

	public async generate(
			text: string,
			keepAlive?: CommonsFixedDuration
	): Promise<TLlamaGuardResult|undefined> {
		const prompt: string = `
<|begin_of_text|><|start_header_id|>user<|end_header_id|>

Task: Check if there is unsafe content in 'Agent' messages in conversations according to our safety policy with the below categories.

<BEGIN UNSAFE CONTENT CATEGORIES>
S1: Violent Crimes. 
S2: Non-Violent Crimes. 
S3: Sex Crimes. 
S4: Child Exploitation. 
S5: Defamation. 
S6: Specialized Advice. 
S7: Privacy. 
S8: Intellectual Property. 
S9: Indiscriminate Weapons. 
S10: Hate. 
S11: Self-Harm. 
S12: Sexual Content. 
S13: Elections. 
<END UNSAFE CONTENT CATEGORIES>

<BEGIN CONVERSATION>

Agent:
<<<TEXT>>>

<END CONVERSATION>

Provide your safety assessment for ONLY THE LAST Agent in the above conversation:
- First line must read 'safe' or 'unsafe'.
- If unsafe, a second line must include a comma-separated list of violated categories.<|eot_id|><|start_header_id|>assistant<|end_header_id|>
		`
				.trim()
				.replace('<<<TEXT>>>', text);

		const body: TPromptAndKeepLoaded = { prompt: prompt };
		if (keepAlive) {
			const mins: number = keepAlive.minutes;
			if (mins < 1) throw new Error('keepAlive cannot be less than 1 minute');

			body.keepLoaded = `${Math.round(mins)}m`;
		}

		try {
			const response: string = await this.submitAndWaitAsPromise<
					string,
					TPromptAndKeepLoaded
			>(
					`/generate/${this.model}`,
					body,
					(data: unknown): data is string => commonsTypeIsString(data),
					undefined,
					undefined,
					{
							timeout: 30000,
							maxReattempts: 0
					}
			);
			if (!commonsTypeIsString(response)) return undefined;

			const parsed: TLlamaGuardResult|false = parseToTLlamaGuardResult(response);
			if (!parsed) return undefined;
			if (!isTLlamaGuardResult(parsed)) return undefined;
			
			return parsed;
		} catch (e) {
			return undefined;
		}
	}
}
