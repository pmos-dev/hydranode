import { URL } from 'url';

import mongodb from 'mongodb';

import { commonsObjectStripNulls, commonsStringRegexLike, commonsTypeAttemptNumber, commonsTypeHasPropertyNumber, commonsTypeHasPropertyString, commonsTypeHasPropertyTArray } from 'tscommons-es-core';

import { IUrlErrors } from 'hydra-crawler-ts-assets';
import { IImage } from 'hydra-crawler-ts-assets';
import { IUrl, isIUrl } from 'hydra-crawler-ts-assets';
import { TPhpError, isTPhpError } from 'hydra-crawler-ts-assets';
import { TAspError, isTAspError } from 'hydra-crawler-ts-assets';
import { TDomain, isTDomain } from 'hydra-crawler-ts-assets';
import { TLink, isTLink } from 'hydra-crawler-ts-assets';
import { EStatus, toEStatus } from 'hydra-crawler-ts-assets';
import { EComparator } from 'hydra-crawler-ts-assets';

import { commonsOutputDebug, commonsOutputDoing, commonsOutputError, commonsOutputProgress, commonsOutputResult, commonsOutputSuccess } from 'nodecommons-es-cli';
import { CommonsMongodbService } from 'nodecommons-es-database-mongodb';

import { IParser } from '../interfaces/iparser';

import { TParserCtor } from '../types/tparser-ctor';

import { EAvailableStrategy } from '../enums/eavailable-strategy';

export type TMongoIdRow = {
		_id: string;
};
export function isTMongoIdRow(test: unknown): test is TMongoIdRow {
	if (!commonsTypeHasPropertyString(test, '_id')) return false;
	
	return true;
}

export type TMongoIdTallyRow = {
		_id: string;
		tally: number;
};
export function isTMongoIdTallyRow(test: unknown): test is TMongoIdTallyRow {
	if (!isTMongoIdRow(test)) return false;
	if (!commonsTypeHasPropertyNumber(test, 'tally')) return false;
	
	return true;
}

export class DatabaseService extends CommonsMongodbService {
	private domains: mongodb.Collection<TDomain>|undefined;
	private urls: mongodb.Collection<IUrl>|undefined;
	private links: mongodb.Collection<TLink>|undefined;

	// not automatically used, only by manual cli running
	private archiveds: mongodb.Collection<IUrl>|undefined;

	public getDomains(): mongodb.Collection<TDomain> {
		if (!this.domains) throw new Error('Domains collected has not been instantiated yet');
		
		return this.domains;
	}
	
	public getUrls(): mongodb.Collection<IUrl> {
		if (!this.urls) throw new Error('Urls collected has not been instantiated yet');
		
		return this.urls;
	}
	
	public getLinks(): mongodb.Collection<TLink> {
		if (!this.links) throw new Error('Links collected has not been instantiated yet');
		
		return this.links;
	}
	
	public getArchiveds(): mongodb.Collection<IUrl> {
		if (!this.archiveds) throw new Error('Archiveds collected has not been instantiated yet');
		
		return this.archiveds;
	}
	
	public async init(): Promise<void> {
		await super.init();
		
		if (!this.database) throw new Error('Database has not been instantiated yet');
		
		this.domains = await this.ensureCollection<TDomain>('domains');
		await this.domains.createIndex({ domain: 1 }, { unique: true });
		await this.domains.createIndex({ ip: 1 }, { unique: false });

		this.urls = await this.ensureCollection<IUrl>('urls');
		await this.urls.createIndex({ url: 1 }, { unique: true });
		await this.urls.createIndex({ domain: 1 }, { unique: false });
		await this.urls.createIndex({ status: 1 }, { unique: false });
		await this.urls.createIndex({ attempted: 1 }, { unique: false });
		await this.urls.createIndex({ orphan: 1 }, { unique: false });
		await this.urls.createIndex({ statusCode: 1 }, { unique: false });
		await this.urls.createIndex({ 'headers.content-type': 1 }, { unique: false });
		await this.urls.createIndex({ 'headers.content-length': 1 }, { unique: false });
		
		this.links = await this.ensureCollection<TLink>('links');
		await this.links.createIndex({ url: 1 }, { unique: false });
		await this.links.createIndex({ outgoing: 1 }, { unique: false });
		await this.links.createIndex({ url: 1, outgoing: 1 }, { unique: true });
		
		this.archiveds = await this.ensureCollection<IUrl>('archiveds');
		// no indices for the archiveds
	}
	
	public async initParser(ctor: TParserCtor): Promise<void> {
		const parser: IParser = new ctor();
		await parser.init(this);
	}
	
	public getRawDatabase(): mongodb.Db {	// changes protected to public
		return super.getRawDatabase();
	}
	
	public async wipe(): Promise<void> {
		await this.getLinks().deleteMany({});
		await this.getUrls().deleteMany({});
		await this.getDomains().deleteMany({});
	}

	public async resetActive(): Promise<void> {
		await this.getUrls().updateMany(
				{ status: EStatus.ACTIVE },
				{ $set: { status: EStatus.QUEUED } }
		);
	}

	public async domain(domain: string, ip: string): Promise<boolean> {
		try {
			await this.getDomains().updateOne(
					{ domain: domain },
					{ $set: { ip: ip } },
					{ upsert: true }
			);
		
			return true;
		} catch (ex) {
			commonsOutputDebug('debug position 7');
			console.log(ex);
			return false;
		}
	}

	public async queue(url: string, isDeny?: boolean): Promise<boolean> {
		const whatwg: URL = new URL(url);
		if (!whatwg.protocol.match(/^http(s?):$/)) return false;
		
		try {
			const status: EStatus = isDeny ? EStatus.DENY : EStatus.QUEUED;
			
			// un-archive if currently archived
			await this.getUrls().deleteOne({ url: url, status: EStatus.ARCHIVED });
			
			await this.getUrls().insertOne({
					url: url,
					domain: whatwg.hostname,
					status: status
			});

			return true;
		} catch (ex) {
			return false;
		}
	}

	public async available(
			strategy: EAvailableStrategy,
			threshold: number,
			limit: number,
			existing: string[],
			restrictTo: string[]
	): Promise<string[]> {
		if (limit === 0) return [];
		
		const comparator: EComparator = strategy === EAvailableStrategy.LARGEST ? EComparator.GTE : EComparator.LT;

		const thresholdMatch: { tally: { [ key: string ]: number } } = { tally: {} };
		switch (comparator) {
			case EComparator.GTE:
				thresholdMatch.tally = { $gte: threshold };
				break;
			case EComparator.LT:
				thresholdMatch.tally = { $lt: threshold };
				break;
		}
		
		const sortOrder: number = strategy === EAvailableStrategy.SMALLEST ? 1 : -1;

		const domainMatch = { $nin: existing };
		if (restrictTo.length > 0) domainMatch['$in'] = restrictTo;
		
		const results: mongodb.AggregationCursor<TMongoIdTallyRow> = this.getUrls().aggregate<TMongoIdTallyRow>([
				{ $match: { status: EStatus.QUEUED, domain: domainMatch } },
				{ $group: { _id: '$domain', tally: { $sum: 1 } } },
				{ $match: thresholdMatch },
				{ $sort: { tally: sortOrder  } },
				{ $limit: limit }
		], { allowDiskUse: true });
		
		const entries: TMongoIdTallyRow[] = await this.listQueryResults(results, isTMongoIdTallyRow);
		
		return entries
				// eslint-disable-next-line no-underscore-dangle
				.map((entry: TMongoIdTallyRow): string => entry._id);
	}
	
	public async next(domain: string): Promise<string|undefined> {
		const next: IUrl|null = await this.getUrls().findOne<IUrl>({
				status: EStatus.QUEUED,
				domain: domain
		});
		if (next === null) return undefined;
		
		return next.url;
	}

	public async setStatus(url: string, status: EStatus): Promise<boolean> {
		const updates: { status: EStatus } = { status: status };
		
		if (![ EStatus.QUEUED, EStatus.ACTIVE ].includes(status)) updates['attempted'] = new Date();
		
		if (status === EStatus.DONE) updates['done'] = new Date();
		
		try {
			await this.getUrls().updateOne(
					{ url: url },
					{ $set: updates }
			);
			return true;
		} catch (ex) {
			console.error(ex);
			return false;
		}
	}

	public async setStatusCode(url: string, code: number): Promise<boolean> {
		try {
			await this.getUrls().updateOne(
					{ url: url },
					{ $set: { statusCode: code } }
			);
			return true;
		} catch (ex) {
			console.error(ex);
			return false;
		}
	}

	public async setHeaders(url: string, headers: { [ key: string ]: string|number|Date }): Promise<boolean> {
		try {
			await this.getUrls().updateOne(
					{ url: url },
					{ $set: { headers: headers } }
			);
			return true;
		} catch (ex) {
			console.error(ex);
			return false;
		}
	}

	public async setData(url: string, context: string, data: unknown): Promise<boolean> {
		try {
			const update: { [ key: string ]: unknown } = {};
			update[context] = data;
			
			await this.getUrls().updateOne(
					{ url: url },
					{ $set: update }
			);

			return true;
		} catch (ex) {
			console.error(ex);
			return false;
		}
	}

	public async unsetData(url: string, context: string): Promise<boolean> {
		try {
			const update: { [ key: string ]: true } = {};
			update[context] = true;
			
			await this.getUrls().updateOne(
					{ url: url },
					{ $unset: update }
			);

			return true;
		} catch (ex) {
			console.error(ex);
			return false;
		}
	}

	public async getTtl(url: string): Promise<number|undefined> {
		const row: IUrl|null = await this.getUrls().findOne<IUrl>({ url: url });
		if (row === null) return undefined;
		
		if (!commonsTypeHasPropertyNumber(row, 'ttl')) return undefined;
		return (row as unknown as { ttl: number })['ttl'];
	}

	public async setTtl(url: string, ttl: number): Promise<boolean> {
		try {
			await this.getUrls().updateOne(
					{ url: url },
					{ $set: { ttl: ttl } }
			);
			return true;
		} catch (ex) {
			console.error(ex);
			return false;
		}
	}

	public async unsetTtl(url: string): Promise<boolean> {
		try {
			await this.getUrls().updateOne(
					{ url: url },
					{ $unset: { ttl: true } }
			);
			return true;
		} catch (ex) {
			console.error(ex);
			return false;
		}
	}

	public async getHash(url: string): Promise<string|undefined> {
		const row: IUrl|null = await this.getUrls().findOne<IUrl>({ url: url });
		if (row === null) return undefined;
		
		if (!commonsTypeHasPropertyString(row, 'hash')) return undefined;
		return (row as unknown as { hash: string })['hash'];
	}

	public async setHash(url: string, hash: string): Promise<boolean> {
		try {
			await this.getUrls().updateOne(
					{ url: url },
					{ $set: {
							hash: hash,
							hashSet: new Date()
					} }
			);
			return true;
		} catch (ex) {
			console.error(ex);
			return false;
		}
	}

	public async unsetHash(url: string): Promise<boolean> {
		try {
			await this.getUrls().updateOne(
					{ url: url },
					{ $unset: {
							hash: true,
							hashSet: true
					} }
			);
			return true;
		} catch (ex) {
			console.error(ex);
			return false;
		}
	}

	public async setFailReason(url: string, reason: string): Promise<boolean> {
		try {
			await this.getUrls().updateOne(
					{ url: url },
					{ $set: {
							reason: reason
					} }
			);
			return true;
		} catch (ex) {
			console.error(ex);
			return false;
		}
	}

	public async unsetFailReason(url: string): Promise<boolean> {
		try {
			await this.getUrls().updateOne(
					{ url: url },
					{ $unset: {
							reason: true
					} }
			);
			return true;
		} catch (ex) {
			console.error(ex);
			return false;
		}
	}

	public async link(url: string, links: string[]): Promise<boolean> {
		// more efficient to only remove removed and only add new
		// rather than just wiping all existing and re-adding
		
		const find: mongodb.FindCursor<TLink> = this.getLinks().find<TLink>({
				url: url
		}, {});

		const existing: string[] = (await this.listQueryResults(find, isTLink))
				.map((link: TLink): string => link.outgoing);
		
		const removed: string[] = [];
		const added: string[] = [];
		
		for (const link of links) {
			if (!existing.includes(link) && !added.includes(link)) added.push(link);
		}
		for (const link of existing) {
			if (!links.includes(link)) removed.push(link);
		}

		for (const outgoing of removed) {
			try {
				await this.getLinks().deleteMany({	// should only be 1, but be safe and delete any
						url: url,
						outgoing: outgoing
				});
			} catch (ex) {
				/* do nothing */
			}
		}
		
		for (const outgoing of added) {
			try {
				await this.getLinks().insertOne({
						url: url,
						outgoing: outgoing
				});
			} catch (ex) {
				switch ((ex as { code: number }).code || -1) {
					case 11000:
						// ignore duplicates
						commonsOutputError(`DUPLICATE: ${url}, ${outgoing}`);
						break;
					case 17280:
					case 17282:
						commonsOutputError(`INDEX TOO LARGE: ${url}, ${outgoing}`);
						// ignore index too large
						break;
					default:
						commonsOutputDebug('debug position 8');
						console.log(ex);
						throw ex;
				}
			}
		}
		
		return true;
	}

	public async markDead(domain: string): Promise<boolean> {
		try {
			await this.getUrls().updateMany(
					{ domain: domain, status: { $in: [ EStatus.QUEUED, EStatus.ACTIVE ] } },
					{ $set: { status: EStatus.DEAD, attempted: new Date() } }
			);

			return true;
		} catch (ex) {
			console.error(ex);
			return false;
		}
	}

	public async listStatusTallies(): Promise<Map<EStatus, number>> {
		const results: mongodb.AggregationCursor<TMongoIdTallyRow> = this.getUrls().aggregate<TMongoIdTallyRow>([
				{ $match: { status: { $ne: EStatus.ARCHIVED } } },
				{ $group: {
						_id: '$status',
						tally: { $sum: 1 }
				} }
		]);
		
		const rows: TMongoIdTallyRow[] = await this.listQueryResults(results, isTMongoIdTallyRow);
		
		const map: Map<EStatus, number> = new Map<EStatus, number>();
		for (const row of rows) {
			// eslint-disable-next-line no-underscore-dangle
			const status: EStatus|undefined = toEStatus(row._id);
			if (status) map.set(status, row.tally);
		}
		
		return map;
	}

	public async getLinkTalliesCount(): Promise<number> {
		return await this.getLinks().estimatedDocumentCount();
	}

	public async getDomainTalliesCount(): Promise<number> {
		return await this.getDomains().estimatedDocumentCount();
	}

	public async listDomainQueuedTallies(): Promise<Map<string, number>> {
		const results: mongodb.AggregationCursor<TMongoIdTallyRow> = this.getUrls().aggregate<TMongoIdTallyRow>([
				{ $match: {
						status: EStatus.QUEUED
				} },
				{ $group: {
						_id: '$domain',
						tally: { $sum: 1 }
				} }
		]);
		
		const rows: TMongoIdTallyRow[] = await this.listQueryResults(results, isTMongoIdTallyRow);
		
		const map: Map<string, number> = new Map<string, number>();
		for (const row of rows) {
			// eslint-disable-next-line no-underscore-dangle
			map.set(row._id, row.tally);
		}
		
		return map;
	}

	public async listPhpErrors(): Promise<IUrlErrors<TPhpError>[]> {
		const results: mongodb.FindCursor<IUrl> = this.getUrls().find<IUrl>(
				{
						status: { $ne: EStatus.ARCHIVED },
						phpErrors: { $exists: true }
				},
				{}
		);

		return (await this.listQueryResults(results, isIUrl))
				.map((url: IUrl): IUrlErrors<TPhpError> => {
					if (!commonsTypeHasPropertyTArray<TPhpError>(url, 'phpErrors', isTPhpError)) throw new Error('Invalid PHP error object');
					
					return {
							url: url.url,
							errors: (url as unknown as { phpErrors: TPhpError[] }).phpErrors
					};
				});
	}

	public async listAspErrors(): Promise<IUrlErrors<TAspError>[]> {
		const results: mongodb.FindCursor<IUrl> = this.getUrls().find<IUrl>(
				{
						status: { $ne: EStatus.ARCHIVED },
						aspErrors: { $exists: true }
				},
				{}
		);

		return (await this.listQueryResults(results, isIUrl))
				.map((url: IUrl): IUrlErrors<TAspError> => {
					if (!commonsTypeHasPropertyTArray<TAspError>(url, 'aspErrors', isTAspError)) throw new Error('Invalid PHP error object');
					
					return {
							url: url.url,
							errors: (url as unknown as { aspErrors: TAspError[] }).aspErrors
					};
				});
	}

	public async listDone200DomainUrls(domain: string): Promise<string[]> {
		const results: mongodb.FindCursor<IUrl> = this.getUrls().find<IUrl>(
				{ $and: [
						{ domain: domain },
						{ status: EStatus.DONE },
						{ statusCode: { $gte: 200 } },
						{ statusCode: { $lt: 300 } }
				]},
				{}
		);

		return (await this.listQueryResults(results, isIUrl))
				.map((url: IUrl): string => url.url);
	}

	public async listDomains(): Promise<TDomain[]> {
		const results: mongodb.FindCursor<TDomain> = this.getDomains().find<TDomain>(
				// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
				{ ip: { $exists: true, $ne: null as any } },	// this is ok, despite the type objection to null
				{}
		);
	
		// since we're doing $ne: null above, we don't need to strip nulls, as there won't be any
		return await this.listQueryResults(results, isTDomain);
	}

	public async listDomainsByLike(term: string): Promise<TDomain[]> {
		const results: mongodb.FindCursor<TDomain> = this.getDomains().find<TDomain>({
				domain: new RegExp(commonsStringRegexLike(`%${term}%`), 'i')
		}, {});
	
		return (await this.listQueryResults(results, isTDomain))
				.map((encoded: TDomain): TDomain => commonsObjectStripNulls(encoded) as TDomain);
	}

	public async listInboundLinks(url: string): Promise<string[]> {
		const results: mongodb.FindCursor<TLink> = this.getLinks().find<TLink>(
				{ outgoing: url },
				{}
		);
	
		return (await this.listQueryResults(results, isTLink))
				.map((link: TLink): string => link.url);
	}

	public async listOutboundLinks(url: string): Promise<string[]> {
		const results: mongodb.FindCursor<TLink> = this.getLinks().find<TLink>(
				{ url: url },
				{}
		);

		return (await this.listQueryResults(results, isTLink))
				.map((link: TLink): string => link.outgoing);
	}

	public async listImagesBySizeThreshold(size: number, comparator: EComparator): Promise<IImage[]> {
		const queries: { [ key: string ]: EStatus|RegExp|{ [ key: string ]: unknown } }[] = [
				{ status: EStatus.DONE },
				{ 'headers.content-type': /^image\/(jpeg)/ },
				{ 'headers.content-length': { $exists: true } }
		];

		switch (comparator) {
			case EComparator.GT:
				queries.push({ 'headers.content-length': { $gt: size } });
				break;
			case EComparator.LT:
				queries.push({ 'headers.content-length': { $lt: size } });
				break;
			case EComparator.GTE:
				queries.push({ 'headers.content-length': { $gte: size } });
				break;
			case EComparator.LTE:
				queries.push({ 'headers.content-length': { $lte: size } });
				break;
		}
		
		const results: mongodb.FindCursor<IUrl> = this.getUrls().find<IUrl>(
				{ $and: queries },
				{}
		);
	
		return (await this.listQueryResults(results, isIUrl))
				.map((row: IUrl): IImage => ({
						url: row.url,
						size: commonsTypeAttemptNumber((row['headers'] as { [ key: string ]: unknown })['content-length']) || -1
				}));
	}

	private async insertArchiveBatch(documents: mongodb.FindCursor<IUrl>): Promise<string[]> {
		const bulkInsert: mongodb.UnorderedBulkOperation = this.getArchiveds().initializeUnorderedBulkOp();

		const insertIds: string[] = [];
		for await (const document of documents) {
			const typecast: IUrl & { _id: string } = document as IUrl & { _id: string };
			
			// eslint-disable-next-line no-underscore-dangle
			const id: string = typecast._id;

			// Insert without raising an error for duplicates
			bulkInsert
					.find({ _id: id })
					.upsert()
					.replaceOne(document);

			insertIds.push(id);
		}
		if (insertIds.length > 0) await bulkInsert.execute();

		return insertIds;
	}

	private async deleteUrlsBatch(documents: mongodb.FindCursor<IUrl>): Promise<void> {
		// NB, this presumes that the links to and from this have already been removed by the cleaner

		const bulkDelete: mongodb.UnorderedBulkOperation = this.getUrls().initializeUnorderedBulkOp();

		let tally: number = 0;
		for await (const document of documents) {
			const typecast: IUrl & { _id: string } = document as IUrl & { _id: string };
				
			// eslint-disable-next-line no-underscore-dangle
			const id: unknown = typecast._id;

			bulkDelete
					.find({ _id: id })
					.deleteOne();
			
			tally++;
		}
		if (tally > 0) await bulkDelete.execute();
	}

	public async moveArchivedUrlsToArchive(
			batchSize: number,
			limit?: number
	): Promise<void> {
		let total: number = 0;
		while (true) {
			commonsOutputDoing(`Inserting up to ${batchSize} urls entries into archiveds`);
			
			const existings: mongodb.FindCursor<IUrl> = this.getUrls().find({ status: EStatus.ARCHIVED }).limit(batchSize);
			const done: mongodb.ObjectId[] = (await this.insertArchiveBatch(existings))
					.map((id: string): mongodb.ObjectId => new mongodb.ObjectId(id));

			await existings.close();

			total += done.length;

			commonsOutputResult(`${done.length}, ${total} total`);
			if (done.length === 0) break;

			commonsOutputDoing('Deleting previously moved urls');

			const toDelete: mongodb.FindCursor<IUrl> = this.getUrls().find({ _id: { $in: done } }).limit(batchSize);
			await this.deleteUrlsBatch(toDelete);

			await toDelete.close();

			if (limit !== undefined) limit -= done.length;

			commonsOutputSuccess();

			if (limit !== undefined && limit <= 0) break;
		}
	}

	public async listDuplicateArchivedUrls(): Promise<string[]> {
		commonsOutputDoing('Building cursor for all archiveds');

		// we have to use this strange sharded increasing array of sets, as the maximum values a Set (or Map) can hold is 16777216 (2^24)

		const urls: Set<string>[] = [];
		const duplicates: Set<string> = new Set<string>();
		let tally: number = 0;

		while (true) {
			let subTally: number = 0;

			const currentUrls: Set<string> = new Set<string>();

			try {
				const archiveds: mongodb.FindCursor<IUrl> = this.getArchiveds().find<IUrl>(
						{},
						{
								sort: { _id: 1 },
								skip: tally,
								limit: 1000000
						}
				);
				commonsOutputSuccess();

				commonsOutputDoing('Searching for duplicates');

				await archiveds.forEach(
						(archived: IUrl): void => {
							try {
								let match: boolean = false;
								for (const s of [ ...urls, currentUrls ]) {
									if (s.has(archived.url)) match = true;
								}

								if (match) {
									duplicates.add(archived.url);
								} else {
									currentUrls.add(archived.url);
								}

								tally++;
								subTally++;
								if ((tally % 10000) === 0) {
									let total: number = 0;
									for (const s of [ ...urls, currentUrls ]) total += s.size;
									commonsOutputProgress(`${tally}, ${total} unique urls, ${duplicates.size} duplicates`);
								}
							} catch (e) {
								console.log(e);
								process.exit(1);
							}
						}
				);

				let batchTotal: number = 0;
				for (const s of [ ...urls, currentUrls ]) batchTotal += s.size;
				commonsOutputResult(`${tally}, ${batchTotal} unique urls, ${duplicates.size} duplicates`);

				await archiveds.close();
			} catch (e2) {
				console.log(e2);
				process.exit(1);
			}

			urls.push(currentUrls);
			if (subTally === 0) break;
		}

		return Array.from(duplicates.values());
	}

	public async purgeArchiveDuplicates(url: string): Promise<void> {
		const archiveds: mongodb.FindCursor<IUrl> = this.getArchiveds().find<IUrl>(
				{
						url: url
				},
				{}
		);

		const bulkDelete: mongodb.UnorderedBulkOperation = this.getArchiveds().initializeUnorderedBulkOp();

		let first: boolean = true;
		let tally: number = 0;
		for await (const document of archiveds) {
			if (first) {
				first = false;
				continue;
			}

			const typecast: IUrl & { _id: string } = document as IUrl & { _id: string };
				
			// eslint-disable-next-line no-underscore-dangle
			const id: unknown = typecast._id;

			bulkDelete
					.find({ _id: id })
					.deleteOne();
			
			tally++;
		}
		if (tally > 0) await bulkDelete.execute();

		await archiveds.close();
	}
}
