import { IMatch } from '../interfaces/imatch';

export abstract class Matcher {
	public static matches(url: URL, test: IMatch): boolean {
		for (const key of Object.keys(test)) {
			const regex: RegExp = new RegExp(test[key] as string);
			if (!regex.test(url[key] as string)) return false;
		}
		return true;
	}

	public static isAnyMatch(url: string, list: IMatch[]): boolean {
		const whatwg: URL = new URL(url);

		for (const comparators of list) {
			if (Matcher.matches(whatwg, comparators)) return true;
		}
		
		return false;
	}
}
