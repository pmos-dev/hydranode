import { TCrawlConfig } from '../types/tcrawl-config';

export abstract class Random {
	public static randomise(value: number, crawlConfig: TCrawlConfig): number {
		const delta: number = (Math.random() - 0.5) * 2;
		const ratio: number = crawlConfig.randomisation * delta;
	
		return value * (1 + ratio);
	}
}
