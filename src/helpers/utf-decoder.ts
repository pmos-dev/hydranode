import * as chardet from 'chardet';

export abstract class UtfDecoder {
	public static fromBuffer(incoming: Buffer): string|undefined {
		if (incoming === undefined) return undefined;
		
		try {
			const length: number = Math.min(incoming.length, 32);
			if (length === 0) return '';
			
			const short: Buffer = Buffer.allocUnsafe(length);
			incoming.copy(short, 0, 0, length);
			switch (chardet.detect(short)) {
				case 'UTF-16LE':
					return incoming.toString('utf16le');
				case 'UTF-16BE':
					return Buffer.from(incoming).swap16().toString('utf16le');
				default:
					return incoming.toString();
			}
		} catch (ex) {
			// might be a chardet failure, so check that first
			try {
				return incoming.toString();
			} catch (ex2) {
				// ignore
			}
		}

		return undefined;
	}
}
