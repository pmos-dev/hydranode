import * as express from 'express';

import { TPropertyObject } from 'tscommons-es-core';
import { CommonsConfig } from 'tscommons-es-config';

import { CommonsStrictExpressServer, ICommonsRequestWithStrictParams } from 'nodecommons-es-express';
import { isICommonsExpressConfig } from 'nodecommons-es-express';
import { CommonsRestServer } from 'nodecommons-es-rest';
import { commonsOutputDoing, commonsOutputSuccess } from 'nodecommons-es-cli';
import { commonsNginxProxyPath } from 'nodecommons-es-http';

import { TestApi } from '../apis/test.api';
import { StatisticsApi } from '../apis/statistics.api';
import { CrawlApi } from '../apis/crawl.api';
import { BugsApi } from '../apis/bugs.api';
import { DomainsApi } from '../apis/domains.api';
import { UrlsApi } from '../apis/urls.api';
import { ImagesApi } from '../apis/images.api';
import { AutocompleteApi } from '../apis/autocomplete.api';

import { DatabaseService } from '../services/database.service';

export class RestServer extends CommonsRestServer<ICommonsRequestWithStrictParams, express.Response> {
	constructor(
			expressServer: CommonsStrictExpressServer,
			database: DatabaseService,
			config: CommonsConfig
	) {
		super(expressServer);

		const expressConfig: TPropertyObject = config.getObject('express');
		if (!isICommonsExpressConfig(expressConfig)) throw new Error('Express config is not valid');
		
		const path: string = commonsNginxProxyPath(expressConfig.path);
		
		commonsOutputDoing('Installing REST Test API');
		new TestApi(this, path);
		commonsOutputSuccess();
		
		commonsOutputDoing('Installing REST Statistics API');
		new StatisticsApi(this, database, path);
		commonsOutputSuccess();
		
		commonsOutputDoing('Installing REST Crawl API');
		new CrawlApi(this, database, path);
		commonsOutputSuccess();
		
		commonsOutputDoing('Installing REST Bugs API');
		new BugsApi(this, database, path);
		commonsOutputSuccess();
		
		commonsOutputDoing('Installing REST Domains API');
		new DomainsApi(this, database, path);
		commonsOutputSuccess();
		
		commonsOutputDoing('Installing REST URLs API');
		new UrlsApi(this, database, path);
		commonsOutputSuccess();
		
		commonsOutputDoing('Installing REST Images API');
		new ImagesApi(this, database, path);
		commonsOutputSuccess();
		
		commonsOutputDoing('Installing REST Autocomplete API');
		new AutocompleteApi(this, database, path);
		commonsOutputSuccess();
	}
	
}
