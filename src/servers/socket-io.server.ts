import { TStatusTallies } from 'hydra-crawler-ts-assets';
import { TFetch } from 'hydra-crawler-ts-assets';
import { TOutcome } from 'hydra-crawler-ts-assets';

import { CommonsAppSocketIoServer } from 'nodecommons-es-app-socket-io';

export class SocketIoServer extends CommonsAppSocketIoServer {
	public async fetching(fetch: TFetch): Promise<void> {
		await super.broadcast('fetching', fetch);
	}
	
	public async outcome(outcome: TOutcome): Promise<void> {
		await super.broadcast('outcome', outcome);
	}
	
	public async statusTallies(tallies: TStatusTallies): Promise<void> {
		await super.broadcast('statusTallies', tallies);
	}
	
	public async linkTallies(tally: number): Promise<void> {
		await super.broadcast('linkTallies', tally);
	}
	
	public async domainTallies(tally: number): Promise<void> {
		await super.broadcast('domainTallies', tally);
	}

	public async bandwidth(bps: number): Promise<void> {
		await super.broadcast('bandwidth', bps);
	}
}
