import * as http from 'http';

import * as express from 'express';

import { CommonsStrictExpressServer } from 'nodecommons-es-express';
import { ICommonsExpressConfig } from 'nodecommons-es-express';
import { commonsGracefulAbortAddCallback } from 'nodecommons-es-process';
import { commonsOutputAlert } from 'nodecommons-es-cli';

export class ExpressServer extends CommonsStrictExpressServer {
	constructor(
			ex: express.Express,
			server: http.Server,
			config: ICommonsExpressConfig
	) {
		super(ex, server, config.port);

		commonsGracefulAbortAddCallback((): void => {
			commonsOutputAlert('SIGINT abort flag is set. Aborting Express server.');
			super.close();
		});
	}
}
