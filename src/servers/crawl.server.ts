import { TKeyObject } from 'tscommons-es-core';
import { commonsAsyncAbortTimeout, commonsAsyncTimeout } from 'tscommons-es-async';

import { EStatus } from 'hydra-crawler-ts-assets';

import { commonsOutputAlert, commonsOutputCompleted, commonsOutputDebug, commonsOutputDoing, commonsOutputProgress, commonsOutputStarting, commonsOutputSuccess } from 'nodecommons-es-cli';
import { commonsGracefulAbortAddCallback } from 'nodecommons-es-process';

import { Crawler } from '../classes/crawler';
import { Lists } from '../classes/lists';
import { Tracker } from '../classes/tracker';

import { DatabaseService } from '../services/database.service';

import { IParserConfig } from '../interfaces/iparser-config';

import { THydraConfig } from '../types/thydra-config';
import { TCrawlConfig } from '../types/tcrawl-config';
import { TRobotsConfig } from '../types/trobots-config';
import { TParserCtor } from '../types/tparser-ctor';

import { EAvailableStrategy } from '../enums/eavailable-strategy';

export class CrawlServer {
	private parsers: TParserCtor[] = [];
	private crawlers: Map<string, Crawler>;
	private strategies: Map<string, EAvailableStrategy>;
	private isAborted: boolean = false;
	
	private preDelayIds: string[] = [];
	
	private isPaused: boolean = false;

	private domainRestrictTo: string[] = [];
	public set restrictTo(domains: string[]) {
		this.domainRestrictTo = domains;
	}
	
	constructor(
			private database: DatabaseService,
			private hydraConfig: THydraConfig,
			private crawlConfig: TCrawlConfig,
			private parsersConfig: TKeyObject<IParserConfig>,
			private robotsConfig: TRobotsConfig,
			private lists: Lists,
			private tracker: Tracker
	) {
		this.crawlers = new Map<string, Crawler>();
		this.strategies = new Map<string, EAvailableStrategy>();
		
		commonsGracefulAbortAddCallback((): void => {
			commonsOutputAlert('SIGINT abort flag is set. Aborting crawl server.');
			this.abort();
			
			for (const preDelayId of this.preDelayIds) commonsAsyncAbortTimeout(preDelayId);
		});
	}
	
	public addParser(parser: TParserCtor): void {
		this.parsers.push(parser);
	}
	
	public listParsers(): TParserCtor[] {
		return this.parsers.slice();
	}
	
	public pause(): void {
		commonsOutputAlert('Pausing crawl server');
		
		this.isPaused = true;
		
		for (const domain of this.crawlers.keys()) {
			this.pauseCrawl(domain);
		}
	}
	
	public resume(): void {
		commonsOutputCompleted('Resuming crawl server');

		this.isPaused = false;
		
		for (const domain of this.crawlers.keys()) {
			this.resumeCrawl(domain);
		}
	}
	
	private abort(): void {
		this.isAborted = true;
		commonsAsyncAbortTimeout('find-new-available');
	}
	
	public async terminate(): Promise<void> {
		this.abort();
		
		await this.database.close();
		process.exit(0);
	}
	
	private pauseCrawl(domain: string): void {
		if (!this.crawlers.has(domain)) return;
		
		commonsOutputAlert(`Pausing crawler for ${domain}`);
				
		this.crawlers.get(domain)!.pause();
	}
	
	private resumeCrawl(domain: string): void {
		if (!this.crawlers.has(domain)) return;
		
		commonsOutputCompleted(`Resuming crawler for ${domain}`);
				
		this.crawlers.get(domain)!.resume();
	}
	
	public async start(): Promise<void> {
		await this.database.resetActive();
		
		const added: boolean = await this.database.queue(this.hydraConfig.startUrl);
		if (added) this.tracker.delta(EStatus.QUEUED, 1);

		while (!this.isAborted) {
			if (!this.isPaused) {
				const existing: string[] = [ ...this.crawlers.keys() ];
				
				const space: number = this.crawlConfig.maxCrawlers - existing.length;
				
				if (space > 0) {
					commonsOutputDebug(`Space for ${space} new crawls available`);
					
					const largestStrategyDomains: string[] = existing
							.filter((domain: string): boolean => this.strategies.get(domain) === EAvailableStrategy.LARGEST);

					const smallestStrategyDomains: string[] = existing
							.filter((domain: string): boolean => this.strategies.get(domain) === EAvailableStrategy.SMALLEST);

					commonsOutputDebug(`Current crawl has LARGEST=${largestStrategyDomains.length}; SMALLEST=${smallestStrategyDomains.length}`);
					
					let spaceForLargestDomains: number = 0;
					let spaceForSmallestDomains: number = space;

					if (largestStrategyDomains.length >= Math.floor(this.crawlConfig.maxCrawlers / 2)) {
						// already 50% large crawling, so all remaining space goes to small crawls
					} else {
						// allow a new large crawl, if enough space
						if (space > 1) {
							spaceForLargestDomains = 1;
							spaceForSmallestDomains--;
						}
					}

					commonsOutputDebug(`Space for LARGEST=${spaceForLargestDomains}; SMALLEST=${spaceForSmallestDomains}`);
					
					const availablesLargest: string[] = await this.database.available(
							EAvailableStrategy.LARGEST,
							this.crawlConfig.availableStrategyThreshold,
							spaceForLargestDomains,
							existing,
							this.domainRestrictTo
					);
					
					existing.push(...availablesLargest);

					const availablesSmallest: string[] = await this.database.available(
							EAvailableStrategy.SMALLEST,
							this.crawlConfig.availableStrategyThreshold,
							spaceForSmallestDomains,
							existing,
							this.domainRestrictTo
					);
					
					const availables: string[] = [
							...availablesSmallest,
							...availablesLargest
					];
	
					if (!this.isAborted) {
						for (const domain of availables) {
							commonsOutputStarting(`Creating new crawl head for ${domain}`);
							
							const crawler: Crawler = new Crawler(
									domain,
									this.database,
									this.crawlConfig,
									this.parsersConfig,
									this.robotsConfig,
									this.parsers,
									this.lists,
									this.tracker
							);
							
							this.crawlers.set(domain, crawler);
							
							if (availablesLargest.includes(domain)) {
								this.strategies.set(domain, EAvailableStrategy.LARGEST);
							} else {
								this.strategies.set(domain, EAvailableStrategy.SMALLEST);
							}
		
							// called without await in order to do parallel crawls
							void (async (): Promise<void> => {
								try {
									// delay randomly to prevent network socket request spikes
									const preDelayId: string = `predelay_${domain}`;
									this.preDelayIds.push(preDelayId);
									try {
										await commonsAsyncTimeout(
												Math.random() * (this.crawlConfig.findNewAvailableDelay - 1000),
												preDelayId
										);
									} catch (e) {
										if ((e as Error).message === 'abortTimeout called') return;
										throw e;
									}

									this.preDelayIds = this.preDelayIds
											.filter((pid: string): boolean => pid !== preDelayId);
	
									if (this.isAborted) return;
									if (this.isPaused) this.pauseCrawl(domain);

									commonsOutputStarting(`Starting crawler for ${domain}`);
									await crawler.crawl();
								} catch (ex) {
									commonsOutputDebug('debug position 6');
									console.log(ex);
								} finally {
									this.crawlers.delete(domain);	// doesn't get called until after doCrawl() is called, so have to do it here if aborted
									this.strategies.delete(domain);
								}
							})();
						}
					}
				}
			}

			try {
				await commonsAsyncTimeout(this.crawlConfig.findNewAvailableDelay, 'find-new-available');
			} catch (ex) {
				// ignore
			}

			if (this.isAborted) {
				commonsOutputAlert('SIGINT abort is set. Aborting new head loop.');
				break;
			}
		}
	}
	
	public async shutdown(): Promise<void> {
		commonsOutputDoing('Waiting for all crawlers to abort');
		while (this.crawlers.size > 0) {
			commonsOutputProgress(this.crawlers.size);
			
			try {
				await commonsAsyncTimeout(1000);
			} catch (ex) {
				// ignore
			}
		}
		commonsOutputSuccess();
	}
}
