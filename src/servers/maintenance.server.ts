import { commonsAsyncTimeout, CommonsSchedule, TCommonsScheduleTime } from 'tscommons-es-async';

import { commonsOutputAlert, commonsOutputDebug, commonsOutputError } from 'nodecommons-es-cli';
import { commonsGracefulAbortAddCallback } from 'nodecommons-es-process';

import { Expiry } from '../classes/expiry';
import { Lists } from '../classes/lists';
import { Expirer } from '../classes/expirer';
import { Cleaner } from '../classes/cleaner';

import { CrawlServer } from '../servers/crawl.server';

import { DatabaseService } from '../services/database.service';

enum EAction {
	EXPIRE = 'expire',
	PURGE_EMPTY_DOMAINS = 'purge-empty-domains',
	PURGE_ORPHAN_URLS = 'purge-orphan-urls'
}

export function toEAction(value: string): EAction|undefined {
	switch (value) {
		case EAction.EXPIRE.toString():
			return EAction.EXPIRE;
		case EAction.PURGE_EMPTY_DOMAINS.toString():
			return EAction.PURGE_EMPTY_DOMAINS;
		case EAction.PURGE_ORPHAN_URLS.toString():
			return EAction.PURGE_ORPHAN_URLS;
	}

	return undefined;
}

export class MaintenanceServer {
	private schedule: CommonsSchedule;
	private expirer: Expirer;
	private cleaner: Cleaner;
	
	private isPaused: boolean = false;
	private isRunning: boolean = false;

	constructor(
			times: TCommonsScheduleTime[],
			expiry: Expiry,
			lists: Lists,
			database: DatabaseService,
			private crawl: CrawlServer,
			private hardLimit?: number
	) {
		this.schedule = new CommonsSchedule('hydra-maintenance');

		this.schedule.parse<EAction>(
				times,
				(action: string): EAction|undefined => toEAction(action),
				async (action: EAction): Promise<void> => {
					// prevent duplicate runs
					if (this.isRunning) {
						commonsOutputDebug('Maintenance is already running. Not re-running');
						return;
					}
					this.isRunning = true;

					try {
						let claimedPause: boolean = false;
						
						if (!this.isPaused) {
							this.isPaused = true;
							claimedPause = true;
							
							this.crawl.pause();
							
							for (let i: number = 30; i-- > 0;) {
								commonsOutputAlert(`Going down for maintenance ... ${i}`);
								
								try {
									await commonsAsyncTimeout(1000);
								} catch (ex) {
									/* do nothing */
								}
							}
						}

						await this.perform(action);
						
						if (claimedPause) {
							commonsOutputAlert('Resuming from maintenance');

							this.isPaused = false;
							
							this.crawl.resume();
						}
					} catch (e) {
						console.log(e);
						commonsOutputError((e as Error).message);
					} finally {
						this.isRunning = false;
					}
				}
		);

		this.expirer = new Expirer(expiry, database);
		this.cleaner = new Cleaner(lists, database);
		
		commonsGracefulAbortAddCallback((): void => {
			commonsOutputAlert('SIGINT abort flag is set. Aborting maintenance server.');
			this.schedule.stop();
		});
	}
	
	private async perform(action: EAction): Promise<void> {
		switch (action) {
			case EAction.EXPIRE:
				await this.expirer.expire();
				break;
			case EAction.PURGE_EMPTY_DOMAINS:
				await this.cleaner.purgeEmptyDomains();
				break;
			case EAction.PURGE_ORPHAN_URLS:
				await this.cleaner.purgeOrphanUrls(this.hardLimit);
				break;
		}
	}
	
	public start(): void {
		this.schedule.start();
	}
}
