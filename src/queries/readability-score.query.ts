import { AggregationCursor } from 'mongodb';

import { TKeyObject } from 'tscommons-es-core';

import { EStatus } from 'hydra-crawler-ts-assets';

import { CommonsArgs, commonsOutputDoing, commonsOutputError, commonsOutputProgress, commonsOutputResult } from 'nodecommons-es-cli';

import { Lists } from '../classes/lists';
import { Expiry } from '../classes/expiry';

import { DatabaseService } from '../services/database.service';

import { IParserConfig } from '../interfaces/iparser-config';

import { TQuery } from '../types/tquery';

type TResult = {
		url: string;
		stats: {
				paragraphs: number;
				sentences: number;
				words: number;
				fkre: number;
				ari: number;
		};
		score: number;
};

export const QUERY: TQuery = async (
		args: CommonsArgs,
		databaseService: DatabaseService,
		_lists: Lists,
		_expiry: Expiry,
		_parsersConfig: TKeyObject<IParserConfig>
): Promise<void> => {
	const domain: string|undefined = args.getString('domain');
	if (!domain) {
		commonsOutputError('No domain specified');
		return;
	}
	
	commonsOutputDoing(`Scoring and ordering readability of DONE URLs for domain ${domain}`);

	const results: AggregationCursor<TResult> = databaseService.getUrls().aggregate<TResult>([
			{ $match: {
					status: { $ne: EStatus.ARCHIVED },
					domain: domain,
					'headers.content-type': 'text/html'
			} },
			{ $match: {
					'accessibilityMetrics.fkre': { $exists: 1 },
					'accessibilityMetrics.ari': { $exists: 1 }
			} },
			{ $project: {
					url: true,
					stats: {
							paragraphs: '$accessibilityMetrics.paragraphs',
							sentences: '$accessibilityMetrics.sentences',
							words: '$accessibilityMetrics.words',
							fkre: '$accessibilityMetrics.fkre',
							ari: '$accessibilityMetrics.ari'
					}
			} },
			{ $project: {
					url: true,
					stats: true,
					bound: {
							fkre: { $max: [0, { $min: [ 100, '$stats.fkre' ] } ] },
							ari: { $max: [1, { $min: [ 14, '$stats.ari' ] } ] }
					}
			} },
			{ $project: {
					url: true,
					stats: true,
					relative: {
							fkre: { $divide: [ { $subtract: [ 100, '$bound.fkre' ] }, 100 ] },
							ari: { $divide: [ { $subtract: [ '$bound.ari', 1 ] }, 13 ] }
					}
			} },
			{ $project: {
					url: true,
					stats: true,
					score: { $divide: [ { $add: [ '$relative.fkre', '$relative.ari' ] }, 2 ] }
			} },
			{ $sort: { score: -1 } }
	]);

	const matches: TResult[] = [];
	let tally: number = 0;
	while (true) {
		tally++;
		if ((tally % 100) === 0) commonsOutputProgress(`${tally}`);

		const row: TResult|null = await results.next();
		if (row === null) break;
		
		matches.push(row);
	}

	commonsOutputResult(tally);
	
	console.log('url\tparagraphs\tsentences\twords\tfkre\tari\tscore');
	for (const match of matches) {
		console.log(`${match.url}\t${match.stats.paragraphs}\t${match.stats.sentences}\t${match.stats.words}\t${match.stats.fkre}\t${match.stats.ari}\t${match.score}`);
	}
};
