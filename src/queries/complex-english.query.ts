import * as cheerio from 'cheerio';
import mongodb from 'mongodb';

import { commonsArrayUnique, TKeyObject } from 'tscommons-es-core';

import { EStatus } from 'hydra-crawler-ts-assets';
import { IUrl } from 'hydra-crawler-ts-assets';

import { CommonsArgs, commonsOutputDoing, commonsOutputError, commonsOutputProgress, commonsOutputResult } from 'nodecommons-es-cli';
import { commonsHttpReadUrlAsBuffer } from 'nodecommons-es-http';

import { Lists } from '../classes/lists';
import { Expiry } from '../classes/expiry';

import { DatabaseService } from '../services/database.service';

import { ComplexEnglishParser } from '../parsers/complex-english.parser';

import { IParserConfig } from '../interfaces/iparser-config';

import { TQuery } from '../types/tquery';

import { EList } from '../enums/elist';

type TComplexMatch = {
		url: string;
		headers?: {
				'content-type'?: string;
		};
		complexEnglish: string[];
};

export const QUERY: TQuery = async (
		args: CommonsArgs,
		databaseService: DatabaseService,
		lists: Lists,
		_expiry: Expiry,
		parsersConfig: TKeyObject<IParserConfig>
): Promise<void> => {
	const domain: string|undefined = args.getString('domain');
	if (!domain) {
		commonsOutputError('No domain specified');
		return;
	}
	
	const parser: ComplexEnglishParser = new ComplexEnglishParser(
			undefined,
			undefined,
			parsersConfig
	);
	const regexs: Map<string, RegExp> = parser.getRegExs();

	const keyDictionary: TKeyObject<string[]>|undefined = parser.getDictionary();
	if (!keyDictionary) throw new Error('No dictionary available');

	commonsOutputDoing(`Searching for complex english detections for domain ${domain}`);

	const result: mongodb.FindCursor<IUrl> = databaseService.getUrls().find<IUrl>(
			{
					domain: domain,
					status: EStatus.DONE,
					statusCode: 200,
					complexEnglish: { $exists: true }
			},
			{}
	);

	const matches: TComplexMatch[] = [];
	let tally: number = 0;
	while (true) {
		tally++;
		if ((tally % 100) === 0) commonsOutputProgress(`${tally}`);

		const row: IUrl|null = await result.next();
		if (row === null) break;
		
		const typecast: TComplexMatch = row as unknown as TComplexMatch;
		
		if (typecast.headers !== undefined && typecast.headers['content-type'] !== undefined) {
			if (!parser.supports(
					typecast.headers['content-type'],
					lists.match(EList.ALLOW, row.url)
			)) continue;
		}
		
		matches.push({
				url: row.url,
				complexEnglish: typecast.complexEnglish
		});
	}

	commonsOutputResult(tally);
	
	for (const match of matches) {
		console.log('----------------------------------------------------');
		console.log(match.url);
		
		const data: Buffer|undefined = await commonsHttpReadUrlAsBuffer(match.url);
		if (!data) {
			commonsOutputError('Unable to read URL. Skipping');
			continue;
		}
		
		const dom: cheerio.Root|undefined = cheerio.load(data);
		if (!dom) {
			commonsOutputError('Unable to parse HTML. Skipping');
			continue;
		}

		const nodes: cheerio.Cheerio = dom('*')
				.contents()
				// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
				.filter((_index: number, element: cheerio.Element): boolean => (element as any).nodeType === 3);

		const deconstruct: string[] = [];

		dom(nodes).each((_index: number, element: cheerio.Element): void => {
			deconstruct.push(dom(element).text());
		});
		
		const lines: string[] = deconstruct
				.join('\n')
				.replace(/[\t\r\n]+/g, '\n')
				.split('\n')
				.map((s: string): string => s.trim())
				.filter((s: string): boolean => s !== '');
				
		const unique: string[] = commonsArrayUnique(lines);
		
		for (const line of unique) {
			let changed: string = line;
			
			for (const complex of match.complexEnglish) {
				const regex: RegExp|undefined = regexs.get(complex);
				if (!regex) continue;
				
				if (!regex.test(line)) continue;
				
				const suggestions: string[] = keyDictionary[complex];

				const parts: string[] = [ `--${complex}` ];
				if (suggestions.length > 0) parts.push(`++${suggestions.join('/')}`);
				
				changed = changed.replace(regex, `$1[${parts.join('|')}]$2`);
			}
			
			if (changed !== line) {
				console.log(changed);
				console.log('');
			}
		}
	}
};
