import mongodb from 'mongodb';

import { TKeyObject } from 'tscommons-es-core';

import { IUrl, isIUrl } from 'hydra-crawler-ts-assets';
import { TLink, isTLink } from 'hydra-crawler-ts-assets';
import { TDomain, isTDomain } from 'hydra-crawler-ts-assets';
import { EStatus } from 'hydra-crawler-ts-assets';

import { CommonsArgs, commonsOutputDoing, commonsOutputError, commonsOutputProgress, commonsOutputResult, commonsOutputSuccess } from 'nodecommons-es-cli';

import { Lists } from '../classes/lists';
import { Expiry } from '../classes/expiry';

import { DatabaseService } from '../services/database.service';

import { IParserConfig } from '../interfaces/iparser-config';

import { TQuery } from '../types/tquery';

type TOutcome = {
		[ domain: string ]: {
				[ url: string ]: string[];
		};
};

const domainSort: (a: string, b: string) => number = (a: string, b: string): number => {
	if (a.startsWith('http://')) a = a.substr(7);
	if (a.startsWith('https://')) a = a.substr(8);
	if (a.startsWith('www.')) a = a.substr(4);
	if (b.startsWith('http://')) b = b.substr(7);
	if (b.startsWith('https://')) b = b.substr(8);
	if (b.startsWith('www.')) b = b.substr(4);
	
	if (a < b) return -1;
	if (a > b) return 1;
	return 0;
};

export const QUERY: TQuery = async (
		args: CommonsArgs,
		databaseService: DatabaseService,
		_lists: Lists,
		_expiry: Expiry,
		_parsersConfig: TKeyObject<IParserConfig>
): Promise<void> => {
	const regexStr: string|undefined = args.getString('regex');
	if (!regexStr) {
		commonsOutputError('No domain regex specified');
		return;
	}
	const regex: RegExp = new RegExp(regexStr);

	commonsOutputDoing('Searching for matching domains');
	const results: mongodb.FindCursor<TDomain> = databaseService.getDomains().find<TDomain>(
			{
					domain: regex
			},
			{}
	);
	const domains: TDomain[] = await databaseService.listQueryResults<TDomain>(
			results,
			isTDomain
	);
	commonsOutputResult(domains.length);

	const domainUrls: Map<TDomain, IUrl[]> = new Map<TDomain, IUrl[]>();
	for (const domain of domains) {
		commonsOutputDoing(`Searching for URLs for ${domain.domain}`);
		const results2: mongodb.FindCursor<IUrl> = databaseService.getUrls().find<IUrl>(
				{
						domain: domain.domain,
						status: { $ne: EStatus.ARCHIVED }
				},
				{}
		);
		const urls: IUrl[] = await databaseService.listQueryResults<IUrl>(
				results2,
				isIUrl
		);
		commonsOutputResult(urls.length);
		
		domainUrls.set(domain, urls);
	}
	
	commonsOutputDoing('Searching for outgoing links to URLs');
	let tally: number = 0;
	const domainUrlLinks: Map<TDomain, Map<IUrl, TLink[]>> = new Map<TDomain, Map<IUrl, TLink[]>>();
	for (const domain of domains) {
		const urls: IUrl[]|undefined = domainUrls.get(domain);
		if (!urls || !urls.length) continue;
		
		const map: Map<IUrl, TLink[]> = new Map<IUrl, TLink[]>();
		
		for (const url of urls) {
			const results3: mongodb.FindCursor<TLink> = databaseService.getLinks().find<TLink>(
					{
							outgoing: url.url
					},
					{}
			);
			const links: TLink[] = await databaseService.listQueryResults<TLink>(
					results3,
					isTLink
			);
			
			if (links.length === 0) continue;
			
			tally += links.length;
			commonsOutputProgress(tally);
						
			map.set(url, links);
		}
		
		if (map.size === 0) continue;
		
		domainUrlLinks.set(domain, map);
	}
	commonsOutputResult(tally);

	commonsOutputDoing('Constructing JSON object');
	const outcome: TOutcome = {};
	for (const domain of domainUrlLinks.keys()) {
		outcome[domain.domain] = {};
		
		for (const url of domainUrlLinks.get(domain)!.keys()) {
			outcome[domain.domain][url.url] = domainUrlLinks.get(domain)!.get(url)!
					.map((link: TLink): string => link.url);
		}
	}
	commonsOutputSuccess();
	
	if (args.hasAttribute('domains-only')) {
		Object.keys(outcome)
				.sort(domainSort)
				.forEach((domain: string): void => {
					console.log(domain);
				});
	} else if (args.hasAttribute('url-list')) {
		const reverseMap: Map<string, string[]> = new Map<string, string[]>();
		
		for (const domain of Object.keys(outcome)) {
			for (const out of Object.keys(outcome[domain])) {
				for (const url of outcome[domain][out]) {
					if (!reverseMap.has(url)) reverseMap.set(url, []);
					if (!reverseMap.get(url)!.includes(out)) reverseMap.get(url)!.push(out);
				}
			}
		}
		
		const urls: string[] = Array.from(reverseMap.keys())
				.sort(domainSort);
		for (const url of urls) {
			reverseMap.get(url)!.sort(domainSort);
			
			let first: boolean = true;
			for (const out of reverseMap.get(url)!) {
				const tsv: string[] = [
						first ? url : '',
						out
				];
				console.log(tsv.join('\t'));
				
				first = false;
			}
		}
	} else if (args.hasAttribute('json-dump')) {
		console.log(JSON.stringify(outcome));
	}
};
