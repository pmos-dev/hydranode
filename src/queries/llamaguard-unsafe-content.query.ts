import mongodb from 'mongodb';

import { TKeyObject } from 'tscommons-es-core';
import { ECommonsCsvColumnType } from 'tscommons-es-format';

import { IUrl } from 'hydra-crawler-ts-assets';
import { EStatus } from 'hydra-crawler-ts-assets';

import { CommonsArgs, commonsOutputDoing, commonsOutputResult, commonsOutputSuccess } from 'nodecommons-es-cli';
import { CommonsCsv } from 'nodecommons-es-file';

import { Lists } from '../classes/lists';
import { Expiry } from '../classes/expiry';

import { DatabaseService } from '../services/database.service';
import { ELLAMA_GUARD_CATEGORYS, ELlamaGuardCategory, prettyLlamaGuardCategory, TLlamaGuardResult } from '../services/rig-llama-guard.service';

import { IParserConfig } from '../interfaces/iparser-config';

import { TQuery } from '../types/tquery';

type TRow = {
		category: ELlamaGuardCategory;
		meaning: string;
		domain: string;
		url: string;
};

type TUrlWithLlamaGuard = IUrl & { llamaguard: TLlamaGuardResult };

export const QUERY: TQuery = async (
		args: CommonsArgs,
		databaseService: DatabaseService,
		_lists: Lists,
		_expiry: Expiry,
		_parsersConfig: TKeyObject<IParserConfig>
): Promise<void> => {
	const filename: string = args.getString('filename');

	commonsOutputDoing('Searching for URLs marked unsafe');
	const results: mongodb.FindCursor<TUrlWithLlamaGuard> = databaseService.getUrls().find<TUrlWithLlamaGuard>(
			{
					'llamaguard.classification': 'unsafe',
					status: { $in: [
							EStatus.ACTIVE,
							EStatus.DENY,
							EStatus.DISALLOWED,
							EStatus.DONE,
							EStatus.FAILED,
							EStatus.QUEUED
					] }
			},
			{}
	);
	const urls: TUrlWithLlamaGuard[] = await databaseService.listQueryResults<TUrlWithLlamaGuard>(
			results,
			(_t: unknown): _t is TUrlWithLlamaGuard => true
	);
	commonsOutputResult(urls.length);

	commonsOutputDoing('Inverting categorisation');
	const map: Map<ELlamaGuardCategory, IUrl[]> = new Map<ELlamaGuardCategory, IUrl[]>();
	for (const category of ELLAMA_GUARD_CATEGORYS) map.set(category, []);

	for (const url of urls) {
		if (url.llamaguard.classification === 'safe') continue;

		for (const category of url.llamaguard.categories) map.get(category)!.push(url);
	}
	commonsOutputSuccess();

	commonsOutputDoing('Building CSV array');

	const rows: TRow[] = [];
	for (const category of ELLAMA_GUARD_CATEGORYS) {
		const meaning: string = prettyLlamaGuardCategory(category);
		
		for (const url of map.get(category)!) {
			rows.push({
					category: category,
					meaning: meaning,
					domain: url.domain,
					url: url.url
			});
		}
	}

	rows
			.sort((a: TRow, b: TRow): number => {
				if (a.category < b.category) return -1;
				if (a.category > b.category) return 1;

				if (a.domain < b.domain) return -1;
				if (a.domain > b.domain) return 1;

				if (a.url < b.url) return -1;
				if (a.url > b.url) return 1;

				return 0;
			});
			
	commonsOutputSuccess();

	const csv: CommonsCsv = new CommonsCsv([
			{
					name: 'category',
					type: ECommonsCsvColumnType.STRING
			},
			{
					name: 'meaning',
					type: ECommonsCsvColumnType.STRING
			},
			{
					name: 'domain',
					type: ECommonsCsvColumnType.STRING
			},
			{
					name: 'url',
					type: ECommonsCsvColumnType.STRING
			}
	]);
	csv.save(
			rows,
			filename,
			true
	);
};
