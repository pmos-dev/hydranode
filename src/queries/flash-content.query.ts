import mongodb from 'mongodb';

import { TKeyObject } from 'tscommons-es-core';
import { ECommonsCsvColumnType } from 'tscommons-es-format';

import { IUrl, isIUrl } from 'hydra-crawler-ts-assets';
import { TLink, isTLink } from 'hydra-crawler-ts-assets';
import { EStatus } from 'hydra-crawler-ts-assets';

import { CommonsArgs, commonsOutputDoing, commonsOutputProgress, commonsOutputResult, commonsOutputSuccess } from 'nodecommons-es-cli';
import { CommonsCsv } from 'nodecommons-es-file';

import { Lists } from '../classes/lists';
import { Expiry } from '../classes/expiry';

import { DatabaseService } from '../services/database.service';

import { IParserConfig } from '../interfaces/iparser-config';

import { TQuery } from '../types/tquery';

type TRow = {
		src: string;
		dest: string;
};

export const QUERY: TQuery = async (
		args: CommonsArgs,
		databaseService: DatabaseService,
		_lists: Lists,
		_expiry: Expiry,
		_parsersConfig: TKeyObject<IParserConfig>
): Promise<void> => {
	const filename: string = args.getString('filename');
	
	commonsOutputDoing('Searching for FLV and SWF URLs');
	const results: mongodb.FindCursor<IUrl> = databaseService.getUrls().find<IUrl>(
			{
					url: /\.(flv|swf)$/i,
					status: { $in: [
							EStatus.ACTIVE,
							EStatus.DENY,
							EStatus.DISALLOWED,
							EStatus.DONE,
							EStatus.FAILED,
							EStatus.QUEUED
					] }
			},
			{}
	);
	const urls: IUrl[] = await databaseService.listQueryResults<IUrl>(
			results,
			isIUrl
	);
	commonsOutputResult(urls.length);

	commonsOutputDoing('Searching for outgoing links to URLs');
	let tally: number = 0;
	const urlLinks: Map<IUrl, TLink[]> = new Map<IUrl, TLink[]>();
	for (const url of urls) {
		const results3: mongodb.FindCursor<TLink> = databaseService.getLinks().find<TLink>(
				{
						outgoing: url.url
				},
				{}
		);
		const links: TLink[] = await databaseService.listQueryResults<TLink>(
				results3,
				isTLink
		);
		
		if (links.length === 0) continue;
		
		tally += links.length;
		commonsOutputProgress(tally);
					
		urlLinks.set(url, links);
	}
	commonsOutputResult(tally);

	commonsOutputDoing('Building CSV array');

	const rows: TRow[] = [];
	for (const url of urls) {
		for (const link of (urlLinks.get(url) || [])) {
			rows.push({
					src: link.url,
					dest: url.url
			});
		}
	}
	rows
			.sort((a: TRow, b: TRow): number => {
				if (a.src < b.src) return -1;
				if (a.src > b.src) return 1;
				if (a.dest < b.dest) return -1;
				if (a.dest > b.dest) return 1;
				return 0;
			});
			
	commonsOutputSuccess();

	const csv: CommonsCsv = new CommonsCsv([
			{
					name: 'src',
					type: ECommonsCsvColumnType.STRING
			},
			{
					name: 'dest',
					type: ECommonsCsvColumnType.STRING
			}
	]);
	csv.save(
			rows,
			filename,
			true
	);
};
