import mongodb from 'mongodb';

import { commonsOutputDoing, commonsOutputResult, commonsOutputSuccess } from 'nodecommons-es-cli';
import { CommonsApp } from 'nodecommons-es-app';

import { DatabaseService } from '../services/database.service';

import { IInternalHydraCommonDbApp } from './internal-hydra-common.app';

// load the tables into Mongo's memory

export class StartupApp extends CommonsApp implements IInternalHydraCommonDbApp {
	private databaseService: DatabaseService|undefined;
	
	constructor() {
		super('hydra-crawler');
	}

	public getAppName(): string {
		return 'Hydra - Startup';
	}

	public setDatabaseService(
			databaseService: DatabaseService
	): void {
		this.databaseService = databaseService;
	}
	
	public async init(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set yet');
		
		commonsOutputDoing('Connecting to database');
		await this.databaseService.init();
		commonsOutputSuccess();

		await super.init();
	}
	
	public async run(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set');
		
		const dbo: mongodb.Db = this.databaseService.getRawDatabase();
		
		for (const collection of 'domains,urls,links'.split(',')) {
			commonsOutputDoing(`Loading ${collection} collection into memory (simple enumeration)`);
			const tally: number = await dbo.collection(collection).find({}).count();
			commonsOutputResult(tally);
		}
	
		commonsOutputDoing('Loading domains collection into memory (regex enumeration)');
		await this.databaseService.getDomains().find({
				domain: /[0-9]temp[0-9]/
		}).count();
		commonsOutputSuccess();
	
		commonsOutputDoing('Loading urls collection into memory (regex enumeration)');
		await this.databaseService.getUrls().find({
				url: /[0-9]temp[0-9]/
		}).count();
		commonsOutputSuccess();
	
		commonsOutputDoing('Loading links collection into memory (regex enumeration)');
		await this.databaseService.getLinks().find({
				outgoing: /[0-9]temp[0-9]/
		}).count();
		commonsOutputSuccess();
	}
}
