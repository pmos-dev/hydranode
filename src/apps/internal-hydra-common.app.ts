import { TCommonsScheduleTime } from 'tscommons-es-async';

import { CommonsConfigFile } from 'nodecommons-es-config';

import { DatabaseService } from '../services/database.service';

import { IMatch } from '../interfaces/imatch';
import { IExpiry } from '../interfaces/iexpiry';

import { EList } from '../enums/elist';

export interface IInternalHydraCommonApp {
	// NodeCommons methods
	loadConfigFile(file: string): CommonsConfigFile;
	loadRawJsonConfigFile(file: string): unknown;
	start(): Promise<void>;
		
	// App common methods
	getAppName(): string;
}

export interface IInternalHydraCommonDbApp extends IInternalHydraCommonApp {
	// App common methods
	setDatabaseService(databaseService: DatabaseService): void;
}
export function isIInternalHydraCommonDbApp(test: IInternalHydraCommonApp): test is IInternalHydraCommonDbApp {
	return 'setDatabaseService' in test;
}

export interface IInternalHydraCommonListApp extends IInternalHydraCommonDbApp {
	addToList(
			list: EList,
			entries: IMatch[]
	): void;
}
export function isIInternalHydraCommonListApp(test: IInternalHydraCommonApp): test is IInternalHydraCommonListApp {
	if (!isIInternalHydraCommonDbApp(test)) return false;
	return 'addToList' in test;
}

export interface IInternalHydraCommonExpiryApp extends IInternalHydraCommonDbApp {
	addToExpiry(expiries: IExpiry[]): void;
}
export function isIInternalHydraCommonExpiryApp(test: IInternalHydraCommonApp): test is IInternalHydraCommonExpiryApp {
	if (!isIInternalHydraCommonDbApp(test)) return false;
	return 'addToExpiry' in test;
}

export interface IInternalHydraCommonMaintenanceApp extends IInternalHydraCommonDbApp {
	setMaintenanceSchedule(times: TCommonsScheduleTime[]): void;
}
export function isIInternalHydraCommonMaintenanceApp(test: IInternalHydraCommonApp): test is IInternalHydraCommonMaintenanceApp {
	if (!isIInternalHydraCommonDbApp(test)) return false;
	return 'setMaintenanceSchedule' in test;
}
