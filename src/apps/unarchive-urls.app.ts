import mongodb from 'mongodb';

import { EStatus } from 'hydra-crawler-ts-assets';
import { IUrl } from 'hydra-crawler-ts-assets';

import { commonsOutputDoing, commonsOutputFound, commonsOutputProgress, commonsOutputResult, commonsOutputSuccess } from 'nodecommons-es-cli';
import { CommonsApp } from 'nodecommons-es-app';

import { Lists } from '../classes/lists';

import { DatabaseService } from '../services/database.service';

import { IMatch } from '../interfaces/imatch';

import { EList } from '../enums/elist';

import { IInternalHydraCommonListApp } from './internal-hydra-common.app';

// This only unarchives the status in the urls collection. It doesn't move entries in the archives collection back into urls

export class UnarchiveUrlsApp extends CommonsApp implements IInternalHydraCommonListApp {
	private databaseService: DatabaseService|undefined;
	
	private lists: Lists;

	constructor() {
		super('hydra-crawler');

		this.lists = new Lists();
	}

	public getAppName(): string {
		return 'Hydra - Unarchive';
	}

	public setDatabaseService(
			databaseService: DatabaseService
	): void {
		this.databaseService = databaseService;
	}
	
	public addToList(
			list: EList,
			entries: IMatch[]
	): void {
		this.lists.add(list, entries);
	}

	public async init(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set yet');
		
		commonsOutputDoing('Connecting to database');
		await this.databaseService.init();
		commonsOutputSuccess();

		await super.init();
	}
	
	public async run(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set');

		let tally: number = 0;
		let found: number = 0;
		
		commonsOutputDoing('Enumerating ARCHIVED URLS');
		
		const results: mongodb.FindCursor<IUrl> = this.databaseService.getUrls()
				.find<IUrl>(
						{ status: EStatus.ARCHIVED },
						{}
				);
	
		const urls: string[] = [];
		tally = 0; found = 0;
		
		while (true) {
			if ((tally % 100) === 0) commonsOutputFound(tally, found);
			tally++;
	
			const row: IUrl|null = await results.next();
			if (row === null) break;
			
			if (!this.lists.match(EList.ALLOW, row.url)) continue;
	
			urls.push(row.url);
	
			found++;
		}
		commonsOutputResult(found);
	
		commonsOutputDoing('Re-QUEUEING matched URLs');
		tally = 0;
		while (urls.length > 0) {
			commonsOutputProgress(tally);
			tally += 100;
			
			const batch: string[] = urls.slice(0, 100);
			
			await this.databaseService.getUrls().updateMany(
					{ url: { $in: batch } },
					{
							$set: { status: EStatus.QUEUED }
					}
			);
	
			urls.splice(0, 100);
		}
		commonsOutputSuccess();
	}
}
