import { commonsOutputAlert, commonsOutputDoing, commonsOutputInfo, commonsOutputProgress, commonsOutputSuccess } from 'nodecommons-es-cli';
import { CommonsApp } from 'nodecommons-es-app';

import { DatabaseService } from '../services/database.service';

export class PruneArchiveApp extends CommonsApp {
	private databaseService: DatabaseService|undefined;

	constructor() {
		super('hydra-crawler');
	}

	public getAppName(): string {
		return 'Hydra - Prune archive';
	}

	public setDatabaseService(
			databaseService: DatabaseService
	): void {
		this.databaseService = databaseService;
	}

	public async init(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set yet');
		
		commonsOutputDoing('Connecting to database');
		await this.databaseService.init();
		commonsOutputSuccess();

		await super.init();
	}
	
	public async run(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set');

		commonsOutputInfo('Searching for duplicate entries in archiveds table.');
		const duplicates: string[] = await this.databaseService.listDuplicateArchivedUrls();

		if (this.getArgs().hasAttribute('purge')) {
			commonsOutputDoing('Purging duplicates');

			for (const duplicate of duplicates) {
				commonsOutputProgress(duplicate.substring(0, 90));

				await this.databaseService.purgeArchiveDuplicates(duplicate);
			}

			commonsOutputSuccess();
		} else {
			commonsOutputAlert('Specify --purge flag to perform the purge');
		}
	}
}
