import mongodb from 'mongodb';

import { EStatus } from 'hydra-crawler-ts-assets';
import { IUrl } from 'hydra-crawler-ts-assets';

import { commonsOutputDoing, commonsOutputProgress, commonsOutputResult, commonsOutputSuccess } from 'nodecommons-es-cli';
import { CommonsApp } from 'nodecommons-es-app';

import { Lists } from '../classes/lists';

import { DatabaseService } from '../services/database.service';

import { IMatch } from '../interfaces/imatch';

import { EList } from '../enums/elist';

import { IInternalHydraCommonListApp } from './internal-hydra-common.app';

export class ReattemptApp extends CommonsApp implements IInternalHydraCommonListApp {
	private databaseService: DatabaseService|undefined;
	
	private lists: Lists;

	constructor(
			private include: EStatus[]
	) {
		super('hydra-crawler');

		this.lists = new Lists();
	}

	public getAppName(): string {
		return 'Hydra - Reattempt';
	}

	public setDatabaseService(
			databaseService: DatabaseService
	): void {
		this.databaseService = databaseService;
	}
	
	public addToList(
			list: EList,
			entries: IMatch[]
	): void {
		this.lists.add(list, entries);
	}

	public async init(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set yet');
		
		commonsOutputDoing('Connecting to database');
		await this.databaseService.init();
		commonsOutputSuccess();

		await super.init();
	}
	
	public async run(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set');
		
		let tally: number = 0;
		
		commonsOutputDoing(`Searching for allowlisted ${this.include.join(' and ')}`);
		
		const results: mongodb.FindCursor<IUrl> = this.databaseService.getUrls()
				.find<IUrl>(
						{
								status: { $in: this.include }
						},
						{}
				);
	
		const urls: string[] = [];
		tally = 0;
		
		while (true) {
			if ((tally % 100) === 0) commonsOutputProgress(tally);
	
			const row: IUrl|null = await results.next();
			if (row === null) break;
			
			if (!this.lists.match(EList.ALLOW, row.url)) continue;
	
			urls.push(row.url);
	
			tally++;
		}
		commonsOutputResult(tally);
		
		commonsOutputDoing('Requeuing matches');
		tally = 0;
		for (const url of urls) {
			tally++;
			if ((tally % 10) === 0) commonsOutputProgress(tally);
			
			await this.databaseService.getUrls()
					.updateOne({ url: url }, {
							$set: { status: EStatus.QUEUED },
							$unset: { ttl: 1 }
					});
		}
		commonsOutputResult(tally);
	}
}
