import mongodb from 'mongodb';

import { commonsArrayChunk } from 'tscommons-es-core';

import { EStatus } from 'hydra-crawler-ts-assets';
import { IUrl } from 'hydra-crawler-ts-assets';

import { commonsOutputDoing, commonsOutputProgress, commonsOutputResult, commonsOutputSuccess } from 'nodecommons-es-cli';
import { CommonsArgs } from 'nodecommons-es-cli';
import { CommonsApp } from 'nodecommons-es-app';

import { Lists } from '../classes/lists';

import { DatabaseService } from '../services/database.service';

import { IMatch } from '../interfaces/imatch';

import { EList } from '../enums/elist';

import { IInternalHydraCommonListApp } from './internal-hydra-common.app';

export class DenylistApp extends CommonsApp implements IInternalHydraCommonListApp {
	private databaseService: DatabaseService|undefined;
	
	private lists: Lists;

	constructor() {
		super('hydra-crawler');

		this.lists = new Lists();
	}

	public getAppName(): string {
		return 'Hydra - Denylist';
	}

	public setDatabaseService(
			databaseService: DatabaseService
	): void {
		this.databaseService = databaseService;
	}
	
	public addToList(
			list: EList,
			entries: IMatch[]
	): void {
		this.lists.add(list, entries);
	}

	public async init(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set yet');
		
		commonsOutputDoing('Connecting to database');
		await this.databaseService.init();
		commonsOutputSuccess();

		await super.init();
	}
	
	private async forward(limit?: number): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set');

		let tally: number = 0;
		let found: number = 0;
		
		commonsOutputDoing('Searching for non-DENY urls on the denylist');
		
		const urls: string[] = [];
	
		const result: mongodb.FindCursor<IUrl> = this.databaseService.getUrls()
				.find<IUrl>(
						{ status: { $nin: [ EStatus.DENY, EStatus.ARCHIVED ] } },
						{}
				);
		
		while (true) {
			tally++;
			if ((tally % 100) === 0) commonsOutputProgress(`${tally}, ${found}`);
			if (limit !== undefined && tally > limit) break;
	
			const row: IUrl|null = await result.next();
			if (row === null) break;
			
			if (!this.lists.match(EList.DENY, row.url)) continue;
	
			found++;
			urls.push(row.url);
		}
		
		commonsOutputResult(found);
	
		commonsOutputDoing('Marking detected as DENY');
		
		const batches: string[][] = commonsArrayChunk(urls, 100);
		
		tally = 0;
		for (const batch of batches) {
			await this.databaseService.getUrls().updateMany(
					{ url: { $in: batch } },
					{ $set: {
							status: EStatus.DENY
					}, $unset: {
							ttl: true
					} }
			);

			tally += 100;
			commonsOutputProgress(tally);
		}
		commonsOutputSuccess();
	}
	
	private async reverse(limit?: number): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set');

		let tally: number = 0;
		let found: number = 0;
		
		commonsOutputDoing('Searching for DENY urls not on the denylist');
		
		const urls: string[] = [];
	
		const result: mongodb.FindCursor<IUrl> = this.databaseService.getUrls()
				.find<IUrl>(
						{ status: EStatus.DENY },
						{}
				);
		
		while (true) {
			tally++;
			if ((tally % 100) === 0) commonsOutputProgress(`${tally}, ${found}`);
			if (limit !== undefined && tally > limit) break;
	
			const row: IUrl|null = await result.next();
			if (row === null) break;
			
			if (this.lists.match(EList.DENY, row.url)) continue;
	
			found++;
			urls.push(row.url);
		}
		
		commonsOutputResult(found);
	
		commonsOutputDoing('Marking detected as QUEUED');

		const batches: string[][] = commonsArrayChunk(urls, 100);

		tally = 0;
		for (const batch of batches) {
			await this.databaseService.getUrls().updateMany(
					{ url: { $in: batch } },
					{ $set: {
							status: EStatus.QUEUED
					}, $unset: {
							ttl: true
					} }
			);
			
			tally += 100;
			commonsOutputProgress(tally);
		}
		commonsOutputSuccess();
	}
	
	public async run(): Promise<void> {
		const args: CommonsArgs = new CommonsArgs();

		await this.forward(args.getNumberOrUndefined('limit'));
		await this.reverse(args.getNumberOrUndefined('limit'));
	}
}
