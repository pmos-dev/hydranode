import { EStatus } from 'hydra-crawler-ts-assets';

import { commonsOutputDoing, commonsOutputSuccess } from 'nodecommons-es-cli';
import { CommonsApp } from 'nodecommons-es-app';

import { DatabaseService } from '../services/database.service';

import { IInternalHydraCommonDbApp } from './internal-hydra-common.app';

export class RequeueDomainApp extends CommonsApp implements IInternalHydraCommonDbApp {
	private databaseService: DatabaseService|undefined;
	
	constructor(
			private domain: string,
			private done200Only: boolean
	) {
		super('hydra-crawler');
	}

	public getAppName(): string {
		return 'Hydra - Requeue domain';
	}

	public setDatabaseService(
			databaseService: DatabaseService
	): void {
		this.databaseService = databaseService;
	}
	
	public async init(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set yet');
		
		commonsOutputDoing('Connecting to database');
		await this.databaseService.init();
		commonsOutputSuccess();

		await super.init();
	}
	
	public async run(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set');
		
		const conditions: {
				domain: string;
				status: {
						$in: EStatus[];
				};
				statusCode?: number;
		} = {
				domain: this.domain,
				status: { $in: [] }
		};
		
		if (this.done200Only) {
			conditions.status.$in = [ EStatus.DONE ];
			conditions.statusCode = 200;

			commonsOutputDoing(`Marking all 200 DONE as QUEUED for ${this.domain}`);
		} else {
			conditions.status.$in = [
					EStatus.DONE,
					EStatus.FAILED
			];

			commonsOutputDoing(`Marking all DONE and FAILED as QUEUED for ${this.domain}`);
		}
		
		await this.databaseService.getUrls()
				.updateMany(
						conditions
						, { $set: { status: EStatus.QUEUED } }
				);
			
		commonsOutputSuccess();
	}
}
