import * as fs from 'fs';
import * as readline from 'readline';

import { commonsTypeHasPropertyString } from 'tscommons-es-core';

import { commonsOutputDoing, commonsOutputError, commonsOutputProgress, commonsOutputResult, commonsOutputSuccess } from 'nodecommons-es-cli';
import { CommonsApp } from 'nodecommons-es-app';

import { DatabaseService } from '../services/database.service';

import { IInternalHydraCommonDbApp } from './internal-hydra-common.app';

// import urls from an exported list

type TRow = {
		domain: string;
		url: string;
};

function isTRow(test: unknown): test is TRow {
	if (!commonsTypeHasPropertyString(test, 'domain')) return false;
	if (!commonsTypeHasPropertyString(test, 'url')) return false;
	
	return true;
}

export class CrossPopulateImportApp extends CommonsApp implements IInternalHydraCommonDbApp {
	private databaseService: DatabaseService|undefined;
	
	constructor(
			private filename: string
	) {
		super('hydra-crawler');
	}

	public getAppName(): string {
		return 'Hydra - Cross Populate Export';
	}

	public setDatabaseService(
			databaseService: DatabaseService
	): void {
		this.databaseService = databaseService;
	}
	
	public async init(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set yet');
		
		commonsOutputDoing('Connecting to database');
		await this.databaseService.init();
		commonsOutputSuccess();

		await super.init();
	}
	
	public async run(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set');

		const fileStream: fs.ReadStream = fs.createReadStream(this.filename);

		const rl: readline.Interface = readline.createInterface({
				input: fileStream,
				crlfDelay: Infinity
		});

		commonsOutputDoing('Importing urls');
		
		let tally: number = 0;
		for await (const line of rl) {
			if (line.trim() === '') continue;

			if (tally % 1000 === 0) commonsOutputProgress(tally);
			
			const json: unknown = JSON.parse(line);
			if (!isTRow(json)) {
				commonsOutputError('Invalid row. Skipping');
				continue;
			}
			
			await this.databaseService.queue(json.url, false);
			tally++;
		}
		commonsOutputResult(tally);

		fileStream.close();
	}
}
