import { commonsOutputDoing, commonsOutputInfo, commonsOutputSuccess } from 'nodecommons-es-cli';
import { CommonsApp } from 'nodecommons-es-app';

import { DatabaseService } from '../services/database.service';

export class MoveToArchiveApp extends CommonsApp {
	private databaseService: DatabaseService|undefined;

	constructor() {
		super('hydra-crawler');
	}

	public getAppName(): string {
		return 'Hydra - Move to archive';
	}

	public setDatabaseService(
			databaseService: DatabaseService
	): void {
		this.databaseService = databaseService;
	}

	public async init(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set yet');
		
		commonsOutputDoing('Connecting to database');
		await this.databaseService.init();
		commonsOutputSuccess();

		await super.init();
	}
	
	public async run(): Promise<void> {
		const batchSize: number = this.getArgs().getNumber('batch-size');
		const limit: number|undefined = this.getArgs().getNumberOrUndefined('limit');

		if (!this.databaseService) throw new Error('Database service has not been set');

		commonsOutputInfo('Moving ARCHIVED urls into archives collection.');
		await this.databaseService.moveArchivedUrlsToArchive(batchSize, limit);
	}
}
