import { commonsInputReadAllStdIn, commonsOutputDoing, commonsOutputProgress, commonsOutputResult, commonsOutputSuccess } from 'nodecommons-es-cli';
import { CommonsApp } from 'nodecommons-es-app';

import { DatabaseService } from '../services/database.service';

import { IInternalHydraCommonDbApp } from './internal-hydra-common.app';

export class ImportApp extends CommonsApp implements IInternalHydraCommonDbApp {
	private databaseService: DatabaseService|undefined;
	
	constructor() {
		super('hydra-crawler');
	}

	public getAppName(): string {
		return 'Hydra - Import';
	}

	public setDatabaseService(
			databaseService: DatabaseService
	): void {
		this.databaseService = databaseService;
	}
	
	public async init(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set yet');
		
		commonsOutputDoing('Connecting to database');
		await this.databaseService.init();
		commonsOutputSuccess();

		await super.init();
	}
	
	public async run(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set');
		
		commonsOutputDoing('Loading URLs from stdin');
		const urls: string[] = await commonsInputReadAllStdIn();
		commonsOutputResult(urls.length);
		
		commonsOutputDoing('Seeding into queue');
		let tally: number = 0; let done: number = 0;
		for (const url of urls) {
			if ((++tally % 100) === 0) commonsOutputProgress(`${tally}, imported ${done}`);
			
			const outcome: boolean = await this.databaseService.queue(url);
			if (outcome) {
				done++;
			}
		}
		commonsOutputResult(done);
	}
}
