import * as express from 'express';

import { commonsStringSplitString, commonsTypeIsTKeyObject, TKeyObject } from 'tscommons-es-core';
import { TCommonsScheduleTime } from 'tscommons-es-async';

import { commonsOutputDebug, commonsOutputDoing, commonsOutputInfo, commonsOutputSuccess } from 'nodecommons-es-cli';
import { commonsExpressBuildDefaultServer, ICommonsExpressConfig, ICommonsRequestWithStrictParams, isICommonsExpressConfig } from 'nodecommons-es-express';
import { CommonsStrictExpressServer } from 'nodecommons-es-express';
import { CommonsRestServer } from 'nodecommons-es-rest';
import { CommonsSocketIoApp } from 'nodecommons-es-app-socket-io';

import { AutocompleteApi } from '../apis/autocomplete.api';
import { BugsApi } from '../apis/bugs.api';
import { CrawlApi } from '../apis/crawl.api';
import { DomainsApi } from '../apis/domains.api';
import { ImagesApi } from '../apis/images.api';
import { StatisticsApi } from '../apis/statistics.api';
import { TestApi } from '../apis/test.api';
import { UrlsApi } from '../apis/urls.api';

import { Lists } from '../classes/lists';
import { Expiry } from '../classes/expiry';
import { Tracker } from '../classes/tracker';
import { Crawler } from '../classes/crawler';

import { SocketIoServer } from '../servers/socket-io.server';
import { CrawlServer } from '../servers/crawl.server';
import { MaintenanceServer } from '../servers/maintenance.server';

import { DatabaseService } from '../services/database.service';

import { ServerParser } from '../parsers/server.parser';
import { HyperlinksParser } from '../parsers/hyperlinks.parser';
import { ImageTagsParser } from '../parsers/image-tags.parser';
import { JpegParser } from '../parsers/jpeg.parser';
import { SpellingParser } from '../parsers/spelling.parser';
import { BadWordsParser } from '../parsers/bad-words.parser';
import { PhpErrorParser } from '../parsers/php-error.parser';
import { AspErrorParser } from '../parsers/asp-error.parser';
import { ComplexEnglishParser } from '../parsers/complex-english.parser';
import { AccessibilityMetricsParser } from '../parsers/accessibility-metrics.parser';
// import { OffenceParser } from '../parsers/offence.parser';
// import { InterestParser } from '../parsers/interest.parser';
import { LlamaGuardParser } from '../parsers/llama-guard.parser';

import { IParserConfig, isIParserConfig } from '../interfaces/iparser-config';
import { IMatch } from '../interfaces/imatch';
import { IExpiry } from '../interfaces/iexpiry';

import { THydraConfig, isTHydraConfig } from '../types/thydra-config';
import { TCrawlConfig, isTCrawlConfig } from '../types/tcrawl-config';
import { TRobotsConfig, isTRobotsConfig } from '../types/trobots-config';

import { EList } from '../enums/elist';

import {
		IInternalHydraCommonListApp,
		IInternalHydraCommonExpiryApp,
		IInternalHydraCommonMaintenanceApp
} from './internal-hydra-common.app';

export class HydraApp extends CommonsSocketIoApp<SocketIoServer> implements IInternalHydraCommonListApp, IInternalHydraCommonExpiryApp, IInternalHydraCommonMaintenanceApp {
	private databaseService: DatabaseService|undefined;

	private lists: Lists;
	private expiry: Expiry;
	
	private hydraConfig: THydraConfig;
	private crawlConfig: TCrawlConfig;
	private parsersConfig: TKeyObject<IParserConfig>;
	private robotsConfig: TRobotsConfig;
	
	private crawlServer: CrawlServer|undefined;
	
	private maintenanceTimes: TCommonsScheduleTime[] = [];
	private maintenanceServer: MaintenanceServer|undefined;
	
	private tracker: Tracker|undefined;
	
	constructor() {
		super('hydra-crawler');

		this.lists = new Lists();
		this.expiry = new Expiry();
		
		const hydraConfig: unknown = this.getConfigArea('hydra');
		if (!isTHydraConfig(hydraConfig)) throw new Error('Hydra config is not valid');
		this.hydraConfig = hydraConfig;

		const crawlConfig: unknown = this.getConfigArea('crawl');
		if (!isTCrawlConfig(crawlConfig)) throw new Error('Crawl config is not valid');
		this.crawlConfig = crawlConfig;
		
		const parsersConfig: unknown = this.getConfigArea('parsers');
		if (!commonsTypeIsTKeyObject<IParserConfig>(parsersConfig, isIParserConfig)) throw new Error('Parsers config is not valid');
		this.parsersConfig = parsersConfig;
		
		const robotsConfig: unknown = this.getConfigArea('robots');
		if (!isTRobotsConfig(robotsConfig)) throw new Error('Robots config is not valid');
		this.robotsConfig = robotsConfig;
	}

	protected override buildHttpServer(): CommonsStrictExpressServer {
		const expressConfig: unknown = this.getConfigArea('express');
		if (!isICommonsExpressConfig(expressConfig)) throw new Error('Invalid express config');

		return commonsExpressBuildDefaultServer(this.httpConfig.port, expressConfig.bodyParserLimit);
	}

	protected buildSocketIoServer(
			expressServer: CommonsStrictExpressServer,
			expressConfig: ICommonsExpressConfig
	): SocketIoServer {
		return new SocketIoServer(
				expressServer,
				expressConfig
		);
	}

	public getAppName(): string {
		return 'Hydra';
	}

	public setDatabaseService(
			databaseService: DatabaseService
	): void {
		this.databaseService = databaseService;
	}
	
	public addToList(
			list: EList,
			entries: IMatch[]
	): void {
		this.lists.add(list, entries);
	}
	
	public addToExpiry(
			expiries: IExpiry[]
	): void {
		this.expiry.add(expiries);
	}
	
	public setMaintenanceSchedule(
			times: TCommonsScheduleTime[]
	): void {
		this.maintenanceTimes = times;
	}
	
	public async init(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set yet');

		this.install(
				(restServer: CommonsRestServer<ICommonsRequestWithStrictParams, express.Response>, path: string): void => {
					if (!this.databaseService) throw new Error('Database service has not been defined yet');
					
					commonsOutputDoing('Installing auto-complete API');
					new AutocompleteApi(
							restServer,
							this.databaseService,
							path
					);
					commonsOutputSuccess();
					
					commonsOutputDoing('Installing bugs API');
					new BugsApi(
							restServer,
							this.databaseService,
							path
					);
					commonsOutputSuccess();
					
					commonsOutputDoing('Installing crawl API');
					new CrawlApi(
							restServer,
							this.databaseService,
							path
					);
					commonsOutputSuccess();
					
					commonsOutputDoing('Installing domains API');
					new DomainsApi(
							restServer,
							this.databaseService,
							path
					);
					commonsOutputSuccess();
					
					commonsOutputDoing('Installing images API');
					new ImagesApi(
							restServer,
							this.databaseService,
							path
					);
					commonsOutputSuccess();
					
					commonsOutputDoing('Installing statistics API');
					new StatisticsApi(
							restServer,
							this.databaseService,
							path
					);
					commonsOutputSuccess();
					
					commonsOutputDoing('Installing test API');
					new TestApi(
							restServer,
							path
					);
					commonsOutputSuccess();
					
					commonsOutputDoing('Installing urls API');
					new UrlsApi(
							restServer,
							this.databaseService,
							path
					);
					commonsOutputSuccess();
				}
		);

		await super.init();
		
		const socketIoServer: SocketIoServer|undefined = this.getSocketIoServer();
		if (!socketIoServer) throw new Error('SocketIo server has not been set yet');

		this.tracker = new Tracker(
				this.databaseService,
				socketIoServer
		);
		
		this.crawlServer = new CrawlServer(
				this.databaseService,
				this.hydraConfig,
				this.crawlConfig,
				this.parsersConfig,
				this.robotsConfig,
				this.lists,
				this.tracker
		);
		
		commonsOutputDoing('Connecting to database');
		await this.databaseService.init();
		commonsOutputSuccess();

		for (const parser of [
				ServerParser,
				HyperlinksParser,
				ImageTagsParser,
				JpegParser,
				SpellingParser,
				BadWordsParser,
				PhpErrorParser,
				AspErrorParser,
				ComplexEnglishParser,
				AccessibilityMetricsParser,
				// OffenceParser,
				// InterestParser,
				LlamaGuardParser
		]) {
			commonsOutputDebug(`Installing parser ${parser.name}`);
			this.crawlServer.addParser(parser);
			await this.databaseService.initParser(parser);
		}

		const maintenanceHardLimit: number|undefined = this.getArgs().getNumberOrUndefined('maintenance-hard-limit');
		if (maintenanceHardLimit) commonsOutputDebug(`Setting maintenance hard limit to ${maintenanceHardLimit}`);

		this.maintenanceServer = new MaintenanceServer(
				this.maintenanceTimes,
				this.expiry,
				this.lists,
				this.databaseService,
				this.crawlServer,
				maintenanceHardLimit	// to prevent running out of heap on very large collections
		);
	}
	
	protected listening(): void {
		void (async (): Promise<void> => {
			if (!this.crawlServer) throw new Error('No crawl server instantiated! This should not be possible');
			
			await this.crawlServer.start();
			
			this.abort();
		})();
	}

	public async run(): Promise<void> {
		const singleUrl: string|undefined = this.getArgs().getStringOrUndefined('single-url');
		if (singleUrl) {
			if (!this.databaseService) throw new Error('Database service has not been set yet');
			if (!this.crawlServer) throw new Error('Crawl server has not been set yet');
			
			const whatwg: URL = new URL(singleUrl);
			
			const crawler: Crawler = new Crawler(
					whatwg.hostname,
					this.databaseService,
					this.crawlConfig,
					this.parsersConfig,
					this.robotsConfig,
					this.crawlServer.listParsers(),
					this.lists
			);
			
			commonsOutputInfo(`Running single crawl of: ${singleUrl}`);
			const outcome: number = await crawler.fetch(singleUrl, false);
			commonsOutputInfo(`HTTP status code outcome was ${outcome}`);
			
			return;
		}

		const domainRestrictTo: string|undefined = this.getArgs().getStringOrUndefined('domain-restrict-to');
		if (domainRestrictTo) {
			const domains: string[] = commonsStringSplitString(domainRestrictTo, ',', true, true);
			commonsOutputDebug(`Restricting crawl domains to: ${domains.join(',')}`);

			if (!this.crawlServer) throw new Error('CrawlServer has not been defined! This should not be possible');
			this.crawlServer.restrictTo = domains;
		}
		
		if (!this.tracker) throw new Error('Tracker has not been defined! This should not be possible');
		if (!this.maintenanceServer) throw new Error('Maintenance server has not been defined! This should not be possible');
		
		this.maintenanceServer.start();
		void this.tracker.start();
		
		await super.run();
	}
	
	protected async shutdown(): Promise<void> {
		if (!this.crawlServer) throw new Error('No crawl server instantiated! This should not be possible');
		
		await this.crawlServer.shutdown();
		
		await super.shutdown();
	}
}
