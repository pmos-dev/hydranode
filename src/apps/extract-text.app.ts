import * as http from 'http';
import * as https from 'https';

import { commonsOutputDoing, commonsOutputSuccess } from 'nodecommons-es-cli';
import { CommonsApp } from 'nodecommons-es-app';

import { Crawler } from '../classes/crawler';

import { DatabaseService } from '../services/database.service';

import { TextParser } from '../parsers/text.parser';

import { IRequestOutcome } from '../interfaces/irequest-outcome';
import { IParserConfig } from '../interfaces/iparser-config';

import { IInternalHydraCommonApp } from './internal-hydra-common.app';

// extract the text from the HTML of a URL

class TextParserInstance extends TextParser<IParserConfig> {
	// eslint-disable-next-line @typescript-eslint/require-await
	protected async parseText(
			_database: DatabaseService,
			text: string
	): Promise<void> {
		console.log(text);
	}
}

export class ExtractTextApp extends CommonsApp implements IInternalHydraCommonApp {
	constructor(
			private url: string
	) {
		super('hydra-crawler');
	}

	public getAppName(): string {
		return 'Hydra - Extract Text';
	}

	public async run(): Promise<void> {
		commonsOutputDoing(`Downloading from URL at ${this.url}`);
		
		const whatwg: URL = new URL(this.url);
		
		let handler: typeof http|typeof https|undefined;
		switch (whatwg.protocol) {
			case 'http:':	handler = http; break;
			case 'https:':	handler = https; break;
			default:
				throw new Error(`unable to handle protocol ${whatwg.protocol}`);
		}
		
		const outcome: IRequestOutcome = await Crawler.request(
				handler,
				this.url,
				10000,
				4096000
		);
		commonsOutputSuccess();

		const parser: TextParserInstance = new TextParserInstance(
				outcome
		);
		await parser.parse(undefined as unknown as DatabaseService);
	}
}
