import { commonsTypeIsTKeyObject, TKeyObject } from 'tscommons-es-core';

import { commonsOutputDie, commonsOutputDoing, commonsOutputSuccess } from 'nodecommons-es-cli';
import { CommonsApp } from 'nodecommons-es-app';

import { Lists } from '../classes/lists';
import { Expiry } from '../classes/expiry';

import { DatabaseService } from '../services/database.service';

import { IParserConfig, isIParserConfig } from '../interfaces/iparser-config';
import { IMatch } from '../interfaces/imatch';
import { IExpiry } from '../interfaces/iexpiry';

import { TQuery } from '../types/tquery';

import { EList } from '../enums/elist';

import { QUERY as QUERY_COMPLEX_ENGLISH } from '../queries/complex-english.query';
import { QUERY as QUERY_FLASH_CONTENT } from '../queries/flash-content.query';
import { QUERY as QUERY_LINKING_TO_DOMAINS } from '../queries/linking-to-domains.query';
import { QUERY as QUERY_READABILITY_SCORE } from '../queries/readability-score.query';
import { QUERY as QUERY_LLAMAGUARD_UNSAFE_CONTENT } from '../queries/llamaguard-unsafe-content.query';

import { IInternalHydraCommonDbApp } from './internal-hydra-common.app';

export class QueryApp extends CommonsApp implements IInternalHydraCommonDbApp {
	private databaseService: DatabaseService|undefined;

	private lists: Lists;
	private expiry: Expiry;

	private parsersConfig: TKeyObject<IParserConfig>;
	
	constructor(
			private queryName: string
	) {
		super('hydra-crawler');
		
		if (/[^-a-z0-9]/i.test(queryName)) throw new Error('Invalid query name');

		this.lists = new Lists();
		this.expiry = new Expiry();

		const parsersConfig: unknown = this.getConfigArea('parsers');
		if (!commonsTypeIsTKeyObject<IParserConfig>(parsersConfig, isIParserConfig)) throw new Error('Parsers config is not valid');
		this.parsersConfig = parsersConfig;
	}

	public getAppName(): string {
		return 'Hydra - Query';
	}

	public setDatabaseService(
			databaseService: DatabaseService
	): void {
		this.databaseService = databaseService;
	}
	
	public addToList(
			list: EList,
			entries: IMatch[]
	): void {
		this.lists.add(list, entries);
	}
	
	public addToExpiry(
			expiries: IExpiry[]
	): void {
		this.expiry.add(expiries);
	}
	
	public async init(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set yet');
		
		commonsOutputDoing('Connecting to database');
		await this.databaseService.init();
		commonsOutputSuccess();

		await super.init();
	}
	
	public async run(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set');

		let query: TQuery|undefined;

		if (this.queryName === 'complex-english') query = QUERY_COMPLEX_ENGLISH;
		if (this.queryName === 'flash-content') query = QUERY_FLASH_CONTENT;
		if (this.queryName === 'linking-to-domains') query = QUERY_LINKING_TO_DOMAINS;
		if (this.queryName === 'readability-score') query = QUERY_READABILITY_SCORE;
		if (this.queryName === 'llamaguard-unsafe-content') query = QUERY_LLAMAGUARD_UNSAFE_CONTENT;

		if (!query) commonsOutputDie('No such available query');

		await query(
				this.getArgs(),
				this.databaseService,
				this.lists,
				this.expiry,
				this.parsersConfig
		);
	}
}
