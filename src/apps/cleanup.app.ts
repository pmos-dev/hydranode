import mongodb from 'mongodb';

import { commonsTypeHasPropertyNumber } from 'tscommons-es-core';

import { IUrl, isIUrl } from 'hydra-crawler-ts-assets';
import { TLink } from 'hydra-crawler-ts-assets';
import { EStatus } from 'hydra-crawler-ts-assets';

import { commonsOutputDoing, commonsOutputPercent, commonsOutputProgress, commonsOutputResult, commonsOutputSuccess } from 'nodecommons-es-cli';
import { CommonsApp } from 'nodecommons-es-app';

import { Lists } from '../classes/lists';
import { Cleaner } from '../classes/cleaner';

import { DatabaseService } from '../services/database.service';

import { IMatch } from '../interfaces/imatch';

import { EList } from '../enums/elist';

import { IInternalHydraCommonListApp } from './internal-hydra-common.app';

export class CleanupApp extends CommonsApp implements IInternalHydraCommonListApp {
	private databaseService: DatabaseService|undefined;
	
	private lists: Lists;

	constructor(
			private includeReset: boolean = false,
			private includeLinks: boolean = false
	) {
		super('hydra-crawler');

		this.lists = new Lists();
	}

	public getAppName(): string {
		return 'Hydra - Cleanup';
	}

	public setDatabaseService(
			databaseService: DatabaseService
	): void {
		this.databaseService = databaseService;
	}
	
	public addToList(
			list: EList,
			entries: IMatch[]
	): void {
		this.lists.add(list, entries);
	}

	public async init(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set yet');
		
		commonsOutputDoing('Connecting to database');
		await this.databaseService.init();
		commonsOutputSuccess();

		await super.init();
	}
	
	private async reset(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set');

		commonsOutputDoing('Enumerating urls for DENY, FAILED, DEAD and DISALLOWED');

		const urls: string[] = [];
		
		let tally: number = 0;
		while (true) {
			const result: mongodb.FindCursor<IUrl> = this.databaseService.getUrls()
					.find({ status: { $in: [
							EStatus.DENY,
							EStatus.FAILED,
							EStatus.DEAD,
							EStatus.DISALLOWED
					] } })
					.sort({ _id: 1 })
					.skip(tally);
			
			try {
				while (true) {
					tally++;
					if ((tally % 100) === 0) commonsOutputProgress(tally);
	
					const row: IUrl|null = await result.next();
					if (row === null) break;

					urls.push(row.url);
				}
				
				break;
			} catch (err) {
				if (!commonsTypeHasPropertyNumber(err, 'code') || err.code !== 43) throw err;
			}
		}
		
		commonsOutputResult(tally);

		commonsOutputDoing('Resetting links for DENY, FAILED, DEAD and DISALLOWED');
		let i: number = 0;
		for (const url of urls) {
			i++;
			if ((i % 100) === 0) commonsOutputPercent(i, urls.length);

			await this.databaseService.getLinks().deleteMany({ url: url });
		}
		commonsOutputSuccess();
	}
	
	private async links(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set');

		commonsOutputDoing('Searching for link orphans');
		let tally: number = 0;
		
		const result: mongodb.FindCursor<TLink> = this.databaseService.getLinks().find<TLink>({}, {});
		
		let batch: string[] = [];
		const orphans: string[] = [];
		while (true) {
			tally++;
			if ((tally % 1000) === 0) commonsOutputProgress(`${tally}, ${orphans.length}`);

			const link: TLink|null = await result.next();
			if (link === null) break;
			
			if (!batch.includes(link.url)) batch.push(link.url);
			if (!batch.includes(link.outgoing)) batch.push(link.outgoing);
			
			if (batch.length < 10000) continue;

			const result2: mongodb.FindCursor<IUrl> = this.databaseService.getUrls().find(
					{ url: { $in: batch } }
			);
			
			const matches: string[] = (await this.databaseService.listQueryResults(result2, isIUrl))
					.map((u: IUrl): string => u.url);
			
			for (const l of batch) {
				if (!matches.includes(l)) orphans.push(l);
			}
			
			batch = [];
		}
		commonsOutputResult(orphans.length);
		
		commonsOutputDoing('Removing orphan links');
		tally = 0;
		for (const orphan of orphans) {
			tally++;
			if ((tally % 100) === 0) commonsOutputProgress(`${tally}`);
			await this.databaseService.getLinks().deleteMany({ url: orphan });
			await this.databaseService.getLinks().deleteMany({ outgoing: orphan });
		}
		commonsOutputSuccess();
	}
	
	public async run(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set');

		if (this.includeReset) await this.reset();
		if (this.includeLinks) await this.links();

		const hardLimit: number|undefined = this.getArgs().getNumberOrUndefined('hard-limit');

		const cleaner: Cleaner = new Cleaner(this.lists, this.databaseService);
		await cleaner.purgeOrphanUrls(hardLimit);
		await cleaner.purgeEmptyDomains();
	}
}
