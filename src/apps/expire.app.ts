import { commonsOutputDoing, commonsOutputSuccess } from 'nodecommons-es-cli';
import { CommonsArgs } from 'nodecommons-es-cli';
import { CommonsApp } from 'nodecommons-es-app';

import { Lists } from '../classes/lists';
import { Expirer } from '../classes/expirer';
import { Expiry } from '../classes/expiry';

import { DatabaseService } from '../services/database.service';

import { IMatch } from '../interfaces/imatch';
import { IExpiry } from '../interfaces/iexpiry';

import { EList } from '../enums/elist';

import { IInternalHydraCommonListApp, IInternalHydraCommonExpiryApp } from './internal-hydra-common.app';

export class ExpireApp extends CommonsApp implements IInternalHydraCommonListApp, IInternalHydraCommonExpiryApp {
	private databaseService: DatabaseService|undefined;
	private lists: Lists;
	private expiry: Expiry;
	
	constructor(
			private threshold: Date|undefined
	) {
		super('hydra-crawler');

		this.lists = new Lists();
		this.expiry = new Expiry();
	}

	public getAppName(): string {
		return 'Hydra - Expire';
	}

	public setDatabaseService(
			databaseService: DatabaseService
	): void {
		this.databaseService = databaseService;
	}
	
	public addToList(
			list: EList,
			entries: IMatch[]
	): void {
		this.lists.add(list, entries);
	}

	public addToExpiry(
			expiries: IExpiry[]
	): void {
		this.expiry.add(expiries);
	}

	public async init(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set yet');
		
		commonsOutputDoing('Connecting to database');
		await this.databaseService.init();
		commonsOutputSuccess();

		await super.init();
	}
	
	public async run(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set');

		const args: CommonsArgs = new CommonsArgs();

		const expirer: Expirer = new Expirer(this.expiry, this.databaseService);
		
		if (this.threshold !== undefined) {
			await expirer.expireFixed(this.threshold);
		} else {
			await expirer.expire(
					args.getNumberOrUndefined('limit')
			);
		}
	}
}
