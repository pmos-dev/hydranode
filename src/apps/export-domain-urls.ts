import mongodb from 'mongodb';

import { EStatus } from 'hydra-crawler-ts-assets';

import { commonsOutputDoing, commonsOutputProgress, commonsOutputResult, commonsOutputSuccess } from 'nodecommons-es-cli';
import { CommonsApp } from 'nodecommons-es-app';

import { DatabaseService } from '../services/database.service';

import { IInternalHydraCommonDbApp } from './internal-hydra-common.app';

// export the QUEUED, DONE and FAILED urls for importing into another instance

export class ExportDomainUrlsApp extends CommonsApp implements IInternalHydraCommonDbApp {
	private databaseService: DatabaseService|undefined;
	
	constructor(
			private domain: string
	) {
		super('hydra-crawler');
	}

	public getAppName(): string {
		return 'Hydra - Export Domain Urls';
	}

	public setDatabaseService(
			databaseService: DatabaseService
	): void {
		this.databaseService = databaseService;
	}
	
	public async init(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set yet');
		
		commonsOutputDoing('Connecting to database');
		await this.databaseService.init();
		commonsOutputSuccess();

		await super.init();
	}
	
	public async run(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set');
	
		const validOnly: boolean = this.getArgs().hasAttribute('valid-only');
		const pagesOnly: boolean = this.getArgs().hasAttribute('pages-only');
		const mergeHttp: boolean = this.getArgs().hasAttribute('merge-http');

		const matchPipeline: mongodb.BSON.Document[] = [
				{ $match: { domain: this.domain } }
		];
		if (validOnly) {
			matchPipeline.push(
					{ $match: {
							status: { $in: [ EStatus.DONE, EStatus.QUEUED, EStatus.ACTIVE ] },
							statusCode: { $in: [ 200, 204 ] }
					} }
			);
		}
		if (pagesOnly) {
			matchPipeline.push(...[
					{ $match: {
							url: { $not: /[?]/ }
					} },
					{ $match: {
							url: { $not: /(gif|jpeg|jpg|png|webp|avif)$/i }
					} }
			]);
		}

		const cursor: mongodb.AggregationCursor<{ url: string }> = this.databaseService.getUrls()
				.aggregate([
						...matchPipeline,
						{ $project: { _id: false, url: true } }
				]);

		// safer to do this directly rather than a call to listQueryResults
		// ditto not using CommonsFile

		commonsOutputDoing(`Exporting URLs for domain ${this.domain}`);
		
		const urls: string[] = [];
		while (true) {
			if (urls.length % 1000 === 0) commonsOutputProgress(urls.length);
			
			const row: { url: string }|null = await cursor.next();
			if (row === null) break;
			
			let url: string = row.url
					.trim()
					.replace(/[?].*$/, '')
					.replace(/\/index\.(htm|html)$/i, '/')
					.replace(/\/$/, '')
					.trim();

			if (mergeHttp) {
				url = url.replace(/^http(s?):/, '');
			}

			if (!urls.includes(url)) urls.push(url);
		}
		commonsOutputResult(urls.length);

		urls.sort();
		for (const url of urls) {
			console.log(url);
		}
	}
}
