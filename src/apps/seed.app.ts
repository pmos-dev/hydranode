import { commonsOutputDoing, commonsOutputProgress, commonsOutputResult, commonsOutputSuccess } from 'nodecommons-es-cli';
import { CommonsApp } from 'nodecommons-es-app';

import { Lists } from '../classes/lists';

import { DatabaseService } from '../services/database.service';

import { IMatch } from '../interfaces/imatch';

import { EList } from '../enums/elist';

import { IInternalHydraCommonListApp } from './internal-hydra-common.app';

export class SeedApp extends CommonsApp implements IInternalHydraCommonListApp {
	private databaseService: DatabaseService|undefined;
	
	private lists: Lists;

	constructor() {
		super('hydra-crawler');

		this.lists = new Lists();
	}

	public getAppName(): string {
		return 'Hydra - Seed';
	}

	public setDatabaseService(
			databaseService: DatabaseService
	): void {
		this.databaseService = databaseService;
	}
	
	public addToList(
			list: EList,
			entries: IMatch[]
	): void {
		this.lists.add(list, entries);
	}

	public async init(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set yet');
		
		commonsOutputDoing('Connecting to database');
		await this.databaseService.init();
		commonsOutputSuccess();

		await super.init();
	}
	
	public async run(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set');
		
		commonsOutputDoing('Building allowlist hostname seeds');
		const hostnames: string[] = this.lists.listHostnames(EList.ALLOW);
		
		const unique: string[] = [];
		for (const hostname of hostnames) {
			if (!unique.includes(hostname)) unique.push(`http://${hostname}/`);
			if (!unique.includes(`www.${hostname}`)) unique.push(`http://www.${hostname}/`);
			commonsOutputProgress(unique.length);
		}
		
		commonsOutputResult(unique.length);
	
		commonsOutputDoing('Seeding into queue');
		let tally: number = 0;
		for (const url of unique) {
			const outcome: boolean = await this.databaseService.queue(url);
			if (outcome) {
				tally++;
				commonsOutputProgress(tally);
			}
		}
		commonsOutputResult(tally);
	}
}
