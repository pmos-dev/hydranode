import * as fs from 'fs';

import mongodb from 'mongodb';

import { IUrl } from 'hydra-crawler-ts-assets';
import { EStatus } from 'hydra-crawler-ts-assets';

import { commonsOutputDoing, commonsOutputProgress, commonsOutputResult, commonsOutputSuccess } from 'nodecommons-es-cli';
import { CommonsApp } from 'nodecommons-es-app';

import { DatabaseService } from '../services/database.service';

import { IInternalHydraCommonDbApp } from './internal-hydra-common.app';

// export the QUEUED, DONE and FAILED urls for importing into another instance

export class CrossPopulateExportApp extends CommonsApp implements IInternalHydraCommonDbApp {
	private databaseService: DatabaseService|undefined;
	
	constructor(
			private filename: string
	) {
		super('hydra-crawler');
	}

	public getAppName(): string {
		return 'Hydra - Cross Populate Export';
	}

	public setDatabaseService(
			databaseService: DatabaseService
	): void {
		this.databaseService = databaseService;
	}
	
	public async init(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set yet');
		
		commonsOutputDoing('Connecting to database');
		await this.databaseService.init();
		commonsOutputSuccess();

		await super.init();
	}
	
	public async run(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set');
		
		const results: mongodb.FindCursor<IUrl> = this.databaseService.getUrls().find<IUrl>({
				status: { $in: [
						EStatus.QUEUED,
						EStatus.DONE,
						EStatus.FAILED
				] }
		}, {});
		
		// safer to do this directly rather than a call to listQueryResults
		// ditto not using CommonsFile
		
		const fd: number = fs.openSync(this.filename, 'w');

		commonsOutputDoing('Exporting QUEUED, DONE and FAILED urls');
		
		let tally: number = 0;
		while (true) {
			if (tally % 1000 === 0) commonsOutputProgress(tally);
			
			const row: IUrl|null = await results.next();
			if (row === null) break;
			
			fs.writeSync(
					fd,
					JSON.stringify({
							domain: row.domain,
							url: row.url
					}) + '\n'
			);
			
			if (tally % 1000 === 0) fs.fdatasyncSync(fd);
			tally++;
		}
		
		fs.fdatasyncSync(fd);
		fs.closeSync(fd);

		commonsOutputResult(tally);
	}
}
