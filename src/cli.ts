//const log = require('why-is-node-running');

import { commonsTypeIsObjectArray, commonsTypeIsTArray } from 'tscommons-es-core';
import { TPropertyObject } from 'tscommons-es-core';
import { TCommonsScheduleTime, isTCommonsScheduleTime } from 'tscommons-es-async';
import { CommonsConfig } from 'tscommons-es-config';

import { EStatus } from 'hydra-crawler-ts-assets';

import { CommonsArgs, commonsOutputInfo, commonsOutputSetDaemon, commonsOutputSetDebugging } from 'nodecommons-es-cli';
import { isICommonsCredentials } from 'nodecommons-es-database';

import {
		IInternalHydraCommonApp,
		isIInternalHydraCommonDbApp,
		isIInternalHydraCommonListApp,
		isIInternalHydraCommonExpiryApp,
		isIInternalHydraCommonMaintenanceApp
} from './apps/internal-hydra-common.app';
import { HydraApp } from './apps/hydra.app';
import { DenylistApp } from './apps/denylist.app';
import { CleanupApp } from './apps/cleanup.app';
import { ExpireApp } from './apps/expire.app';
import { ImportApp } from './apps/import.app';
import { ReattemptApp } from './apps/reattempt.app';
import { SeedApp } from './apps/seed.app';
import { StartupApp } from './apps/startup.app';
import { RequeueDomainApp } from './apps/requeue-domain.app';
import { UnarchiveUrlsApp } from './apps/unarchive-urls.app';
import { MoveToArchiveApp } from './apps/move-to-archive.app';
import { PruneArchiveApp } from './apps/prune-archive.app';
import { QueryApp } from './apps/query.app';
import { CrossPopulateExportApp } from './apps/cross-populate-export.app';
import { CrossPopulateImportApp } from './apps/cross-populate-import.app';
import { ExtractTextApp } from './apps/extract-text.app';
import { ExportDomainUrlsApp } from './apps/export-domain-urls';

import { DatabaseService } from './services/database.service';

import { IMatch, isIMatch } from './interfaces/imatch';
import { IExpiry, isIExpiry, toIExpiry } from './interfaces/iexpiry';

import { filenameFromEList, ELISTS } from './enums/elist';

const args: CommonsArgs = new CommonsArgs();
if (args.hasAttribute('debug')) commonsOutputSetDebugging(true);
if (args.hasAttribute('daemon')) commonsOutputSetDaemon(true);

let app: IInternalHydraCommonApp|undefined;

if (args.hasAttribute('denylist')) {
	app = new DenylistApp();
} else if (args.hasAttribute('cleanup')) {
	const includeReset: boolean = args.hasAttribute('reset');
	const includeLinks: boolean = args.hasAttribute('links');
	
	app = new CleanupApp(includeReset, includeLinks);
} else if (args.hasAttribute('expire')) {
	let threshold: Date|undefined;
	
	if (args.hasAttribute('days')) {
		threshold = new Date();
		threshold.setDate(threshold.getDate() - args.getNumber('days'));
	}
	if (args.hasAttribute('date')) {
		threshold = args.getDate('date');
	}
	
	app = new ExpireApp(threshold);
} else if (args.hasAttribute('import')) {
	app = new ImportApp();
} else if (args.hasAttribute('reattempt')) {
	const include: EStatus[] = [];
	if (args.hasAttribute('failed')) include.push(EStatus.FAILED);
	if (args.hasAttribute('dead')) include.push(EStatus.DEAD);
	
	app = new ReattemptApp(include);
} else if (args.hasAttribute('seed')) {
	app = new SeedApp();
} else if (args.hasAttribute('startup')) {
	app = new StartupApp();
} else if (args.hasAttribute('requeue-domain')) {
	if (!args.hasProperty('domain', 'string')) throw new Error('No domain supplied');
	const domain: string = args.getString('domain');
	
	app = new RequeueDomainApp(
			domain,
			args.hasProperty('done-200-only')
	);
} else if (args.hasAttribute('unarchive')) {
	app = new UnarchiveUrlsApp();
} else if (args.hasAttribute('move-to-archive')) {
	app = new MoveToArchiveApp();
} else if (args.hasAttribute('prune-archive')) {
	app = new PruneArchiveApp();
} else if (args.hasAttribute('cross-populate-export')) {
	const filename: string = args.getString('filename');
	
	app = new CrossPopulateExportApp(filename);
} else if (args.hasAttribute('cross-populate-import')) {
	const filename: string = args.getString('filename');
	
	app = new CrossPopulateImportApp(filename);
} else if (args.hasProperty('query', 'string')) {
	const query: string = args.getString('query');
	
	app = new QueryApp(query);
} else if (args.hasAttribute('extract-text')) {
	const url: string = args.getString('url');
	
	app = new ExtractTextApp(url);
} else if (args.hasAttribute('export-domain-urls')) {
	const domain: string = args.getString('domain');
	
	app = new ExportDomainUrlsApp(domain);
} else {
	app = new HydraApp();
}

if (!app) throw new Error('No app created. This should not be possible');

const configAuth: CommonsConfig = app.loadConfigFile('hydra-auth.json');
const credentials: TPropertyObject = configAuth.getObject('database');
if (!isICommonsCredentials(credentials)) throw new Error('Database credentials are not valid');

if (isIInternalHydraCommonDbApp(app)) {
	const databaseService: DatabaseService = new DatabaseService(credentials);
	app.setDatabaseService(databaseService);
}

if (isIInternalHydraCommonListApp(app)) {
	for (const list of ELISTS) {
		const filename: string = filenameFromEList(list);
	
		const entries: unknown = app.loadRawJsonConfigFile(filename);
		if (!commonsTypeIsTArray<IMatch>(entries, isIMatch)) throw new Error(`Invalid JSON list for ${list}`);
		app.addToList(list, entries);
	}
}

if (isIInternalHydraCommonExpiryApp(app)) {
	const data: unknown = app.loadRawJsonConfigFile('list-expiry.json');
	if (!commonsTypeIsObjectArray(data)) throw new Error('Invalid expiry list');
	
	const expiries: IExpiry[] = data
			.map((expiry: { [ key: string ]: unknown }): IExpiry => toIExpiry(expiry));

	if (!commonsTypeIsTArray<IExpiry>(expiries, isIExpiry)) throw new Error('Invalid JSON list for expiry');
	app.addToExpiry(expiries);
}

if (isIInternalHydraCommonMaintenanceApp(app)) {
	const data: unknown = app.loadRawJsonConfigFile('schedule.json');
	if (!commonsTypeIsTArray<TCommonsScheduleTime>(data, isTCommonsScheduleTime)) throw new Error('Invalid schedule list');
	
	app.setMaintenanceSchedule(data);
}

void (async (): Promise<void> => {
	commonsOutputInfo(`Starting application: ${app.getAppName()}`);
	await app.start();
	commonsOutputInfo('Application completed');
	
	setTimeout((): void => {
		//log() // logs out active handles that are keeping node running
		process.exit(0);
	}, 100);
})();
