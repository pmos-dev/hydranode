import { commonsTypeHasPropertyBoolean } from 'tscommons-es-core';

export type TRobotsConfig = {
		ignoreWholeSiteDirectives: boolean;
};

export function isTRobotsConfig(test: unknown): test is TRobotsConfig {
	if (!commonsTypeHasPropertyBoolean(test, 'ignoreWholeSiteDirectives')) return false;
	
	return true;
}
