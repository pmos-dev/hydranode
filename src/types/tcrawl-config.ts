import { commonsTypeHasPropertyBooleanOrUndefined, commonsTypeHasPropertyNumber, commonsTypeHasPropertyStringArray } from 'tscommons-es-core';

export type TCrawlConfig = {
		findNewAvailableDelay: number;
		maxCrawlers: number;
		maxFetchesPerCrawl: number;
		availableStrategyThreshold: number;
		betweenFetchDelay: number;
		randomisation: number;
		connectTimeout: number;
		maxFileSize: number;
		maxFailedTtl: number;
		keepHeaders: string[];
		treatTimeoutDnsAsDead?: boolean;
};

export function isTCrawlConfig(test: unknown): test is TCrawlConfig {
	if (!commonsTypeHasPropertyNumber(test, 'findNewAvailableDelay')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'maxCrawlers')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'maxFetchesPerCrawl')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'availableStrategyThreshold')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'betweenFetchDelay')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'randomisation')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'connectTimeout')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'maxFileSize')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'maxFailedTtl')) return false;

	if (!commonsTypeHasPropertyStringArray(test, 'keepHeaders')) return false;
	for (const header of test.keepHeaders) {
		if (!/^.+:.+$/.test(header as string)) return false;
	}

	if (!commonsTypeHasPropertyBooleanOrUndefined(test, 'treatTimeoutDnsAsDead')) return false;
	
	return true;
}
