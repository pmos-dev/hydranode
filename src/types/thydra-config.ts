import { commonsTypeHasPropertyString } from 'tscommons-es-core';

export type THydraConfig = {
		startUrl: string;
};

export function isTHydraConfig(test: unknown): test is THydraConfig {
	if (!commonsTypeHasPropertyString(test, 'startUrl')) return false;
	
	return true;
}
