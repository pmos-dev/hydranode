export type TQueueLength = {
		queue: number;
		active: number;
		line: number;
};
