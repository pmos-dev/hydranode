import { TKeyObject } from 'tscommons-es-core';

import { CommonsArgs } from 'nodecommons-es-cli';

import { Lists } from '../classes/lists';
import { Expiry } from '../classes/expiry';

import { DatabaseService } from '../services/database.service';

import { IParserConfig } from '../interfaces/iparser-config';

export type TQuery = (
		args: CommonsArgs,
		databaseService: DatabaseService,
		lists: Lists,
		expiry: Expiry,
		parsersConfig: TKeyObject<IParserConfig>
) => Promise<void>;
