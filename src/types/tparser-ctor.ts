import { TKeyObject } from 'tscommons-es-core';

import { IParser } from '../interfaces/iparser';
import { IRequestOutcome } from '../interfaces/irequest-outcome';
import { IParserConfig } from '../interfaces/iparser-config';

export type TParserCtor = {
		// eslint-disable-next-line @typescript-eslint/prefer-function-type
		new(
				url?: string,
				outcome?: IRequestOutcome,
				config?: TKeyObject<IParserConfig>
		): IParser;
};
