export enum EList {
		DENY = 'deny',
		ALLOW = 'allow'
}

export function keyToEList(key: string): EList {
	switch (key) {
		case 'DENY':
			return EList.DENY;
		case 'ALLOW':
			return EList.ALLOW;
	}
	
	throw new Error(`Unable to obtain EList for key: ${key}`);
}

export function filenameFromEList(list: EList): string {
	switch (list) {
		case EList.DENY:
			return 'list-deny.json';
		case EList.ALLOW:
			return 'list-allow.json';
	}
	
	throw new Error('Unknown list entry');
}

export const ELISTS: EList[] = Object.keys(EList)
		.map((key: string): EList => keyToEList(key));
