export enum EXPoweredBy {
		PHP = 'php',
		ASPNET = 'asp.net',
		EXPRESS = 'express',
		ZOPE = 'zope'
}
