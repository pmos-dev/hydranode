export enum EServer {
		APACHE = 'apache',
		IIS = 'iis',
		NGINX = 'nginx',
		CLOUDFLARE = 'cloudflare',
		AMAZONS3 = 'amazons3',
		JETTY = 'jetty'
}
