import { URL } from 'url';

import { commonsArrayUnique, commonsTypeHasPropertyStringArray, TKeyObject } from 'tscommons-es-core';

import { DatabaseService } from '../services/database.service';

import { IParserConfig } from '../interfaces/iparser-config';
import { IRequestOutcome } from '../interfaces/irequest-outcome';

import { HtmlParser } from './html.parser';
import { IDataConfig, isIDataConfig } from './data.parser';

export interface IHyperlinksConfig extends IDataConfig {
		sources: string[];
}
export function isIHyperlinksConfig(test: unknown): test is IHyperlinksConfig {
	if (!isIDataConfig(test)) return false;
	
	if (!commonsTypeHasPropertyStringArray(test, 'sources')) return false;

	return true;
}

export class HyperlinksParser extends HtmlParser<IHyperlinksConfig> {
	
	public static parseTagLinks(
			dom: cheerio.Root,
			tag: string,
			attribute: string
	): string[] {
		const links: string[] = [];

		dom(`${tag}[${attribute}]`).each((_index: number, element: cheerio.Element): boolean => {
			const link: string|undefined = dom(element).attr(attribute);
			if (link !== undefined) links.push(link);
			return true;
		});

		return commonsArrayUnique(links);
	}
	
	public static getBaseHref(dom: cheerio.Root, url: string): string|undefined {
		try {
			const base: cheerio.Cheerio = dom('base[href]');
			if (base.length === 0) return undefined;
		
			const href: string|undefined = dom(base[0]).attr('href');
			if (!href) return undefined;
		
			const whatwg: URL = new URL(href, url);
			return whatwg.toString();
		} catch (ex) { /* do nothing */ }
		return undefined;
	}
	
	private hyperlinksConfig: IHyperlinksConfig|undefined;
	
	constructor(
			private url?: string,
			outcome?: IRequestOutcome,
			config?: TKeyObject<IParserConfig>
	) {
		super(outcome, config, 'hyperlinks');
		
		this.hyperlinksConfig = this.getConfig(isIHyperlinksConfig);
	}

	public async parse(database: DatabaseService): Promise<void> {
		if (!this.dom || !this.url || !this.hyperlinksConfig) return;	// invalid parse
		
		let title: string|undefined;
		try {
			title = this.dom('title').text();
		} catch (ex) { /* do nothing */ }
		
		if (title !== undefined && title !== null && title !== '') {
			await database.setData(this.url, 'title', title);
		}
		
		const links: { [ tag: string ]: string[] } = {};
		for (const source of this.hyperlinksConfig.sources) {
			const pattern: RegExpMatchArray|null = source.match(/^([a-z]+)\[(src|href|action)\]$/);
			if (pattern) links[pattern[1]] = HyperlinksParser.parseTagLinks(this.dom, pattern[1], pattern[2]);
		}
		
		await database.setData(this.url, 'links', links);
	}
	
	// eslint-disable-next-line @typescript-eslint/require-await
	public async links(): Promise<string[]> {
		if (!this.dom || !this.url || !this.hyperlinksConfig) return [];	// invalid parse
		
		let links: string[] = [];
		for (const source of this.hyperlinksConfig.sources) {
			const pattern: RegExpMatchArray|null = source.match(/^([a-z]+)\[(src|href|action)\]$/);
			if (pattern) {
				if (pattern[1] === 'form') continue;
				links = [...links, ...HyperlinksParser.parseTagLinks(this.dom, pattern[1], pattern[2])];
			}
		}

		const unique: string[] = [...new Set(links)];
		
		let baseHref: string|undefined = HyperlinksParser.getBaseHref(this.dom, this.url);
		if (baseHref === undefined) baseHref = this.url;
		
		const results: string[] = [];
		for (let l of unique) {
			const anchor: string[] = l.split('#');
			l = anchor[0];
			if (l === '') continue;

			try {
				const u: URL = new URL(l, baseHref);
				if (u.toString().indexOf('#') !== -1) {
					process.exit(1);
				}
				if (!u.protocol.match(/^http(s?):$/)) continue;
				if (u.toString() === this.url || u.toString() === baseHref) {
					// skip self-links, or presumed self-links
					continue;
				}
				
				results.push(u.toString());
			} catch (ex) { /* do nothing */ }
		}
		
		return results;
	}
}
