import { TKeyObject } from 'tscommons-es-core';

import { DatabaseService } from '../services/database.service';

import { IParserConfig } from '../interfaces/iparser-config';
import { IRequestOutcome } from '../interfaces/irequest-outcome';

import { StringParser } from './string.parser';
import { IDataConfig } from './data.parser';

const ASP_ERROR: string = 'Server Error in \'[^\']+\' Application[.]';
const ASP_ERROR_PARSE: string = 'Server Error in \'([^\']+)\' Application[.].*<h2>\\s*<i>(.+?)</i>\\s*</h2>.+?<code><pre>(.+?)</pre></code>';

export class AspErrorParser extends StringParser<IDataConfig> {
	constructor(
			private url?: string,
			outcome?: IRequestOutcome,
			config?: TKeyObject<IParserConfig>
	) {
		super(outcome, config, 'aspError');
	}

	public async parse(database: DatabaseService): Promise<void> {
		if (!this.url || !this.stringData) return;

		const singlified: string = this.stringData.replace(/[\t\n\r]/g, ' ');
		
		const pattern: RegExp = new RegExp(ASP_ERROR, 'g');

		const result: RegExpExecArray|null = pattern.exec(singlified);
		if (result === null) {
			await database.unsetData(this.url, 'aspErrors');
		} else {
			const detail: RegExp = new RegExp(ASP_ERROR_PARSE, 'g');
			const result2: RegExpExecArray|null = detail.exec(singlified);
			
			if (result2 === null) {
				await database.setData(this.url, 'aspErrors', true);
			} else {
				await database.setData(this.url, 'aspErrors', {
						application: result2[1],
						message: result2[2],
						stack: result2[3]
				});
			}
		}
	}

	public supports(_contentType: string, isAllow: boolean): boolean {
		return isAllow;	// everything allowlisted
	}
}
