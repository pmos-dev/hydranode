import { commonsTypeHasPropertyBooleanOrUndefined, commonsTypeHasPropertyString, commonsTypeHasPropertyT, commonsTypeIsStringArray, TKeyObject } from 'tscommons-es-core';

import { commonsOutputError } from 'nodecommons-es-cli';
import { commonsFileCwdRelativeOrAbsolutePath, commonsFileReadJsonFile } from 'nodecommons-es-file';

import { DatabaseService } from '../services/database.service';

import { IParserConfig } from '../interfaces/iparser-config';
import { IRequestOutcome } from '../interfaces/irequest-outcome';

import { WordsParser, IWordsConfig, isIWordsConfig } from './words.parser';

export interface IDictionaryConfig extends IWordsConfig {
		dictionary: string;
		caseSensitive?: boolean;
}
export function isIDictionaryConfig(test: unknown): test is IDictionaryConfig {
	if (!isIWordsConfig(test)) return false;
	
	if (!commonsTypeHasPropertyString(test, 'dictionary')) return false;
	if (!commonsTypeHasPropertyBooleanOrUndefined(test, 'caseSensitive')) return false;

	return true;
}

const CACHE: Map<string, string[]> = new Map<string, string[]>();

function loadDictionary(config: IDictionaryConfig): string[] {
	const filename: string = commonsFileCwdRelativeOrAbsolutePath(`config/${config.dictionary}`);
	const json: unknown|undefined = commonsFileReadJsonFile(filename);
	if (json === undefined) throw new Error('Unable to read dictionary file');

	if (!commonsTypeIsStringArray(json)) throw new Error('Dictionary file is not a JSON array');
	
	return json;
}

export abstract class DictionaryParser<T extends IDictionaryConfig> extends WordsParser<T> {
	protected caseSensitive: boolean = false;
	private dictionary: string[] = [];
	private cacheId: string|undefined;

	constructor(
			outcome?: IRequestOutcome,
			config?: TKeyObject<IParserConfig>,
			configKey?: string
	) {
		super(outcome, config, configKey);
	
		if (!config || !configKey) return;
		
		this.cacheId = configKey;
		
		if (!commonsTypeHasPropertyT<IDictionaryConfig>(config, configKey, isIDictionaryConfig)) {
			commonsOutputError(`Invalid config for DictionaryParser (${configKey})`);
		}
		const dictionaryConfig: IDictionaryConfig = config[configKey] as IDictionaryConfig;
		
		this.caseSensitive = dictionaryConfig.caseSensitive || false;

		if (!CACHE.has(this.cacheId)) {
			const loaded: string[] = loadDictionary(dictionaryConfig);
			CACHE.set(this.cacheId, loaded);
		}
	}

	protected getDictionary(): string[] {
		if (!this.cacheId) return [];
		
		return CACHE.get(this.cacheId) || [];
	}

	protected abstract parseMatches(database: DatabaseService, matches: string[], nonMatches: string[]): Promise<void>;

	protected async parseWords(database: DatabaseService, words: string[]): Promise<void> {
		this.dictionary = this.getDictionary()
				.map((word: string): string => this.caseSensitive ? word : word.toLowerCase());
		
		const matches: string[] = [];
		const nonMatches: string[] = [];

		for (const word of words) {
			const compare: string = this.caseSensitive ? word : word.toLowerCase();
			if (this.dictionary.includes(compare)) matches.push(word);
			else nonMatches.push(word);
		}
		
		await this.parseMatches(database, matches, nonMatches);
	}
}
