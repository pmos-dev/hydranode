import { commonsTypeHasPropertyBooleanOrUndefined, commonsTypeHasPropertyNumberOrUndefined, TKeyObject } from 'tscommons-es-core';

import { commonsOutputError } from 'nodecommons-es-cli';

import { IRequestOutcome } from '../interfaces/irequest-outcome';
import { IParserConfig, isIParserConfig } from '../interfaces/iparser-config';

import { Parser } from './parser';

// We have a separate maxDataBytes as well as Hydra's own maxFileSize, as some parsers may not support the large data amount Hydra allows, or may tolerate exceeded etc.
export interface IDataConfig extends IParserConfig {
		maxDataBytes?: number;
		allowExceeded?: boolean;
}
export function isIDataConfig(test: unknown): test is IDataConfig {
	if (!isIParserConfig(test)) return false;
	
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'maxDataBytes')) return false;
	if (!commonsTypeHasPropertyBooleanOrUndefined(test, 'allowExceeded')) return false;

	return true;
}

const MAX_DATA_BYTES_HARD_LIMIT: number = 1024 * 1024 * 1024;	// 1Gb

export abstract class DataParser<T extends IDataConfig> extends Parser<T> {
	protected data: Buffer|undefined;
	
	constructor(
			outcome?: IRequestOutcome,
			config?: TKeyObject<IParserConfig>,
			configKey?: string
	) {
		super(outcome, config, configKey);

		if (!outcome || !outcome.data) return;

		const dataConfig: IDataConfig|undefined = this.getConfig<IDataConfig>(isIDataConfig);
		const maxByteSize: number = (dataConfig && dataConfig.maxDataBytes) ? dataConfig.maxDataBytes : MAX_DATA_BYTES_HARD_LIMIT;
		if (outcome.data.byteLength >= maxByteSize) {
			commonsOutputError(`Data to be parsed exceeded parser limit of ${maxByteSize}. Skipping parse.`);
			this.data = undefined;
			return;
		}
		
		if ((!dataConfig || !dataConfig.allowExceeded) && outcome.exceeded) {
			// by default don't parse data which has exceeded Hydra's upper download limit, so will be corrupt. But there is a parser option to force it to do so if desired for some reason

			commonsOutputError('Data to be parsed exceeded Hydra\'s upper download limit and allowExceed it set. Skipping parse.');
			this.data = undefined;
			return;
		}
		
		this.data = outcome.data;
	}
}
