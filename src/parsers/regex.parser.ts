import { DatabaseService } from '../services/database.service';

import { TextParser } from './text.parser';
import { IDataConfig } from './data.parser';

export abstract class RegexParser<T extends IDataConfig> extends TextParser<T> {
	private regexs: Map<string, RegExp> = new Map<string, RegExp>();
	
	protected setRegExs(regexs: Map<string, RegExp>): void {
		for (const key of regexs.keys()) {
			this.regexs.set(key, regexs.get(key)!);
		}
	}
	
	public getRegExs(): Map<string, RegExp> {
		return this.regexs;
	}

	protected abstract parseRegExs(database: DatabaseService, matches: Map<string, RegExpExecArray[]>): Promise<void>;

	protected async parseText(database: DatabaseService, text: string): Promise<void> {
		const matches: Map<string, RegExpExecArray[]> = new Map<string, RegExpExecArray[]>();
		for (const key of this.regexs.keys()) {
			const regex: RegExp = this.regexs.get(key)!;
			
			const results: RegExpExecArray[] = [];
			while (true) {
				const result: RegExpExecArray|null = regex.exec(text);
				if (!result || result === null) break;
				
				results.push(result);
			}
			
			if (results.length > 0) matches.set(key, results);
		}

		await this.parseRegExs(database, matches);
	}
}
