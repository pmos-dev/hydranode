import { commonsStringSplitWords, commonsTypeHasPropertyNumberOrUndefined } from 'tscommons-es-core';

import { DatabaseService } from '../services/database.service';

import { HtmlParser } from './html.parser';
import { IDataConfig, isIDataConfig } from './data.parser';

export interface IParagraphsConfig extends IDataConfig {
		paragraphWordsThreshold?: number;
}
export function isIParagraphsConfig(test: unknown): test is IParagraphsConfig {
	if (!isIDataConfig(test)) return false;
	
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'paragraphWordsThreshold')) return false;

	return true;
}

export abstract class ParagraphsParser<T extends IParagraphsConfig> extends HtmlParser<T> {
	public static parseParagraphs(
			dom: cheerio.Root,
			paragraphsConfig: IParagraphsConfig
	): string[] {
		const nodes: cheerio.Cheerio = dom('p,dd')
				.contents()
				// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
				.filter((_index: number, element: cheerio.Element): boolean => (element as any).nodeType === 3);

		const paragraphs: string[] = [];

		dom(nodes).each(
				(_index: number, element: cheerio.Element): void => {
					const paragraph: string = dom(element).text();
			
					if (paragraphsConfig.paragraphWordsThreshold && commonsStringSplitWords(paragraph).length >= paragraphsConfig.paragraphWordsThreshold) {
						paragraphs.push(paragraph);
					}
				}
		);

		return paragraphs
				.map((p: string): string => p.trim())
				.filter((p: string): boolean => p !== '');
	}

	protected abstract parseParagraphs(database: DatabaseService, paragraphs: string[]): Promise<void>;

	public async parse(database: DatabaseService): Promise<void> {
		if (!this.dom) return;	// invalid parse
		
		const paragraphsConfig: IParagraphsConfig|undefined = this.getConfig(isIParagraphsConfig);
		if (!paragraphsConfig) return;
		
		const paragraphs: string[] = ParagraphsParser.parseParagraphs(
				this.dom,
				paragraphsConfig
		);
		
		await this.parseParagraphs(database, paragraphs);
	}
}
