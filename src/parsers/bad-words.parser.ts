import { TKeyObject } from 'tscommons-es-core';

import { DatabaseService } from '../services/database.service';

import { IParserConfig } from '../interfaces/iparser-config';
import { IRequestOutcome } from '../interfaces/irequest-outcome';

import { DictionaryParser, IDictionaryConfig } from './dictionary.parser';

export class BadWordsParser extends DictionaryParser<IDictionaryConfig> {
	constructor(
			private url?: string,
			outcome?: IRequestOutcome,
			config?: TKeyObject<IParserConfig>
	) {
		super(outcome, config, 'badWords');
	}

	protected async parseMatches(database: DatabaseService, matches: string[], _nonMatches: string[]): Promise<void> {
		if (!this.url) return;
		
		if (matches.length > 0) await database.setData(this.url, 'badWords', matches);
		else await database.unsetData(this.url, 'badWords');
	}
}
