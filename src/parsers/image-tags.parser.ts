import { TKeyObject } from 'tscommons-es-core';

import { commonsOutputDebug } from 'nodecommons-es-cli';

import { DatabaseService } from '../services/database.service';

import { IParserConfig } from '../interfaces/iparser-config';
import { IRequestOutcome } from '../interfaces/irequest-outcome';

import { HtmlParser } from './html.parser';
import { IDataConfig } from './data.parser';

type TImageTag = {
		src: string;
		alt?: string;
		title?: string;
};

export class ImageTagsParser extends HtmlParser<IDataConfig> {
	public static parseImageTags(
			dom: cheerio.Root
	): TImageTag[] {
		const images: TImageTag[] = [];

		dom('img').each((_index: number, element: cheerio.Element): boolean => {
			const src: string|null|undefined = dom(element).attr('src');
			if (src === null || src === undefined) return true;
			
			const alt: string|null|undefined = dom(element).attr('alt');
			const title: string|null|undefined = dom(element).attr('title');

			const image: TImageTag = {
					src: src
			};
			if (alt !== undefined && alt !== null) image.alt = alt;
			if (title !== undefined && title !== null) image.title = title;

			images.push(image);

			return true;
		});

		return images;
	}

	constructor(
			private url?: string,
			outcome?: IRequestOutcome,
			config?: TKeyObject<IParserConfig>
	) {
		super(outcome, config, 'imageTags');
	}

	public async parse(database: DatabaseService): Promise<void> {
		if (!this.dom || !this.url) return;	// invalid parse
			
		const images: TImageTag[] = ImageTagsParser.parseImageTags(this.dom);
		if (images.length > 0) {
			commonsOutputDebug(`Detected images for ${this.url}`);
			await database.setData(this.url, 'images', images);
		} else {
			await database.unsetData(this.url, 'images');
		}
	}
}
