import extractor from 'unfluff';

import { DatabaseService } from '../services/database.service';

import { HtmlParser } from './html.parser';
import { IDataConfig } from './data.parser';

export abstract class TextParser<T extends IDataConfig> extends HtmlParser<T> {
	public static attemptExtract(html: string): string {
		try {
			// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-call
			const extracted: { text: string } = extractor(html);
			if (!extracted) throw new Error('Unable to run unfluff');
			
			return extracted.text
					.split(/\n+/)
					.map((s: string): string => s.trim())
					.filter((s: string): boolean => s !== '')
					.join('\n');
		} catch (ex) {
			return '';
		}
	}
	
	protected abstract parseText(database: DatabaseService, text: string): Promise<void>;
	
	public async parse(database: DatabaseService): Promise<void> {
		if (!this.dom) {
			await this.parseText(database, '');
			return;
		}
		
		const html: string = this.dom.html();
		const extracted: string = TextParser.attemptExtract(html);

		await this.parseText(database, extracted);
	}
}
