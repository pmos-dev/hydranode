import { CommonsFixedDuration, commonsTypeHasPropertyNumberOrUndefined, commonsTypeHasPropertyString, commonsTypeHasPropertyT, TKeyObject } from 'tscommons-es-core';

import { commonsOutputDebug, commonsOutputError } from 'nodecommons-es-cli';

import { RigQueueLengthService } from '../services/rig-queue-length.service';

import { IParserConfig } from '../interfaces/iparser-config';
import { IRequestOutcome } from '../interfaces/irequest-outcome';

import { TQueueLength } from '../types/tqueue-length';

import { TextParser } from './text.parser';
import { IDataConfig, isIDataConfig } from './data.parser';

export interface IOllamaConfig extends IDataConfig {
		url: string;
		key: string;
		keepAlive?: number;
}
export function isIOllamaConfig(test: unknown): test is IOllamaConfig {
	if (!isIDataConfig(test)) return false;

	if (!commonsTypeHasPropertyString(test, 'url')) return false;
	if (!commonsTypeHasPropertyString(test, 'key')) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'keepAlive')) return false;

	return true;
}

export abstract class OllamaParser<T extends IOllamaConfig> extends TextParser<T> {
	protected readonly ollamaUrl: string|undefined;
	protected readonly ollamaKey: string|undefined;
	protected readonly ollamaKeepAlive: CommonsFixedDuration|undefined;

	constructor(
			outcome?: IRequestOutcome,
			config?: TKeyObject<IParserConfig>,
			configKey?: string
	) {
		super(
				outcome,
				config,
				configKey
		);

		if (!config) return;
		
		if (!commonsTypeHasPropertyT<IOllamaConfig>(config, 'ollama', isIOllamaConfig)) {
			commonsOutputError('Invalid config for OllamaParser');
		}
		const ollamaConfig: IOllamaConfig = config['ollama'] as IOllamaConfig;

		this.ollamaUrl = ollamaConfig.url;
		this.ollamaKey = ollamaConfig.key;
		
		if (ollamaConfig.keepAlive) {
			this.ollamaKeepAlive = CommonsFixedDuration.fromMillis(ollamaConfig.keepAlive);
		}
	}

	public async debugGetLength(): Promise<void> {
		if (!this.ollamaUrl || !this.ollamaKey) return;

		const rigService: RigQueueLengthService = new RigQueueLengthService(
				this.ollamaUrl,
				this.ollamaKey
		);

		const length: TQueueLength = await rigService.getLength();
		commonsOutputDebug(`Rig queue length is: ${length.active} active, ${length.queue} queued: ${length.line} total line length.`);
	}
}
