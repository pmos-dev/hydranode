import * as cheerio from 'cheerio';

import { TKeyObject } from 'tscommons-es-core';

import { IRequestOutcome } from '../interfaces/irequest-outcome';
import { IParserConfig } from '../interfaces/iparser-config';

import { StringParser } from './string.parser';
import { IDataConfig } from './data.parser';

export abstract class HtmlParser<T extends IDataConfig> extends StringParser<T> {
	public static collapseWhitespace(raw: string): string {
		return raw.replace(/\s+/g, ' ');
	}
	
	public static loadDom(html: string): cheerio.Root|undefined {
		try {
			return cheerio.load(html);
		} catch (ex) {
			// ignore
			return undefined;
		}
	}

	protected dom: cheerio.Root|undefined;
	
	constructor(
			outcome?: IRequestOutcome,
			config?: TKeyObject<IParserConfig>,
			configKey?: string
	) {
		super(outcome, config, configKey);

		if (this.stringData && this.stringData.length) {
			const collapsed: string = HtmlParser.collapseWhitespace(this.stringData);
			
			this.dom = HtmlParser.loadDom(collapsed);
		}
	}
	
	public supports(contentType: string, isAllow: boolean): boolean {
		if (!isAllow) return false;
		return /^text\/html([^a-z0-9].*)?$/.test(contentType);
	}
}
