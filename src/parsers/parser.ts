import { commonsTypeHasPropertyT, TKeyObject } from 'tscommons-es-core';

import { commonsOutputError } from 'nodecommons-es-cli';

import { DatabaseService } from '../services/database.service';

import { IParser } from '../interfaces/iparser';
import { IRequestOutcome } from '../interfaces/irequest-outcome';
import { IParserConfig, isIParserConfig } from '../interfaces/iparser-config';

export abstract class Parser<T extends IParserConfig = IParserConfig> implements IParser {
	private enabled: boolean = false;
	private parserConfig: IParserConfig|undefined;
	
	// these all have to be optional because of the Ctor approach used elsewhere
	// similarly, we don't check types of the config (T) other than the enabled in TParserConfig
	constructor(
			protected outcome?: IRequestOutcome,
			config?: TKeyObject<IParserConfig>,
			private configKey?: string
	) {
		if (!config || !this.configKey) return;

		if (!commonsTypeHasPropertyT<IParserConfig>(config, this.configKey, isIParserConfig)) {
			commonsOutputError(`Invalid config for Parser (${this.configKey})`);
		}

		this.parserConfig = config[this.configKey] as T;
		this.enabled = this.parserConfig.enabled;
	}
	
	protected getConfig<P>(	// this allows subclasses to get their config without having to have all the T attributes of their own children
			checker: (test: unknown) => test is P
	): P|undefined {
		if (!this.parserConfig) return undefined;
		
		if (!checker(this.parserConfig)) throw new Error(`Invalid parser config for ${this.configKey!}`);

		return this.parserConfig as P;
	}

	public isEnabled(): boolean {
		return this.enabled;
	}

	public abstract supports(contentType: string, isAllow: boolean): boolean;

	public async init(_database: DatabaseService): Promise<void> {
		// subclasses can implement
	}

	public async parse(_database: DatabaseService): Promise<void> {
		// subclasses can implement
	}
	
	// eslint-disable-next-line @typescript-eslint/require-await
	public async links(): Promise<string[]> {
		// subclasses can implement

		return [];
	}
}
