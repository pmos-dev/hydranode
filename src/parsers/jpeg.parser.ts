import * as jpeg from 'jpeg-js';

import { TKeyObject } from 'tscommons-es-core';

import { DatabaseService } from '../services/database.service';

import { IRequestOutcome } from '../interfaces/irequest-outcome';
import { IParserConfig } from '../interfaces/iparser-config';

import { DataParser, IDataConfig } from './data.parser';

export class JpegParser extends DataParser<IDataConfig> {
	constructor(
			private url?: string,
			outcome?: IRequestOutcome,
			config?: TKeyObject<IParserConfig>
	) {
		super(outcome, config, 'jpeg');
	}
	
	public supports(contentType: string, isAllow: boolean): boolean {
		if (!isAllow) return false;
		return contentType.match(/^image\/(jpeg|jpg)$/) ? true : false;
	}
	
	public async parse(database: DatabaseService): Promise<void> {
		if (!this.outcome || !this.url) return;
		
		if (!this.outcome.data) return;
		if (this.outcome.exceeded) return;	// don't try and parse vast JPEGs than haven't been downloaded properly, regardless of what the IDataConfig.allowExceeded is set to
		
		try {
			const rawImageData: jpeg.BufferRet = jpeg.decode(this.outcome.data);

			await database.setData(this.url, 'width', rawImageData.width);
			await database.setData(this.url, 'height', rawImageData.height);
		} catch (ex) { /* do nothing */ }
	}
}
