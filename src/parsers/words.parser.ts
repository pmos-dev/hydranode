import { commonsTypeHasPropertyBooleanOrUndefined } from 'tscommons-es-core';

import { DatabaseService } from '../services/database.service';

import { TextParser } from './text.parser';
import { IDataConfig, isIDataConfig } from './data.parser';

const WORD_WITH_HYPHEN: string = '(?<![a-z0-9])[a-z0-9](?:[-\']?[a-z0-9])*(?![a-z0-9])';
const WORD_WITHOUT_HYPHEN: string = '(?<![a-z0-9])[a-z0-9](?:[\']?[a-z0-9])*(?![a-z0-9])';

export interface IWordsConfig extends IDataConfig {
		allowHyphenatedWords?: boolean;
}
export function isIWordsConfig(test: unknown): test is IWordsConfig {
	if (!isIDataConfig(test)) return false;
	
	if (!commonsTypeHasPropertyBooleanOrUndefined(test, 'allowHyphenatedWords')) return false;

	return true;
}

export abstract class WordsParser<T extends IWordsConfig> extends TextParser<T> {
	protected abstract parseWords(database: DatabaseService, words: string[]): Promise<void>;

	protected async parseText(database: DatabaseService, text: string): Promise<void> {
		const wordsConfig: IWordsConfig|undefined = this.getConfig(isIWordsConfig);
		if (!wordsConfig) return;
		
		const pattern: RegExp = wordsConfig.allowHyphenatedWords ? new RegExp(WORD_WITH_HYPHEN, 'ig') : new RegExp(WORD_WITHOUT_HYPHEN, 'ig');
		
		const words: string[] = [];
		while (true) {
			const result: RegExpExecArray|null = pattern.exec(text);
			if (result === null) break;

			words.push(result[0]);
		}

		await this.parseWords(database, words);
	}
}
