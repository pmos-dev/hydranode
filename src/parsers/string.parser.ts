import { TKeyObject } from 'tscommons-es-core';

import { UtfDecoder } from '../helpers/utf-decoder';

import { IRequestOutcome } from '../interfaces/irequest-outcome';
import { IParserConfig } from '../interfaces/iparser-config';

import { DataParser, IDataConfig } from './data.parser';

export abstract class StringParser<T extends IDataConfig> extends DataParser<T> {
	protected stringData: string|undefined;
	
	constructor(
			outcome?: IRequestOutcome,
			config?: TKeyObject<IParserConfig>,
			configKey?: string
	) {
		super(outcome, config, configKey);
		
		if (!this.data) return;
		
		try {
			this.stringData = UtfDecoder.fromBuffer(this.data);
		} catch (ex) {
			// ignore
		}
	}
}
