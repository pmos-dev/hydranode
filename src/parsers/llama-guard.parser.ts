import { TKeyObject, commonsTypeHasPropertyString, commonsTypeHasPropertyStringArray, commonsTypeHasPropertyT } from 'tscommons-es-core';

import { commonsOutputDebug, commonsOutputError } from 'nodecommons-es-cli';

import { DatabaseService } from '../services/database.service';
import { ELlamaGuardClassification, RigLlamaGuardService, TLlamaGuardResult } from '../services/rig-llama-guard.service';

import { IParserConfig } from '../interfaces/iparser-config';
import { IRequestOutcome } from '../interfaces/irequest-outcome';

import { IOllamaConfig, OllamaParser } from './ollama.parser';
import { IDataConfig, isIDataConfig } from './data.parser';

export interface ILlamaGuardConfig extends IDataConfig {
		model: string;
		permittedDomains: string[];
}
export function isILlamaGuardConfig(test: unknown): test is ILlamaGuardConfig {
	if (!isIDataConfig(test)) return false;

	if (!commonsTypeHasPropertyString(test, 'model')) return false;
	if (!commonsTypeHasPropertyStringArray(test, 'permittedDomains')) return false;

	return true;
}

export class LlamaGuardParser extends OllamaParser<IOllamaConfig> {
	private model: string|undefined;
	private permittedDomains: string[] = [];

	constructor(
			private url?: string,
			outcome?: IRequestOutcome,
			config?: TKeyObject<IParserConfig>
	) {
		super(
				outcome,
				config,
				'llamaguard'
		);

		if (!config) return;
		
		if (!commonsTypeHasPropertyT<ILlamaGuardConfig>(config, 'llamaguard', isILlamaGuardConfig)) {
			commonsOutputError('Invalid config for LlamaGuardParser');
		}
		this.model = (config['llamaguard'] as ILlamaGuardConfig).model;
		this.permittedDomains = (config['llamaguard'] as ILlamaGuardConfig).permittedDomains;
	}

	protected async parseText(database: DatabaseService, text: string): Promise<void> {
		if (!this.url) return;
		if (!this.ollamaUrl || !this.ollamaKey || !this.model) return;

		const hostname: string = new URL(this.url).hostname;
		if (!this.permittedDomains.includes(hostname)) return;
		
		if (text.length === 0) {
			await database.unsetData(this.url, 'llamaguard');
			return;
		}

		const rigService: RigLlamaGuardService = new RigLlamaGuardService(
				this.ollamaUrl,
				this.ollamaKey,
				this.model
		);

		await this.debugGetLength();

		// run this outside of await so that it doesn't block other things
		void (async (): Promise<void> => {
			if (!this.url) return;

			const result: TLlamaGuardResult|undefined = await rigService.generate(
					text,
					this.ollamaKeepAlive
			);
			if (result === undefined) return;

			commonsOutputDebug(`LlamaGuard result is: ${result.classification.toString()}${result.classification === ELlamaGuardClassification.UNSAFE ? ` (${result.categories.join(',')})` : ''}`);
			
			await database.setData(this.url, 'llamaguard', result);
		})();
	}
}
