import { commonsStringAutomatedReadabilityIndex, commonsStringFleschKincaidReadingEase, commonsStringSplitSentences, commonsStringSplitWords, TKeyObject } from 'tscommons-es-core';

import { commonsOutputDebug } from 'nodecommons-es-cli';

import { DatabaseService } from '../services/database.service';

import { IParserConfig } from '../interfaces/iparser-config';
import { IRequestOutcome } from '../interfaces/irequest-outcome';

import { TextParser } from './text.parser';
import { IDataConfig } from './data.parser';

type TAccessibilityMetrics = {
		paragraphs: number;
		sentences: number;
		words: number;
		fkre: number;
		ari: number;
};

export class AccessibilityMetricsParser extends TextParser<IDataConfig> {
	constructor(
			private url?: string,
			outcome?: IRequestOutcome,
			config?: TKeyObject<IParserConfig>
	) {
		super(
				outcome,
				config,
				'accessibilityMetrics'
		);
	}
	
	protected async parseText(database: DatabaseService, text: string): Promise<void> {
		if (!this.url) return;
		
		if (text.length === 0) {
			await database.unsetData(this.url, 'accessibilityMetrics');
			return;
		}
		
		const paragraphs: string[] = text.split('\n');
		const all: string = paragraphs.join('. ');
		
		const metrics: TAccessibilityMetrics = {
				paragraphs: paragraphs.length,
				sentences: commonsStringSplitSentences(all).length,
				words: commonsStringSplitWords(all).length,
				fkre: commonsStringFleschKincaidReadingEase(all),
				ari: commonsStringAutomatedReadabilityIndex(all)
		};
		
		commonsOutputDebug(`Accessibility metrics are: ${JSON.stringify(metrics)}`);
		await database.setData(this.url, 'accessibilityMetrics', metrics);
	}
}
