import { TKeyObject } from 'tscommons-es-core';

import { TPhpError } from 'hydra-crawler-ts-assets';
import { EPhpErrorType, deriveEPhpErrorType } from 'hydra-crawler-ts-assets';

import { DatabaseService } from '../services/database.service';

import { IParserConfig } from '../interfaces/iparser-config';
import { IRequestOutcome } from '../interfaces/irequest-outcome';

import { StringParser } from './string.parser';
import { IDataConfig } from './data.parser';

const PHP_ERROR: string = '<br />\\s{1,2}<b>([A-Za-z ]+?)</b>:  ([^<]+?) in <b>([^<]+?)</b> on line <b>([0-9]+?)</b><br />';

export class PhpErrorParser extends StringParser<IDataConfig> {
	constructor(
			private url?: string,
			outcome?: IRequestOutcome,
			config?: TKeyObject<IParserConfig>
	) {
		super(outcome, config, 'phpError');
	}

	public async parse(database: DatabaseService): Promise<void> {
		if (!this.stringData || !this.url) return;
	
		const pattern: RegExp = new RegExp(PHP_ERROR, 'g');

		const errors: TPhpError[] = [];
		while (true) {
			const result: RegExpExecArray|null = pattern.exec(this.stringData);
			if (result === null) break;

			const type: EPhpErrorType|undefined = deriveEPhpErrorType(result[1]);
			if (type === undefined) continue;
			
			const message: string = result[2];
			const file: string = result[3];
			const line: number = parseInt(result[4], 10);

			const error: TPhpError = {
					type: type,
					message: message,
					file: file,
					line: line
			};
			errors.push(error);
		}
		
		if (errors.length > 0) await database.setData(this.url, 'phpErrors', errors);
		else await database.unsetData(this.url, 'phpErrors');
	}

	public supports(_contentType: string, isAllow: boolean): boolean {
		return isAllow;	// everything allowlisted
	}
}
