import { TKeyObject, commonsTypeHasPropertyString, commonsTypeHasPropertyStringArray, commonsTypeHasPropertyT } from 'tscommons-es-core';

import { commonsOutputDebug, commonsOutputError } from 'nodecommons-es-cli';

import { DatabaseService } from '../services/database.service';
import { RigSubjectivityScaleService, TSubjectivityResult } from '../services/rig-subjectivity-scale.service';

import { IParserConfig } from '../interfaces/iparser-config';
import { IRequestOutcome } from '../interfaces/irequest-outcome';

import { IOllamaConfig, OllamaParser } from './ollama.parser';
import { IDataConfig, isIDataConfig } from './data.parser';

export interface IOffenceConfig extends IDataConfig {
		model: string;
		permittedDomains: string[];
}
export function isIOffenceConfig(test: unknown): test is IOffenceConfig {
	if (!isIDataConfig(test)) return false;

	if (!commonsTypeHasPropertyString(test, 'model')) return false;
	if (!commonsTypeHasPropertyStringArray(test, 'permittedDomains')) return false;

	return true;
}

export class OffenceParser extends OllamaParser<IOllamaConfig> {
	private model: string|undefined;
	private permittedDomains: string[] = [];

	constructor(
			private url?: string,
			outcome?: IRequestOutcome,
			config?: TKeyObject<IParserConfig>
	) {
		super(
				outcome,
				config,
				'offence'
		);

		if (!config) return;
		
		if (!commonsTypeHasPropertyT<IOffenceConfig>(config, 'offence', isIOffenceConfig)) {
			commonsOutputError('Invalid config for OffenceParser');
		}
		this.model = (config['offence'] as IOffenceConfig).model;
		this.permittedDomains = (config['offence'] as IOffenceConfig).permittedDomains;
	}

	protected async parseText(database: DatabaseService, text: string): Promise<void> {
		if (!this.url) return;
		if (!this.ollamaUrl || !this.ollamaKey || !this.model) return;

		const hostname: string = new URL(this.url).hostname;
		if (!this.permittedDomains.includes(hostname)) return;
		
		if (text.length === 0) {
			await database.unsetData(this.url, 'offence');
			return;
		}
		
		const rigService: RigSubjectivityScaleService = new RigSubjectivityScaleService(
				this.ollamaUrl,
				this.ollamaKey,
				this.model
		);

		await this.debugGetLength();

		// run this outside of await so that it doesn't block other things
		void (async (): Promise<void> => {
			if (!this.url) return;

			const score: TSubjectivityResult|undefined = await rigService.generateOffenceScore(
					text,
					this.ollamaKeepAlive
			);
			if (score === undefined) return;

			if (score.score < 1) delete score.reason;

			commonsOutputDebug(`Offence score is: ${score.score.toString(10)}`);
			if (score.reason) commonsOutputDebug(`Reason for score is: ${score.reason}`);

			await database.setData(this.url, 'offence', score);
		})();
	}
}
