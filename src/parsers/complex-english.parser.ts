import { commonsTypeHasPropertyT, commonsTypeIsStringArrayKeyObject, TKeyObject } from 'tscommons-es-core';

import { commonsOutputDebug, commonsOutputError } from 'nodecommons-es-cli';
import { commonsFileCwdRelativeOrAbsolutePath, commonsFileReadJsonFile } from 'nodecommons-es-file';

import { DatabaseService } from '../services/database.service';

import { IParserConfig } from '../interfaces/iparser-config';
import { IRequestOutcome } from '../interfaces/irequest-outcome';

import { IDictionaryConfig, isIDictionaryConfig } from './dictionary.parser';
import { PhraseParser } from './phrase.parser';

let CACHE: TKeyObject<string[]>|undefined;

export class ComplexEnglishParser extends PhraseParser<IDictionaryConfig> {
	public static loadKeyObject(config: IDictionaryConfig): TKeyObject<string[]> {
		const filename: string = commonsFileCwdRelativeOrAbsolutePath(`config/${config.dictionary}`);

		const json: unknown|undefined = commonsFileReadJsonFile(filename);
		if (json === undefined) throw new Error('Unable to read dictionary file');
	
		if (!commonsTypeIsStringArrayKeyObject(json)) throw new Error('Dictionary file is not a JSON array');
		
		return json;
	}

	public static loadDictionary(config: IDictionaryConfig): string[] {
		return Object.keys(ComplexEnglishParser.loadKeyObject(config));
	}
	
	private dictionary: TKeyObject<string[]>|undefined;
	
	constructor(
			private url?: string,
			outcome?: IRequestOutcome,
			config?: TKeyObject<IParserConfig>
	) {
		super(outcome, config, 'complexEnglish');
		
		if (!config) return;

		if (!commonsTypeHasPropertyT<IDictionaryConfig>(config, 'complexEnglish', isIDictionaryConfig)) {
			commonsOutputError('Invalid config for ComplexEnglishParser');
		}
		const dictionaryConfig: IDictionaryConfig = config['complexEnglish'] as IDictionaryConfig;

		if (!CACHE) CACHE = ComplexEnglishParser.loadKeyObject(dictionaryConfig);
		this.dictionary = CACHE;

		const keys: string[] = Object.keys(this.dictionary);
		this.setPhrases(keys, dictionaryConfig.caseSensitive || false);
	}
	
	public getDictionary(): TKeyObject<string[]>|undefined {
		return this.dictionary;
	}

	protected async parsePhrases(database: DatabaseService, matches: string[]): Promise<void> {
		if (!this.url) return;
		
		if (matches.length > 0) {
			commonsOutputDebug(`Found ${matches.length} potentially complex phrases`);
			await database.setData(this.url, 'complexEnglish', matches);
		} else {
			await database.unsetData(this.url, 'complexEnglish');
		}
	}
}
