import { DatabaseService } from '../services/database.service';

import { RegexParser } from './regex.parser';
import { IDictionaryConfig } from './dictionary.parser';

export abstract class PhraseParser<T extends IDictionaryConfig> extends RegexParser<T> {
	public setPhrases(
			phrases: string[],
			caseInsensitive: boolean
	): void {
		const map: Map<string, RegExp> = new Map<string, RegExp>();
		for (const phrase of phrases) {
			const regex: RegExp = new RegExp(`(^|[^0-9A-Za-z])${phrase}($|[^0-9A-Za-z])`, `g${caseInsensitive ? 'i' : ''}`);
			map.set(phrase, regex);
		}
		
		this.setRegExs(map);
	}
	
	protected abstract parsePhrases(database: DatabaseService, phrases: string[]): Promise<void>;

	protected async parseRegExs(database: DatabaseService, matches: Map<string, RegExpExecArray[]>): Promise<void> {
		await this.parsePhrases(database, Array.from(matches.keys()));
	}
}
