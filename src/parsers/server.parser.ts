import { TKeyObject } from 'tscommons-es-core';

import { DatabaseService } from '../services/database.service';

import { IRequestOutcome } from '../interfaces/irequest-outcome';
import { IServer } from '../interfaces/iserver';
import { IParserConfig } from '../interfaces/iparser-config';

import { EServer } from '../enums/eserver';
import { EXPoweredBy } from '../enums/ex-powered-by';

import { Parser } from './parser';

export class ServerParser extends Parser<IParserConfig> {
	constructor(
			private url?: string,
			outcome?: IRequestOutcome,
			config?: TKeyObject<IParserConfig>
	) {
		super(outcome, config, 'server');
	}
	
	public supports(_contentType: string): boolean {
		return true;	// everything
	}
	
	public async parse(database: DatabaseService): Promise<void> {
		if (!this.outcome || !this.url) return;
		
		const server: string|undefined = (this.outcome.headers['server'] as string) || undefined;
		const xPoweredBy: string|undefined = (this.outcome.headers['x-powered-by'] as string) || undefined;

		if (server === undefined && xPoweredBy === undefined) {
			await database.unsetData(this.url, 'server');
			return;
		}
		
		const data: IServer = {};
		
		if (server) {
			if (/^Apache($|\/)/.test(server)) data.server = EServer.APACHE;
			if (/^Microsoft-IIS($|\/)/.test(server)) data.server = EServer.IIS;
			if (/^nginx($|\/)/.test(server)) data.server = EServer.NGINX;
			if (/^cloudflare($|\/)/.test(server)) data.server = EServer.CLOUDFLARE;
			if (/^AmazonS3($|\/)/.test(server)) data.server = EServer.AMAZONS3;
			if (/^Jetty($|\/)/.test(server)) data.server = EServer.JETTY;
		}

		if (xPoweredBy) {
			if (/^PHP($|\/)/.test(xPoweredBy)) data.xPoweredBy = EXPoweredBy.PHP;
			if (/^ASP.NET($|\/)/.test(xPoweredBy)) data.xPoweredBy = EXPoweredBy.ASPNET;
			if (/^Express($|\/)/.test(xPoweredBy)) data.xPoweredBy = EXPoweredBy.EXPRESS;
			if (/^Zope($|\/)/.test(xPoweredBy)) data.xPoweredBy = EXPoweredBy.ZOPE;
		}

		if (data.server === undefined && data.xPoweredBy === undefined) {
			await database.unsetData(this.url, 'server');
			return;
		}
	
		try {
			await database.setData(this.url, 'server', data);
		} catch (ex) { /* do nothing */ }
	}
}
