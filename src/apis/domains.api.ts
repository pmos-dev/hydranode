import * as express from 'express';

import { commonsTreeStringSegmentArrayToTree, commonsTypeEncode, TEncoded, TSegmentTree } from 'tscommons-es-core';

import { TDomain } from 'hydra-crawler-ts-assets';

import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';
import { CommonsRestApi, commonsRestApiBadRequest, commonsRestApiError } from 'nodecommons-es-rest';

import { RestServer } from '../servers/rest.server';

import { DatabaseService } from '../services/database.service';

export class DomainsApi extends CommonsRestApi<ICommonsRequestWithStrictParams, express.Response> {
	constructor(
			restServer: RestServer,
			private databaseService: DatabaseService,
			path: string
	) {
		super(restServer);

		super.getHandler(
				`${path}rest/domains`,
				async (_req: express.Request, _res: express.Response): Promise<TEncoded> => {
					const domains: TDomain[] = await this.databaseService.listDomains();

					return commonsTypeEncode(domains);
				}
		);

		super.getHandler(
				`${path}rest/domains/:domain[idname]/tree`,
				async (req: ICommonsRequestWithStrictParams, _res: express.Response): Promise<TEncoded> => {
					if (!/^[-a-z0-9]+(\.[-a-z0-9]+)*$/i.test(req.strictParams.domain as string)) return commonsRestApiBadRequest('Invalid domain');

					const urls: string[] = await this.databaseService.listDone200DomainUrls(req.params.domain);
				
					const splits: string[][] = [];
					for (const url of urls) {
						const regex: RegExp = new RegExp(`^http(?:s?)://${req.params.domain}(?::[0-9]+)?(/.*)$`);
						const match: RegExpExecArray|null = regex.exec(url);
						
						if (match === null) return commonsRestApiError(`Unable to match for ${url}`);
				
						let urlPath: string = match[1];
						if (urlPath.indexOf('?') !== -1) urlPath = urlPath.split('?')[0];
						
						const segments: string[] = urlPath.split('/');
						segments.pop();	// remove the tail
						
						splits.push(segments);
					}

					const tree: TSegmentTree = commonsTreeStringSegmentArrayToTree(splits);
					return commonsTypeEncode(tree);
				}
		);
	}
}
