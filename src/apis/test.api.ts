import * as express from 'express';

import { CommonsRestApi } from 'nodecommons-es-rest';
import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';

import { RestServer } from '../servers/rest.server';

export class TestApi extends CommonsRestApi<ICommonsRequestWithStrictParams, express.Response> {
	constructor(
			restServer: RestServer,
			path: string
	) {
		super(restServer);
		
		super.getHandler(
				`${path}test/hello`,
				// eslint-disable-next-line @typescript-eslint/require-await
				async (_: unknown): Promise<string> => 'Hello world'
		);
	}
}
