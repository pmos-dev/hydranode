import * as express from 'express';

import { TDomain } from 'hydra-crawler-ts-assets';

import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';
import { CommonsRestApi } from 'nodecommons-es-rest';

import { RestServer } from '../servers/rest.server';

import { DatabaseService } from '../services/database.service';

export class AutocompleteApi extends CommonsRestApi<ICommonsRequestWithStrictParams, express.Response> {
	constructor(
			restServer: RestServer,
			private databaseService: DatabaseService,
			path: string
	) {
		super(restServer);

		super.getHandler(
				`${path}rest/autocomplete/domain/:term[idname]`,
				async (req: ICommonsRequestWithStrictParams, _res: express.Response): Promise<string[]> => {
					const domains: TDomain[] = await this.databaseService.listDomainsByLike(req.strictParams.term as string);

					return domains
							.map((domain: TDomain): string => domain.domain);
				}
		);
	}
}
