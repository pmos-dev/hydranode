import * as express from 'express';
import Cache from 'memcached-promisify';

import { commonsTypeHasPropertyEnum, commonsTypeHasPropertyNumber, commonsTypeIsTArray } from 'tscommons-es-core';

import { EStatus, isEStatus } from 'hydra-crawler-ts-assets';

import { CommonsRestApi } from 'nodecommons-es-rest';
import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';

import { RestServer } from '../servers/rest.server';

import { DatabaseService } from '../services/database.service';

// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-var-requires

type TStatusTally = {
		status: EStatus;
		tally: number;
};
function isTStatusTally(test: unknown): test is TStatusTally {
	if (!commonsTypeHasPropertyEnum(test, 'status', isEStatus)) return false;
	if (!commonsTypeHasPropertyNumber(test, 'tally')) return false;

	return true;
}

export class StatisticsApi extends CommonsRestApi<ICommonsRequestWithStrictParams, express.Response> {
	private memcached: Cache;
	
	constructor(
			restServer: RestServer,
			private databaseService: DatabaseService,
			path: string
	) {
		super(restServer);

		// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-call
		this.memcached = new Cache({ keyPrefix: 'hydra', cacheHost: 'localhost:11211' });

		super.getHandler(
				`${path}rest/statistics/status`,
				async (_req: express.Request, _res: express.Response): Promise<TStatusTally[]> => {
					const index: string = 'statistics/status';

					const cached: unknown|undefined = await this.memcached.get(index);
					if (cached && commonsTypeIsTArray<TStatusTally>(cached, isTStatusTally)) return cached;

					const result: Map<EStatus, number> = await this.databaseService.listStatusTallies();
					
					const tallies: TStatusTally[] = [];
					for (const key of result.keys()) tallies.push({ status: key, tally: result.get(key) || 0 });
					
					await this.memcached.set(index, tallies, 10);
					
					return tallies;
				}
		);
	}
}
