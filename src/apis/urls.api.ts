import * as express from 'express';

import { commonsTypeEncode, TEncoded } from 'tscommons-es-core';

import { TUrlLinks } from 'hydra-crawler-ts-assets';
import { EDirection } from 'hydra-crawler-ts-assets';

import { CommonsRestApi } from 'nodecommons-es-rest';
import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';

import { RestServer } from '../servers/rest.server';

import { DatabaseService } from '../services/database.service';

export class UrlsApi extends CommonsRestApi<ICommonsRequestWithStrictParams, express.Response> {
	constructor(
			restServer: RestServer,
			private databaseService: DatabaseService,
			path: string
	) {
		super(restServer);

		// Can't use strict and ICommonsRequestWithStrictParams as the url could be anything, and idname won't accept that

		super.getHandler(
				`${path}rest/urls/:url/links`,
				async (req: express.Request, _res: express.Response): Promise<TEncoded> => {
					const outbound: string[] = await this.databaseService.listOutboundLinks(req.params.url);
					const inbound: string[] = await this.databaseService.listInboundLinks(req.params.url);

					const urlLinks: TUrlLinks = {
							[EDirection.INBOUND]: inbound,
							[EDirection.OUTBOUND]: outbound
					};
		
					return commonsTypeEncode(urlLinks);
				}
		);
	}
}
