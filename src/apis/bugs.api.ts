import * as express from 'express';

import { commonsTypeEncode, TEncoded } from 'tscommons-es-core';

import { IUrlErrors } from 'hydra-crawler-ts-assets';
import { TPhpError } from 'hydra-crawler-ts-assets';
import { TAspError } from 'hydra-crawler-ts-assets';

import { CommonsRestApi } from 'nodecommons-es-rest';
import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';

import { RestServer } from '../servers/rest.server';

import { DatabaseService } from '../services/database.service';

export class BugsApi extends CommonsRestApi<ICommonsRequestWithStrictParams, express.Response> {
	constructor(
			restServer: RestServer,
			private databaseService: DatabaseService,
			path: string
	) {
		super(restServer);

		super.getHandler(
				`${path}rest/bugs/php`,
				async (_req: express.Request, _res: express.Response): Promise<TEncoded> => {
					const result: IUrlErrors<TPhpError>[] = await this.databaseService.listPhpErrors();
	
					return commonsTypeEncode(result);
				}
		);

		super.getHandler(
				`${path}rest/bugs/asp`,
				async (_req: express.Request, _res: express.Response): Promise<TEncoded> => {
					const result: IUrlErrors<TAspError>[] = await this.databaseService.listAspErrors();
	
					return commonsTypeEncode(result);
				}
		);
	}
}
