import * as express from 'express';

import { TDomainQueue } from 'hydra-crawler-ts-assets';

import { CommonsRestApi } from 'nodecommons-es-rest';
import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';

import { RestServer } from '../servers/rest.server';

import { DatabaseService } from '../services/database.service';

export class CrawlApi extends CommonsRestApi<ICommonsRequestWithStrictParams, express.Response> {
	constructor(
			restServer: RestServer,
			private databaseService: DatabaseService,
			path: string
	) {
		super(restServer);

		super.getHandler(
				`${path}rest/crawl/queue`,
				async (_req: express.Request, _res: express.Response): Promise<TDomainQueue[]> => {
					const tallies: Map<string, number> = await this.databaseService.listDomainQueuedTallies();

					return Array.from(tallies.keys())
							.map((domain: string): TDomainQueue => ({
									domain: domain,
									queue: tallies.get(domain) || 0
							}));
				}
		);

	}
}
