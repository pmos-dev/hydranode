import * as express from 'express';

import { commonsTypeEncode, TEncoded } from 'tscommons-es-core';

import { IImage } from 'hydra-crawler-ts-assets';
import { EComparator, toEComparator, ECOMPARATORS } from 'hydra-crawler-ts-assets';

import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';
import { CommonsRestApi, commonsRestApiBadRequest } from 'nodecommons-es-rest';

import { RestServer } from '../servers/rest.server';

import { DatabaseService } from '../services/database.service';

export class ImagesApi extends CommonsRestApi<ICommonsRequestWithStrictParams, express.Response> {
	constructor(
			restServer: RestServer,
			private databaseService: DatabaseService,
			path: string
	) {
		super(restServer);

		super.getHandler(
				`${path}rest/images/:comparator(${ECOMPARATORS.join('|')})/:size[int]`,
				async (req: ICommonsRequestWithStrictParams, _res: express.Response): Promise<TEncoded> => {
					const comparator: EComparator|undefined = toEComparator(req.strictParams.comparator as string);
					if (!comparator) return commonsRestApiBadRequest('Invalid comparator');
					
					const size: number = req.strictParams.size as number;
					
					const matches: IImage[] = await this.databaseService.listImagesBySizeThreshold(size, comparator);
					
					return commonsTypeEncode(matches);
				}
		);
	}
}
