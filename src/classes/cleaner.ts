import mongodb from 'mongodb';

import { commonsTypeHasPropertyNumber } from 'tscommons-es-core';

import { TDomain, TLink } from 'hydra-crawler-ts-assets';
import { EStatus } from 'hydra-crawler-ts-assets';
import { IUrl } from 'hydra-crawler-ts-assets';

import { commonsOutputDebug, commonsOutputDoing, commonsOutputProgress, commonsOutputResult, commonsOutputSuccess } from 'nodecommons-es-cli';

import { Lists } from '../classes/lists';

import { DatabaseService } from '../services/database.service';

import { EList } from '../enums/elist';

export class Cleaner {
	constructor(
			private lists: Lists,
			private databaseService: DatabaseService
	) {}

	private async detectStatusOrphans(
			statuses: EStatus[],
			hardLimit?: number
	): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set yet');
	
		let tally: number = 0;
		let found: number = 0;

		commonsOutputDoing(`Detecting ${statuses.join(', ')} orphan urls`);
		
		while (true) {
			const urls: mongodb.FindCursor<IUrl> = this.databaseService.getUrls()
					.find<IUrl>(
							{
									$and: [
											{ status: { $in: statuses } },
											{ $or: [
													{ orphan: null },
													{ orphan: false }
											] }
									]
							},
							{}
					)
					.sort({ _id: 1 })
					.skip(tally);
			
			try {
				while (true) {
					tally++;
					if ((tally % 100) === 0) commonsOutputProgress(`${tally} urls, ${found} orphans`);

					if (hardLimit && tally >= hardLimit) break;

					const row: IUrl|null = await urls.next();
					if (row === null) break;
	
					const incoming: TLink|null = await this.databaseService.getLinks().findOne(
							{ outgoing: row.url },
							{ limit: 1 }
					);
					if (incoming) continue;
					
					// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
					await this.databaseService.getUrls().updateOne({ _id: row['_id'] }, { $set: { orphan: true } });
					found++;
				}
				
				break;
			} catch (err) {
				if (!commonsTypeHasPropertyNumber(err, 'code') || err.code !== 43) throw err;
				commonsOutputDebug('Code 43 encountered. Continuing');
			}
		}
		commonsOutputResult(`${tally} urls, ${found} orphans`);
	}

	private async detectNonAllowlistOrphans(
			statuses: EStatus[],
			hardLimit?: number
	): Promise<void> {
		let tally: number = 0;
		let found: number = 0;

		commonsOutputDoing(`Detecting non-allowlist ${statuses.join(', ')} orphan urls`);
		
		while (true) {
			const urls: mongodb.FindCursor<IUrl> = this.databaseService.getUrls()
					.find<IUrl>(
							{
									$and: [
											{ status: { $in: statuses } },
											{ $or: [
													{ orphan: null },
													{ orphan: false }
											] }
									]
							},
							{}
					)
					.sort({ _id: 1 })
					.skip(tally);
			
			try {
				while (true) {
					tally++;
					if ((tally % 100) === 0) commonsOutputProgress(`${tally} urls, ${found} orphans`);

					if (hardLimit && tally >= hardLimit) break;

					const row: IUrl|null = await urls.next();
					if (row === null) break;
					
					if (this.lists.match(EList.ALLOW, row.url)) continue;
					
					const incoming: TLink|null = await this.databaseService.getLinks().findOne(
							{ outgoing: row.url },
							{ limit: 1 }
					);
					if (incoming) continue;
					
					// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
					await this.databaseService.getUrls().updateOne({ _id: row['_id'] }, { $set: { orphan: true } });
					found++;
				}
				
				break;
			} catch (err) {
				if (!commonsTypeHasPropertyNumber(err, 'code') || err.code !== 43) throw err;
				commonsOutputDebug('Code 43 encountered. Continuing');
			}
		}
		commonsOutputResult(`${tally} urls, ${found} orphans`);
	}

	private async detectStatusCodeOrphans(
			gt: number,
			lt: number,
			hardLimit?: number
	): Promise<void> {
		let tally: number = 0;
		let found: number = 0;

		commonsOutputDoing(`Detecting DONE status code between ${gt}<${lt} orphan urls`);
		
		while (true) {
			const urls: mongodb.FindCursor<IUrl> = this.databaseService.getUrls()
					.find<IUrl>(
							{
									$and: [
											{ status: EStatus.DONE },
											{ statusCode: { $gt: gt } },
											{ statusCode: { $lt: lt } },
											{ $or: [
													{ orphan: null },
													{ orphan: false }
											] }
									]
							},
							{}
					)
					.sort({ _id: 1 })
					.skip(tally);
			
			try {
				while (true) {
					tally++;
					if ((tally % 100) === 0) commonsOutputProgress(`${tally} urls, ${found} orphans`);

					if (hardLimit && tally >= hardLimit) break;

					const row: IUrl|null = await urls.next();
					if (row === null) break;
	
					const incoming: TLink|null = await this.databaseService.getLinks().findOne(
							{ outgoing: row.url },
							{ limit: 1 }
					);
					if (incoming) continue;
					
					// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
					await this.databaseService.getUrls().updateOne({ _id: row['_id'] }, { $set: { orphan: true } });
					found++;
				}
				
				break;
			} catch (err) {
				if (!commonsTypeHasPropertyNumber(err, 'code') || err.code !== 43) throw err;
				commonsOutputDebug('Code 43 encountered. Continuing');
			}
		}
		
		commonsOutputResult(`${tally} urls, ${found} orphans`);
	}

	public async purgeOrphanUrls(hardLimit?: number): Promise<void> {
		await this.detectStatusOrphans([
				EStatus.DENY,
				EStatus.FAILED,
				EStatus.DEAD,
				EStatus.DISALLOWED
		], hardLimit);

		await this.detectNonAllowlistOrphans([
				EStatus.QUEUED,
				EStatus.DONE
		], hardLimit);

		await this.detectStatusCodeOrphans(300, 310, hardLimit);
		await this.detectStatusCodeOrphans(400, 500, hardLimit);
		
		let tally: number = 0;

		commonsOutputDoing('Removing orphan outgoing links');
		while (true) {
			const urls2: mongodb.FindCursor<IUrl> = this.databaseService.getUrls()
					.find<IUrl>({ orphan: true }, {})
					.sort({ _id: 1 })
					.skip(tally);
	
			try {
				while (true) {
					tally++;
					if ((tally % 100) === 0) commonsOutputProgress(tally);
	
					const row: IUrl|null = await urls2.next();
					if (row === null) break;
	
					await this.databaseService.getLinks().deleteMany({ url: row.url });
				}
				
				break;
			} catch (err) {
				if (!commonsTypeHasPropertyNumber(err, 'code') || err.code !== 43) throw err;
				commonsOutputDebug('Code 43 encountered. Continuing');
			}
		}
		commonsOutputResult(tally);
	
		commonsOutputDoing('Archiving orphans');
		await this.databaseService.getUrls().updateMany(
				{ orphan: true },
				{
						$set: { status: EStatus.ARCHIVED },
						$unset: {
								orphan: true,
								headers: true,
								server: true,
								hash: true,
								hashSet: true,
								ttl: true,
								reason: true,
								links: true,
								title: true
						}
				}
		);
		commonsOutputSuccess();
	}
	
	public async purgeEmptyDomains(): Promise<void> {
		let tally: number = 0;
		let found: number = 0;

		commonsOutputDoing('Enumerating domains');
		
		const results: mongodb.FindCursor<TDomain> = this.databaseService.getDomains().find<TDomain>({}, {});
		
		const domains: string[] = [];
		try {
			tally = 0;
			
			while (true) {
				tally++;
				if ((tally % 100) === 0) commonsOutputProgress(tally);

				const row: TDomain|null = await results.next();
				if (row === null) break;

				domains.push(row.domain);
			}
		} catch (err) {
			if (!commonsTypeHasPropertyNumber(err, 'code') || err.code !== 43) throw err;
		}
		
		commonsOutputResult(tally);

		tally = 0; found = 0;
		commonsOutputDoing('Detecting and removing empty domains');
		for (const domain of domains) {
			tally++;
			if ((tally % 10) === 0) commonsOutputProgress(`${tally} domains, ${found} removed`);

			const urls: IUrl|null = await this.databaseService.getUrls()
					.findOne<IUrl>({
							domain: domain,
							status: { $ne: EStatus.ARCHIVED }
					}, { limit: 1 });
			if (urls) continue;

			await this.databaseService.getDomains().deleteOne({ domain: domain });
			
			found++;
		}
		commonsOutputResult(`${tally} domains, ${found} removed`);
	}
		
}
