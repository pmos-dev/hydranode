import { URL } from 'url';

import { commonsTypeHasPropertyString, commonsTypeIsArray, commonsTypeIsTArray } from 'tscommons-es-core';

import { commonsFileReadJsonFile } from 'nodecommons-es-file';

import { Matcher } from '../helpers/matcher';

import { IExpiry, isIExpiry, toIExpiry } from '../interfaces/iexpiry';

export class Expiry {
	public static loadFromFile(file: string): IExpiry[] {
		const json: unknown = commonsFileReadJsonFile(file);
		if (!commonsTypeIsArray(json)) throw new Error('Invalid expiry list');
		
		const expiries: (IExpiry|undefined)[] = json
				.map((entry: unknown): IExpiry|undefined => {
					try {
						return toIExpiry(entry);
					} catch (e) {
						return undefined;
					}
				});
		
		if (!commonsTypeIsTArray<IExpiry>(expiries, isIExpiry)) throw new Error(`Invalid JSON list in ${file}`);
		
		return expiries;
	}
	
	private static getScore(expiry: IExpiry): number {
		let total: number = 0;

		if (commonsTypeHasPropertyString(expiry.match, 'search')) {
			total += 1000 + expiry.match.search!.length;
		}
		if (commonsTypeHasPropertyString(expiry.match, 'pathname')) {
			total += 1000 + expiry.match.pathname!.length;
		}
		if (commonsTypeHasPropertyString(expiry.match, 'hostname')) {
			total += 1000 + expiry.match.hostname!.length;
		}
	
		return total;
	}
	
	private expiries: IExpiry[] = [];
	
	public add(expiries: IExpiry[]): void {
		for (const expiry of expiries) {
			this.expiries.push(expiry);
		}
	}

	public getBestExpiry(url: string): number {
		const whatwg: URL = new URL(url);
	
		let best: IExpiry|undefined;
		let bestScore: number|undefined;
	
		for (const expiry of this.expiries) {
			if (!Matcher.matches(whatwg, expiry.match)) continue;

			const score: number = Expiry.getScore(expiry);
		
			if (bestScore === undefined || score > bestScore) {
				best = expiry;
				bestScore = score;
			}
		}
	
		if (best === undefined) throw new Error('Unable to match any expiry');
	
		return best.expiry;
	}
}
