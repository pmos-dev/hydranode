import * as http from 'http';
import * as https from 'https';
import { URL } from 'url';

import { IRequestOutcome } from '../interfaces/irequest-outcome';

import { TCrawlConfig } from '../types/tcrawl-config';
import { TRobotsConfig } from '../types/trobots-config';

import { Crawler } from './crawler';
import { Tracker } from './tracker';

export class Robot {
	public static parse(robotstxt: string, robotsConfig: TRobotsConfig): string[] {
		const paths: string[] = [];
		
		const sections: string[] = robotstxt.replace(/\r/g, '').split(/\n\n/);
		for (let section of sections) {
			section = section.trim();

			const parse: RegExpMatchArray|null = section.match(/^User-agent: \*\n([\s\S]+)/i);
			if (!parse) continue;

			for (const line of parse[1].trim().split('\n')) {
				const match: RegExpMatchArray|null = line.trim().match(/^Disallow: (\/.*)$/i);
				if (!match) continue;

				if (robotsConfig.ignoreWholeSiteDirectives && match[1] === '/') continue;
				paths.push(match[1]);
			}
		}
		
		return [...new Set(paths)];
	}
	
	private paths: Map<string, string[]>;
	
	constructor(
			private domain: string,
			private crawlConfig: TCrawlConfig,
			private robotsConfig: TRobotsConfig,
			private tracker?: Tracker
	) {
		this.paths = new Map<string, string[]>();
	}
	
	private async checkDomain(protocol: string): Promise<string[]> {
		let outcome: IRequestOutcome|undefined;
		try {
			switch (protocol) {
				case 'http':
					outcome = await Crawler.request(
							http,
							`http://${this.domain}/robots.txt`,
							this.crawlConfig.connectTimeout,
							this.crawlConfig.maxFileSize,
							this.tracker
					);
					break;
				case 'https':
					outcome = await Crawler.request(
							https,
							`https://${this.domain}/robots.txt`,
							this.crawlConfig.connectTimeout,
							this.crawlConfig.maxFileSize,
							this.tracker
					);
					break;
				default: throw new Error('Unknown protocol');
			}
		} catch (ex) { /* ignore */ }
		
		if (!outcome || !outcome.data) return [];

		return Robot.parse(outcome.data.toString(), this.robotsConfig);
	}
	
	public async load(): Promise<void> {
		for (const protocol of [ 'http', 'https' ]) {
			this.paths.set(protocol, await this.checkDomain(protocol));
		}
	}
	
	public isDisallowed(url: string): boolean {
		const whatwg: URL = new URL(url);

		const protocol: RegExpMatchArray|null = whatwg.protocol.match(/^(http(?:s?)):$/);
		if (!protocol) return false;
		
		if (!this.paths.has(protocol[1])) return false;
		
		for (const path of this.paths.get(protocol[1])!) {
			if (whatwg.pathname.substring(0, path.length) === path) return true;
		}
	
		return false;
	}
}
