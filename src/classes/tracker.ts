import { commonsAsyncAbortInterval, commonsAsyncInterval } from 'tscommons-es-async';
import { ECommonsInvocation } from 'tscommons-es-async';

import { TStatusTallies } from 'hydra-crawler-ts-assets';
import { TFetch } from 'hydra-crawler-ts-assets';
import { TOutcome } from 'hydra-crawler-ts-assets';
import { EStatus } from 'hydra-crawler-ts-assets';

import { commonsGracefulAbortAddCallback } from 'nodecommons-es-process';
import { commonsOutputAlert } from 'nodecommons-es-cli';

import { SocketIoServer } from '../servers/socket-io.server';

import { DatabaseService } from '../services/database.service';

export class Tracker {
	private map: Map<EStatus, number> = new Map<EStatus, number>();
	
	private current: number = 0;
	
	private index: number = 0;
	private fetches: TFetch[] = [];
	
	private linkTallies: number = 0;
	private domainTallies: number = 0;
	
	constructor(
			private databaseService: DatabaseService,
			private socketIoServer: SocketIoServer
	) {
		commonsGracefulAbortAddCallback((): void => {
			commonsOutputAlert('SIGINT abort flag is set. Aborting bandwidth.');

			this.abort();
		});
	}
	
	public async fetching(url: string): Promise<void> {
		const fetch: TFetch = {
				index: ++this.index,
				url: url
		};
		this.fetches.push(fetch);
		
		if (this.index > 1000) this.index = 0;
		while (this.fetches.length > 999) this.fetches.shift();
		
		await this.socketIoServer.fetching(fetch);
	}
	
	private async outcome(url: string, failed: boolean, statusCode?: number): Promise<void> {
		const fetch: TFetch|undefined = this.fetches
				.find((f: TFetch): boolean => f.url === url);
		
		if (!fetch) return;

		this.fetches = this.fetches
				.filter((f: TFetch): boolean => f.index !== fetch.index);

		const outcome: TOutcome = {
				index: fetch.index,
				failed: failed
		};
		if (statusCode !== undefined) outcome.statusCode = statusCode;
		
		await this.socketIoServer.outcome(outcome);
	}
	
	public async done(url: string, statusCode: number): Promise<void> {
		await this.outcome(url, false, statusCode);
	}

	public async failed(url: string): Promise<void> {
		await this.outcome(url, true);
	}

	public bandwidth(size: number): void {
		this.current += size;
	}

	public delta(status: EStatus, delta: number): void {
		const existing: number = this.map.get(status) || 0;
		this.map.set(status, existing + delta);
	}
	
	public abort(): void {
		commonsAsyncAbortInterval('sync');
		commonsAsyncAbortInterval('bandwidth');
		commonsAsyncAbortInterval('tally');
	}
	
	public link(count: number = 1): void {
		this.linkTallies += count;
	}
	
	public domain(): void {
		this.domainTallies++;
	}
	
	private async sync(): Promise<void> {
		const initial: Map<EStatus, number> = await this.databaseService.listStatusTallies();
		for (const status of initial.keys()) {
			this.map.set(status, initial.get(status)!);
		}
		
		this.linkTallies = await this.databaseService.getLinkTalliesCount();
		this.domainTallies = await this.databaseService.getDomainTalliesCount();
	}
	
	public async start(): Promise<void> {
		await this.sync();
		
		commonsAsyncInterval(
				1000,
				ECommonsInvocation.IF,
				async (): Promise<void> => {
					await this.socketIoServer.bandwidth(this.current);
					this.current = 0;
				},
				'bandwidth'
		);

		commonsAsyncInterval(
				1000,
				ECommonsInvocation.IF,
				async (): Promise<void> => {
					const tallies: TStatusTallies = {};
					
					for (const status of this.map.keys()) {
						tallies[status] = this.map.get(status)!;
					}
					
					await this.socketIoServer.statusTallies(tallies);
					await this.socketIoServer.linkTallies(this.linkTallies);
					await this.socketIoServer.domainTallies(this.domainTallies);
				},
				'tally'
		);

		commonsAsyncInterval(
				60000,
				ECommonsInvocation.WHEN,
				async (): Promise<void> => {
					await this.sync();
				},
				'sync'
		);
	}
}
