import { Socket } from 'net';
import { URL } from 'url';
import * as http from 'http';
import * as https from 'https';
import { IncomingHttpHeaders } from 'http';

import { commonsMapToObject, commonsTypeHasPropertyString } from 'tscommons-es-core';
import { TKeyObject } from 'tscommons-es-core';
import { commonsAsyncAbortTimeout, commonsAsyncTimeout } from 'tscommons-es-async';

import { EStatus } from 'hydra-crawler-ts-assets';

import { commonsOutputAlert, commonsOutputCompleted, commonsOutputDebug, commonsOutputError, commonsOutputInfo } from 'nodecommons-es-cli';
import { commonsGracefulAbortAddCallback, commonsGracefulAbortRemoveCallback } from 'nodecommons-es-process';
import { commonsHashMd5 } from 'nodecommons-es-security';

import { Lists } from '../classes/lists';
import { Robot } from '../classes/robot';
import { Tracker } from '../classes/tracker';

import { DatabaseService } from '../services/database.service';

import { Random } from '../helpers/random';

import { IRequestOutcome } from '../interfaces/irequest-outcome';
import { IParser } from '../interfaces/iparser';
import { IParserConfig } from '../interfaces/iparser-config';

import { TCrawlConfig } from '../types/tcrawl-config';
import { TRobotsConfig } from '../types/trobots-config';
import { TParserCtor } from '../types/tparser-ctor';

import { EList } from '../enums/elist';

import { Dns } from './dns';

export class Crawler {
	public static applyMasqueradeHeaders(request: http.ClientRequest): void {
		request.setHeader('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8');
		request.setHeader('Accept-Encoding', 'identity');
		request.setHeader('Connection', 'close');
		request.setHeader('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/68.0.3440.106 Chrome/68.0.3440.106 Safari/537.36 (UoM Hydra)');
	}
	
	public static request(
			handler: typeof http|typeof https,
			url: string,
			connectTimeout: number,
			maxFileSize: number,
			tracker?: Tracker
	): Promise<IRequestOutcome> {
		return new Promise<IRequestOutcome>((resolve: (_: IRequestOutcome) => void, reject: (_: Error) => void): void => {
			const start: number = new Date().getTime();
			let exceeded: boolean = false;

			let completed: boolean = false;
			let timedOut: boolean = false;
			const timedOutMessage: string = `Timeout on crawl of ${url}`;

			const secondaryTimeoutId: string = `crawler_${url}_secondary_timeout`;
			commonsAsyncTimeout(
					connectTimeout + 60000,
					secondaryTimeoutId
			)
					.then((): void => {
						if (timedOut || completed) return;
						
						commonsOutputDebug(`Secondary timeout for ${url}`);
						
						timedOut = true;
						reject(new Error(timedOutMessage));
						return;
					})
					.catch((e: Error): void => {
						if (e.message === 'abortTimeout called') return;
						commonsOutputDebug('debug position 10');
						console.log(e);
						throw e;
					});

			const req: http.ClientRequest = handler.request(
					url,
					(res: http.IncomingMessage): void => {
						// likely unnecessary due to doing it below, but doesn't hurt to do it twice
						req.setTimeout(connectTimeout);
						
						res.setTimeout(connectTimeout);
						
						const end: number = new Date().getTime();
						const latency: number = end - start;
										
						// always check for a body, even in 4xx 3xx etc.
						
						let read: number = 0;
						const data: Buffer[] = [];
						res.on(
								'data',
								(chunk: Buffer): void => {
									if (read > maxFileSize) {
										if (!exceeded) {
											exceeded = true;
											res.destroy();
											
											commonsOutputAlert(`Exceeded maxFileSize for URL: ${url}`);
										}
										
										return;
									}
									data.push(chunk);
									read += chunk.length;
									if (tracker) tracker.bandwidth(chunk.length);
								}
						);
						res.on(
								'end',
								(): void => {
									if (timedOut || completed) return;
									
									completed = true;
									
									commonsAsyncAbortTimeout(secondaryTimeoutId);
									
									resolve({
											latency: latency,
											// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
											ip: res['remoteIp'],
											statusCode: res.statusCode || 599,
											headers: res.headers,
											data: Buffer.concat(data),
											exceeded: exceeded
									});
								}
						);
						
						// not sure if this event actually occurs, as opposed to req error below
						res.on(
								'error',
								(err: Error): void => {
									if (timedOut || completed) return;

									commonsAsyncAbortTimeout(secondaryTimeoutId);
									
									reject(err);
								}
						);
					}
			);
		
			req.setTimeout(connectTimeout);
			
			Crawler.applyMasqueradeHeaders(req);

			req.on('timeout', (): void => {
				if (timedOut || completed) return;
				
				timedOut = true;
				req.destroy();

				commonsAsyncAbortTimeout(secondaryTimeoutId);

				reject(new Error(timedOutMessage));
			});
		
			req.on(
					'response',
					(res: http.IncomingMessage): void => {
						res['remoteIp'] = res.connection.remoteAddress;
					}
			);
			
			req.on(
					'error',
					(err: Error): void => {
						if (timedOut || completed) return;

						commonsAsyncAbortTimeout(secondaryTimeoutId);

						reject(err);
					}
			);
			
			req.on(
					'socket',
					(socket: Socket): void => {
						socket.setTimeout(connectTimeout);
						socket.on(
								'timeout',
								(): void => {
									if (timedOut || completed) return;
				
									commonsAsyncAbortTimeout(secondaryTimeoutId);
				
									timedOut = true;
									req.destroy();
					
									reject(new Error(timedOutMessage));
								}
						);
					}
			);
			
			req.end();
		});
	}
	
	private static pruneHeaders(headers: IncomingHttpHeaders, keepHeaders: string[]): Map<string, number|string|Date> {
		const map: Map<string, number|string|Date> = new Map<string, number|string|Date>();

		const keys: { key: string; type: string }[] = [];
		for (const k of keepHeaders) {
			const kt: string[] = k.split(':');
			keys.push({ key: kt[0], type: kt[1] });
		}
		
		for (const key of keys) {
			if (!Object.keys(headers).includes(key.key)) continue;
			if (!commonsTypeHasPropertyString(headers, key.key)) continue;
			
			const value: string = headers[key.key] as string;
			switch (key.type) {
				case 'string': map.set(key.key, value.toString()); break;
				case 'number': map.set(key.key, parseInt(value, 10)); break;
				case 'date': map.set(key.key, new Date(value)); break;
				default: throw new Error('unknown header key type');
			}
		}
		
		return map;
	}
	
	private isPaused: boolean = false;
	private isAborted: boolean = false;
	
	constructor(
			private domain: string,
			private database: DatabaseService,
			private crawlConfig: TCrawlConfig,
			private parsersConfig: TKeyObject<IParserConfig>,
			private robotsConfig: TRobotsConfig,
			private parsers: TParserCtor[],
			private lists: Lists,
			private tracker?: Tracker
	) {}
	
	private abort(): void {
		this.isAborted = true;
		commonsAsyncAbortTimeout(`crawl_${this.domain}`);
	}
	
	public pause(): void {
		commonsOutputAlert(`Pausing crawler for ${this.domain}`);
		
		this.isPaused = true;
	}
	
	public resume(): void {
		commonsOutputCompleted(`Resuming crawler for ${this.domain}`);
		
		this.isPaused = false;
	}
	
	public async fetch(url: string, setDomainIp: boolean): Promise<number> {
		commonsOutputInfo(`Fetching ${url}`);
		
		const whatwg: URL = new URL(url);
		
		let handler: typeof http|typeof https|undefined;
		switch (whatwg.protocol) {
			case 'http:':	handler = http; break;
			case 'https:':	handler = https; break;
			default:
				throw new Error(`unable to handle protocol ${whatwg.protocol}`);
		}

		let outcome: IRequestOutcome;
		try {
			outcome = await Crawler.request(
					handler,
					url,
					this.crawlConfig.connectTimeout,
					this.crawlConfig.maxFileSize,
					this.tracker
			);
		} catch (ex) {
			//if (!/^Timeout on crawl of/.test(ex.message)) console.error(ex);
			throw ex;
		}
		
		if (setDomainIp) {
			await this.database.domain(whatwg.hostname, outcome.ip);
			if (this.tracker) this.tracker.domain();
		}
		
		await this.database.setStatusCode(url, outcome.statusCode);

		const headers: Map<string, number|string|Date> = Crawler.pruneHeaders(outcome.headers, this.crawlConfig.keepHeaders);
		if (!headers.has('content-length') && outcome.data !== undefined) headers.set('content-length', outcome.data.length);
		await this.database.setHeaders(url, commonsMapToObject(headers));
		
		const outgoing: string[] = [];
		
		if (
			outcome.statusCode >= 300
			&& outcome.statusCode < 400
			&& headers.has('location')
			&& this.lists.match(EList.ALLOW, url)	// only queue redirects for allowlist urls
		) {
			// redirect codes
			const location: URL = new URL(headers.get('location')! as string, url);

			if (location.protocol.match(/^http(s?):$/)) {
				const added: boolean = await this.database.queue(location.toString(), this.lists.match(EList.DENY, location.toString()));
				if (added && this.tracker) this.tracker.delta(this.lists.match(EList.DENY, location.toString()) ? EStatus.DENY : EStatus.QUEUED, 1);
				
				if (location.toString() !== url) outgoing.push(location.toString());
			}
		}
		
		if (outcome.data) {
			const hash: string = commonsHashMd5(outcome.data);
			const existing: string|undefined = await this.database.getHash(url);
			
			if (hash !== existing) await this.database.setHash(url, hash);
		}
		
		if (outcome.data && outcome.data.length && headers.has('content-type')) {
			const contentType: string = headers.get('content-type')! as string;
			const isAllow: boolean = this.lists.match(EList.ALLOW, url);

			for (const ctor of this.parsers) {
				const parser: IParser = new ctor(url, outcome, this.parsersConfig);
				if (!parser.isEnabled()) continue;
				
				if (!parser.supports(contentType, isAllow)) continue;
				
				try {
					await parser.parse(this.database);
				} catch (ex) {
					commonsOutputDebug('debug position 3');
					console.log(ex);
				}
				
				if (isAllow) {
					try {
						const links: string[] = await parser.links();
						if (links.length === 0) continue;
		
						for (const link of links) {
							if (link === url) continue;
							try {
								outgoing.push(link);
							} catch (ex) { /* do nothing */ }
						}
					} catch (ex) {
						commonsOutputDebug('debug position 2');
						console.log(ex);
					}
				}
			}
		}
		
		for (const link of outgoing) {
			const added: boolean = await this.database.queue(link, this.lists.match(EList.DENY, link));
			if (added && this.tracker) this.tracker.delta(this.lists.match(EList.DENY, link) ? EStatus.DENY : EStatus.QUEUED, 1);
		}
		await this.database.link(url, outgoing);
		
		if (this.tracker) this.tracker.link(outgoing.length);

		return outcome.statusCode;
	}
	
	public async crawl(): Promise<void> {
		const gracefulAbortId: string = commonsGracefulAbortAddCallback((): void => {
			commonsOutputAlert(`SIGINT abort flag is set. Aborting crawler ${this.domain}.`);

			this.abort();
		});

		try {
			try {
				await Dns.resolve(this.domain, this.crawlConfig.connectTimeout);
			} catch (ex) {
				if ((ex as Error).message === 'DNS timeout' && !this.crawlConfig.treatTimeoutDnsAsDead) {
					commonsOutputError(`DNS timed out for ${this.domain}. Will allow reattempts`);
					return;
				}
				
				commonsOutputError(`Domain ${this.domain} is dead.`);
				await this.database.markDead(this.domain);
				return;
			}
			
			const robots: Robot = new Robot(
					this.domain,
					this.crawlConfig,
					this.robotsConfig,
					this.tracker
			);
			await robots.load();
			
			let tally: number = 0;
			while (true) {
				if (this.isAborted) break;
				
				if (this.isPaused) {
					await commonsAsyncTimeout(1000);
					continue;
				}
	
				const url: string|undefined = await this.database.next(this.domain);
				if (!url) {
					commonsOutputCompleted(`Crawl for ${this.domain} completed.`);
					break;
				}
				
				await this.database.unsetFailReason(url);
	
				if (this.tracker) this.tracker.delta(EStatus.QUEUED, -1);
	
				if (this.lists.match(EList.DENY, url)) {
					await this.database.setStatus(url, EStatus.DENY);
					await this.database.unsetTtl(url);
					if (this.tracker) this.tracker.delta(EStatus.DENY, 1);
	
					// To get here, a url was queued but is now denylisted.
					// It may have had outgoings, so remove any that were there.
					await this.database.link(url, []);
					continue;
				}
	
				if (robots.isDisallowed(url)) {
					await this.database.setStatus(url, EStatus.DISALLOWED);
					await this.database.unsetTtl(url);
					if (this.tracker) this.tracker.delta(EStatus.DISALLOWED, 1);
	
					// To get here, a url was queued but is now disallowed.
					// It may have had outgoings, so remove any that were there.
					await this.database.link(url, []);
					continue;
				}
				
				if (!await this.database.setStatus(url, EStatus.ACTIVE)) {
					commonsOutputError(`Unable to set status to active for ${url}. Aborting crawler.`);
					break;
				}
				if (this.tracker) this.tracker.delta(EStatus.ACTIVE, 1);
				
				if (tally > 0) {
					try {
						const timeout: number = Random.randomise(this.crawlConfig.betweenFetchDelay, this.crawlConfig);
						
						await commonsAsyncTimeout(timeout, `crawl_${this.domain}`);
						
						while (this.isPaused && !this.isAborted) {
							await commonsAsyncTimeout(1000);
						}
					} catch (ex) { /* ignore */ }
				}
				if (this.isAborted) break;
				
				if (this.tracker) await this.tracker.fetching(url);
				try {
					const statusCode: number = await this.fetch(url, tally === 0);
		
					if (this.tracker) await this.tracker.done(url, statusCode);
					await this.database.setStatus(url, EStatus.DONE);
					await this.database.unsetTtl(url);
					if (this.tracker) this.tracker.delta(EStatus.DONE, 1);
				} catch (ex) {
					if (this.tracker) await this.tracker.failed(url);
	
					const ttl: number = await this.database.getTtl(url) || 0;
					if (ttl < this.crawlConfig.maxFailedTtl) {
						await this.database.setTtl(url, ttl + 1);
						await this.database.setStatus(url, EStatus.QUEUED);
						if (this.tracker) this.tracker.delta(EStatus.QUEUED, 1);
					} else {
						await this.database.setStatus(url, EStatus.FAILED);
						if (this.tracker) this.tracker.delta(EStatus.FAILED, 1);
	
						if (commonsTypeHasPropertyString(ex, 'code')) {
							await this.database.setFailReason(url, (ex as { code: string }).code);
						} else if (commonsTypeHasPropertyString(ex, 'message')) {
							if (/^Timeout on crawl/.test((ex as { message: string }).message)) {
								await this.database.setFailReason(url, 'TIMEOUT');
							} else {
								await this.database.setFailReason(url, (ex as { message: string }).message);
							}
						}
					}
				} finally {
					if (this.tracker) this.tracker.delta(EStatus.ACTIVE, -1);
				}
				
				tally++;
				if (tally >= this.crawlConfig.maxFetchesPerCrawl) {
					commonsOutputAlert(`Crawl for ${this.domain} reached the maximum permitted fetches for this session.`);
					break;
				}
			}
		} catch (e) {
			commonsOutputDebug('debug position 1');
			console.log(e);
		} finally {
			commonsGracefulAbortRemoveCallback(gracefulAbortId);
		}
	}
}
