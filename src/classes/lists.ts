import { Matcher } from '../helpers/matcher';

import { IMatch } from '../interfaces/imatch';

import { EList, ELISTS } from '../enums/elist';

export class Lists {
	private lists: Map<EList, IMatch[]> = new Map<EList, IMatch[]>();

	constructor() {
		for (const list of ELISTS) {
			this.lists.set(list, []);
		}
	}

	public add(list: EList, entries: IMatch[]): void {
		for (const entry of entries) {
			this.lists.get(list)!.push(entry);
		}
	}

	public match(list: EList, url: string): boolean {
		const entries: IMatch[]|undefined = this.lists.get(list);
		if (!entries) throw new Error('Unknown list');
		
		return Matcher.isAnyMatch(url, entries);
	}
	
	public listHostnames(list: EList): string[] {
		const entries: IMatch[]|undefined = this.lists.get(list);
		if (!entries) throw new Error('Unknown list');

		return entries
				.filter((item: IMatch): boolean => item.hostname !== undefined)
				.map((item: IMatch): string => item.hostname!)
				.map((hostname: string): string => hostname
						.replace(/\(\^\|\\.\)/g, '')
						.replace(/\\./g, '.')
						.replace(/[$^]/g, '')
						.replace(/[^-a-z0-9.]/gi, '')
						.replace('www.', '')
						.trim()
				)
				.filter((hostname: string): boolean => hostname !== '');
	}
}
