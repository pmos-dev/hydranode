import mongodb from 'mongodb';

import { commonsArrayChunk, commonsDateDateToYmdHis, commonsTypeHasPropertyDate } from 'tscommons-es-core';

import { EStatus } from 'hydra-crawler-ts-assets';
import { IUrl } from 'hydra-crawler-ts-assets';

import { commonsOutputAlert, commonsOutputDoing, commonsOutputProgress, commonsOutputSuccess } from 'nodecommons-es-cli';

import { Expiry } from '../classes/expiry';

import { DatabaseService, TMongoIdRow, isTMongoIdRow } from '../services/database.service';

export class Expirer {
	constructor(
			private expiry: Expiry,
			private database: DatabaseService
	) {}
	
	private async listQueuedAndActive(): Promise<string[]> {
		commonsOutputDoing('Enumering queued and active domains');
		const results: mongodb.AggregationCursor<TMongoIdRow> = this.database.getUrls().aggregate<TMongoIdRow>([
				{ $match: { status: { $in: [ EStatus.QUEUED, EStatus.ACTIVE ] } } },
				{ $group: { _id: '$domain' } }
		], { allowDiskUse: true });

		const queued: TMongoIdRow[] = await this.database.listQueryResults(results, isTMongoIdRow);
		
		commonsOutputSuccess(queued.length);
		
		return queued
				// eslint-disable-next-line no-underscore-dangle
				.map((q: TMongoIdRow): string => q._id);
	}
	
	public async expire(limit?: number): Promise<boolean> {
		commonsOutputAlert('Running expiry thread');

		const queued: string[] = await this.listQueuedAndActive();
	
		commonsOutputDoing('Searching for expired URLs');
	
		const results: mongodb.FindCursor<IUrl> = this.database.getUrls().find<IUrl>(
				{ $and: [
						{ status: { $nin: [
								EStatus.ARCHIVED,
								EStatus.QUEUED,
								EStatus.ACTIVE
						] } },
						{ domain: { $nin: queued } },
						{ attempted: { $exists: true } }
				]},
				{}
		);
	
		const now: number = new Date().getTime() / 1000;
	
		const expired: string[] = [];
		let tally: number = 0;
		let found: number = 0;
		while (true) {
			tally++;
			if ((tally % 1000) === 0) commonsOutputProgress(`${tally}, ${found}`);
			if (limit !== undefined && tally > limit) break;
			
			const row: IUrl|null = await results.next();
			if (row === null) break;
	
			const interval: number = this.expiry.getBestExpiry(row.url);
			
			if (!commonsTypeHasPropertyDate(row, 'attempted')) continue;
			
			const attempted: number = (row['attempted'] as Date).getTime() / 1000;
			if ((attempted + interval) < now) {
				// eslint-disable-next-line @typescript-eslint/no-unsafe-argument
				expired.push(row['_id']);
				found++;
			}
		}
		commonsOutputSuccess(found);
	
		if (found === 0) return false;
	
		const batches: string[][] = commonsArrayChunk(expired, 100);

		commonsOutputDoing('Re-queuing expired');
		tally = 0;
		for (const batch of batches) {
			const batchIds: mongodb.ObjectId[] = batch
					.map((id: string): mongodb.ObjectId => new mongodb.ObjectId(id));

			await this.database.getUrls().updateMany(
					{ _id: { $in: batchIds } },
					{
							$set: { status: EStatus.QUEUED },
							$unset: { ttl: true }
					}
			);
			
			tally += 100;
			commonsOutputProgress(tally);
		}
		commonsOutputSuccess();
		
		return true;
	}

	public async expireFixed(threshold: Date): Promise<boolean> {
		commonsOutputAlert('Running fixed date expiry thread');

		const queued: string[] = await this.listQueuedAndActive();
	
		commonsOutputDoing(`Searching attempted before date ${commonsDateDateToYmdHis(threshold)}`);
		const count: number = await this.database.getUrls().find<IUrl>(
				{ $and: [
						{ status: { $ne: EStatus.ARCHIVED } },
						{ domain: { $nin: queued } },
						{ attempted: { $exists: true } },
						{ attempted: { $lt: threshold } }
				]},
				{}
		).count();
		commonsOutputSuccess(count);
	
		if (count === 0) return false;
	
		commonsOutputDoing('Re-queuing expired');
		await this.database.getUrls().updateMany(
				{ $and: [
						{ status: { $ne: EStatus.ARCHIVED } },
						{ domain: { $nin: queued } },
						{ attempted: { $exists: true } },
						{ attempted: { $lt: threshold } }
				]},
				{
						$set: { status: EStatus.QUEUED },
						$unset: { ttl: true }
				}
		);
		commonsOutputSuccess();
	
		commonsOutputDoing('Searching for expired URLs');
		
		return true;
	}
}
