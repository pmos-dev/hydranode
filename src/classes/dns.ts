import * as dns from 'dns';

import { commonsAsyncAbortTimeout, commonsAsyncTimeout } from 'tscommons-es-async';

import { commonsOutputDebug } from 'nodecommons-es-cli';

export abstract class Dns {
	public static resolve(
			domain: string,
			connectTimeout: number
	): Promise<string[]> {
		// try IP4 first, then IP6
		
		return new Promise<string[]>((resolve: (_: string[]) => void, reject: (_: Error) => void): void => {
			let timedout: boolean = false;
			const timeoutId: string = `dns_${domain}_timeout`;
			
			commonsAsyncTimeout(
					connectTimeout,
					timeoutId
			)
					.then((): void => {
						timedout = true;
						reject(new Error('DNS timeout'));
					})
					.catch((e: Error): void => {
						if (e.message === 'abortTimeout called') return;
						commonsOutputDebug('debug position 4');
						console.log(e);
						reject(e);
					});
			
			dns.resolve4(domain, (err: Error|null, address: string[]): void => {
				if (err) {
					dns.resolve6(domain, (err2: Error|null, address2: string[]): void => {
						if (err2) {
							if (!timedout) commonsAsyncAbortTimeout(timeoutId);
							reject(err2);
							return;
						}
						if (!timedout) {
							commonsAsyncAbortTimeout(timeoutId);
							resolve(address2);
						}
						return;
					});
					return;
				}
				if (!timedout) {
					commonsAsyncAbortTimeout(timeoutId);
					resolve(address);
				}
			});
		});
	}
}
