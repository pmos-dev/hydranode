import { commonsTypeHasPropertyStringOrUndefined } from 'tscommons-es-core';

export interface IMatch {
	hostname?: string;
	pathname?: string;
	search?: string;
}

export function isIMatch(test: unknown, allowBlank: boolean = false): test is IMatch {
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'hostname')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'pathname')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'search')) return false;
	
	if (!test.hostname && !test.pathname && !test.search && !allowBlank) return false;

	return true;
}
