import { commonsTypeHasPropertyBoolean } from 'tscommons-es-core';

// has to be an interface so it can be extended

export interface IParserConfig {
		enabled: boolean;
}

export function isIParserConfig(test: unknown): test is IParserConfig {
	if (!commonsTypeHasPropertyBoolean(test, 'enabled')) return false;

	return true;
}
