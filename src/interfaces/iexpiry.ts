import { commonsTypeHasPropertyNumber, commonsTypeHasPropertyObject, commonsTypeHasPropertyString, TEnumObject } from 'tscommons-es-core';

import { IMatch, isIMatch } from './imatch';

export interface IExpiry {
		match: IMatch;
		expiry: number;
}

export function isIExpiry(test: unknown): test is IExpiry {
	// Can't use hasPropertyT as we need to pass a true parameter
	if (!commonsTypeHasPropertyObject(test, 'match')) return false;
	if (!isIMatch(test.match, true)) return false;
	
	if (!commonsTypeHasPropertyNumber(test, 'expiry')) return false;

	return true;
}

enum EInterval {
		SECOND = 'second',
		MINUTE = 'minute',
		HOUR = 'hour',
		DAY = 'day',
		WEEK = 'week',
		MONTH = 'month',
		YEAR = 'year'
}

const INTERVAL_SECOND: number = 1;
const INTERVAL_MINUTE: number = INTERVAL_SECOND * 60;
const INTERVAL_HOUR: number = INTERVAL_MINUTE * 60;
const INTERVAL_DAY: number = INTERVAL_HOUR * 24;
const INTERVAL_WEEK: number = INTERVAL_DAY * 7;
const INTERVAL_YEAR: number = INTERVAL_DAY * 365.25;
const INTERVAL_MONTH: number = INTERVAL_YEAR / 12;

const INTERVAL_UNITS: TEnumObject<EInterval, number> = {
		[EInterval.SECOND]: INTERVAL_SECOND,
		[EInterval.MINUTE]: INTERVAL_MINUTE,
		[EInterval.HOUR]: INTERVAL_HOUR,
		[EInterval.DAY]: INTERVAL_DAY,
		[EInterval.WEEK]: INTERVAL_WEEK,
		[EInterval.MONTH]: INTERVAL_MONTH,
		[EInterval.YEAR]: INTERVAL_YEAR
};

function calculateExpiry(expiry: string): number {
	let total: number = 0;
	
	for (const unit of Object.keys(INTERVAL_UNITS)) {
		while (true) {
			const regex: RegExp = new RegExp(`(?:^|[^a-z])(([0-9]+)[ ]*${unit}[s]?)(?:$|[^0-9])`, 'i');
			const result: RegExpExecArray|null = regex.exec(expiry);
			if (result === null) break;
			
			let value: number = parseInt(result[2], 10);
			value *= INTERVAL_UNITS[unit];
			total += value;
			
			expiry = expiry.replace(result[1], ' ');
		}
	}
	
	if ('' !== expiry.trim()) throw new Error(`Remaining expiry: ${expiry.trim()}`);
	
	return total;
}

export function toIExpiry(data: unknown): IExpiry {
	if (commonsTypeHasPropertyString(data, 'expiry')) {
		data['expiry'] = calculateExpiry(data['expiry'] as string);
	}
	
	if (!isIExpiry(data)) throw new Error('Object is not an instance of IExpiry');
	
	return data;
}
