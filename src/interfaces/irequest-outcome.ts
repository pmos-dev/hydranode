import { IncomingHttpHeaders } from 'http';

export interface IRequestOutcome {
		latency: number;
		ip: string;
		statusCode: number;
		headers: IncomingHttpHeaders;
		data?: Buffer;
		exceeded: boolean;
}
