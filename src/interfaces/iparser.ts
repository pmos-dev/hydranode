import { DatabaseService } from '../services/database.service';

// this is a true interface
export interface IParser {
		isEnabled(): boolean;
		supports(contentType: string, isAllow: boolean): boolean;

		init(database: DatabaseService): Promise<void>;

		parse(database: DatabaseService): Promise<void>;
		links(): Promise<string[]>;
}
