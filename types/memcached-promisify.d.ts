declare module 'memcached-promisify' {
	namespace MemcachedPromisify {}

	class MemcachedPromisify {
		constructor(config: {}, options?: {});

		get(key: string): Promise<unknown>;
		utilGet(key: string): Promise<unknown>;
		getMulti(keys: string[]): Promise<unknown>;
		stats(): Promise<unknown>;
		set(key: string, data: string|number|{}, expires: number): Promise<unknown>;
		del(key: string): Promise<unknown>;
	}
	
	export = MemcachedPromisify;
}
